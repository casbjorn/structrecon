Flask>=2.3.2
networkx~=3.1
graphviz~=0.20.1
numpy~=1.25.1
requests~=2.31.0
MarkupSafe~=2.1.3
Werkzeug~=2.3.6
click~=8.1.6
setuptools~=65.5.1
scipy~=1.11.1
urllib3~=2.0.4
psycopg>=3.0

# May not work in Docker (alpine Linux)
rdkit~=2023.3.2