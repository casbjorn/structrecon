import os
import datetime
import time
import uuid
from typing import Optional
import re
import logging

import psycopg
import psycopg.sql as sql
import psycopg.types.composite as composite
from psycopg.rows import namedtuple_row

import structrecon.model.types as types
import structrecon.model.standard_data_files as datafile
import structrecon.cache.populator as populator_base
import structrecon.model.properties as properties
import structrecon.model.ontology as ontology

import structrecon.cache.populators.bigg_populator as bigg_populator
import structrecon.cache.populators.chebi_populator as chebi_populator
import structrecon.cache.populators.chebi_ontology_populator as chebi_ontology_populator
import structrecon.cache.populators.ecmdb_populator as ecmdb_populator
import structrecon.cache.populators.metanetx_populator as metanetx_populator
import structrecon.cache.populators.pubchem_populator as pubchem_populator

# TODO backup and restore using pg_dump

# Default parameters for local database
    # (different when running in Docker than locally)
# db_host: str = 'localhost'
# db_port: str = '5455'
# db_name: str = 'structrecon_db'
# db_user: str = 'postgres'
# db_pass: str = 'postgres'

# Connect with:
# postgres://user:pass@host:port/db_name

# By default, on linux, may need to set password for 'postgres' user in postgres:
# sudo -u postgres psql
# \password postgres
# Set password to 'postgres'

basedir = '.'
schema_file = f'{basedir}/structrecon/cache/database_schema.sql'

main_table_name = 'ids'

# List of actual populators
populators: list[populator_base.Populator] = [
    bigg_populator.bigg_populator,
    chebi_ontology_populator.chebi_ontology_populator,
    chebi_populator.chebi_populator,
    ecmdb_populator.ecmdb_populator,
    metanetx_populator.mnx_populator,
    pubchem_populator.pubchem_populator
]

# Type info
info_ref_id: composite.CompositeInfo
info_ref_st: composite.CompositeInfo


# Connect to the database, create if it does not exist
    # Requires running Postgres server
# ======================================================================================================================
def connect(
        db_host: str,
        db_port: str,
        db_name: str,
        db_user: str,
        db_pass: str
) -> psycopg.connection.Connection:
    """ Connect and create if it does not exist.
        Also create all relevant types and populators.
    """

    # Connect to Server and create database if not exists
    # ==================================================================================================================
    try:
        print(f'Attempting to connect: postgres://{db_user}:{db_pass}@{db_host}:{db_port}')
        with psycopg.connect(f'postgres://{db_user}:{db_pass}@{db_host}:{db_port}') as conn:
            print(f'Connection succesful')

            # Attempt to create the database
            with conn.cursor() as c:
                try:
                    # Create database
                    conn.autocommit = True
                    c.execute(
                        sql.SQL(f'CREATE DATABASE {db_name}')
                    )
                    conn.autocommit = False
                    conn.commit()
                    print(f'  - Database {db_name} created.')

                except psycopg.errors.DuplicateDatabase:
                    print(f'  - Database {db_name} exists.')
                    # TODO check database version (in 'meta' table)

    except psycopg.OperationalError as e:
        print(f'Database connection failed:\n\t{e}')
        exit(1)


    # Connect to database
    # ==================================================================================================================
    print(f'Connecting to database: postgres://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}')
    try:
        conn = psycopg.connect(f'postgres://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}')
        c = conn.cursor()
        print(f'Connected to database {db_name}. Applying schema')

        # Also create the populators
        c.execute("CREATE TABLE IF NOT EXISTS meta (field TEXT PRIMARY KEY, val INT);")
        c.execute("INSERT INTO meta VALUES ('structrecon_version', 1) ON CONFLICT DO NOTHING;")
        c.execute("INSERT INTO meta VALUES ('schema_version', 1) ON CONFLICT DO NOTHING;")
        c.execute("CREATE TABLE IF NOT EXISTS sources ("
                  "source TEXT PRIMARY KEY,"
                  "last_updated TIMESTAMP"
                  ");")
        conn.commit()

        # Create enumerated types for identifier types and structural types
        # ==============================================================================================================
        sqltypedef_id = sql.SQL(', ').join([
            sql.SQL(f"'{repr_type.shortname}'") for repr_type in types.standard_repr_types
            if isinstance(repr_type, types.IdentifierType)
        ])

        sqltypedef_st = sql.SQL(', ').join([
            sql.SQL(f"'{repr_type.shortname}'") for repr_type in types.standard_repr_types
            if isinstance(repr_type, types.StructureType)
        ])

        q = sql.SQL(
            "CREATE TYPE {type_id} AS ENUM ({sqltypedef_id}, 'input');"
        ).format(
            type_id = sql.Identifier('IDENTIFIER_TYPE'),
            sqltypedef_id = sqltypedef_id
        )
        try:
            c.execute(q)
            conn.commit()
        except psycopg.errors.DuplicateObject as e:
            print(e)
            conn.rollback()

        q = sql.SQL(
            "CREATE TYPE {type_st} AS ENUM ({sqltypedef_st});"
        ).format(
            type_st = sql.Identifier('STRUCTURE_TYPE'),
            sqltypedef_st=sqltypedef_st
        )
        try:
            c.execute(q)
            conn.commit()
        except psycopg.errors.DuplicateObject as e:
            print(e)
            conn.rollback()

        # Identifier reference
        q = sql.SQL(
            "CREATE TYPE {REF_ID} AS ("
            "repr_type {IDENTIFIER_TYPE},"
            "id TEXT,"
            "src TEXT"
            ");"
        ).format(
            IDENTIFIER_TYPE = sql.Identifier('IDENTIFIER_TYPE'),
            REF_ID = sql.Identifier('REF_ID')
        )
        try:
            c.execute(q)
            conn.commit()
        except psycopg.errors.DuplicateObject as e:
            print(e)
            conn.rollback()

        # Structure reference
        q = sql.SQL(
            "CREATE TYPE {REF_ST} AS ("
            "repr_type {STRUCTURE_TYPE},"
            "id TEXT,"
            "src TEXT"
            ");"
        ).format(
            STRUCTURE_TYPE=sql.Identifier('STRUCTURE_TYPE'),
            REF_ST = sql.Identifier('REF_ST')
        )
        try:
            c.execute(q)
            conn.commit()
        except psycopg.errors.DuplicateObject as e:
            print(e)
            conn.rollback()

        # Ontology reference
        q = sql.SQL(
            "CREATE TYPE {REF_ONT} AS ("
            "ont_type TEXT,"
            "target_type {IDENTIFIER_TYPE},"
            "target_id TEXT,"
            "src TEXT"
            ");"
        ).format(
            REF_ONT = sql.Identifier('REF_ONT'),
            IDENTIFIER_TYPE = sql.Identifier('IDENTIFIER_TYPE')
        )
        try:
            c.execute(q)
            conn.commit()
        except psycopg.errors.DuplicateObject as e:
            print(e)
            conn.rollback()

        # Property
        q = sql.SQL(
            "CREATE TYPE {PROP} AS ("
            "property_type TEXT,"
            "property TEXT,"
            "src TEXT"
            ");"
        ).format(
            PROP = sql.Identifier('PROP')
        )
        try:
            c.execute(q)
            conn.commit()
        except psycopg.errors.DuplicateObject as e:
            print(e)
            conn.rollback()


        # Create main table
        # ==============================================================================================================
        q = sql.SQL(
            "CREATE TABLE IF NOT EXISTS {main_table_name} ("
            "repr_type {IDENTIFIER_TYPE},"  # Type of identifier, from the ENUM
            "id TEXT,"                      # ID for the database entry
            "entry_name TEXT,"              # Name given in the database entry, if any
            "refs {REF_ID} ARRAY,"          # References
            "structs {REF_ST} ARRAY,"       # Structures associated with the entry
            "properties {PROP} ARRAY,"      # Properties as found in the entry
            "ontology {REF_ONT} ARRAY,"     # Ontological relations     
            "PRIMARY KEY (repr_type, id)"
            ");"
        ).format(
            main_table_name = sql.Identifier(main_table_name),
            IDENTIFIER_TYPE = sql.Identifier('IDENTIFIER_TYPE'),
            REF_ID = sql.Identifier('REF_ID'),
            REF_ST = sql.Identifier('REF_ST'),
            PROP = sql.Identifier('PROP'),
            REF_ONT = sql.Identifier('REF_ONT')
        )
        c.execute(q)
        conn.commit()


        # Create session table
        # ==============================================================================================================
        q = sql.SQL(
            "CREATE TABLE IF NOT EXISTS {session_table_name} ("
            "session_id UUID PRIMARY KEY, "
            "last_access TIMESTAMP, "
            "dir_size INTEGER"
            ");"
        ).format(
            session_table_name = sql.Identifier('sessions')
        )
        c.execute(q)
        conn.commit()

    except psycopg.OperationalError as e:
        print(f'Database connection failed:\n\t{e}')
        exit(1)

    # TODO return connection to the database
    return conn


# Session table
# ======================================================================================================================
def activate_session(conn: psycopg.connection.Connection, uid: uuid.UUID) -> None:
    """ Create or update the session entry corresonding to the given user id. """
    log = logging.getLogger(f'structrecon.{uid}')
    log.info(f'Updating session database, uid: {uid}')
    c = conn.cursor()
    q = sql.SQL(
        "INSERT INTO {table_sessions} "
        "VALUES ({uid}, {last_accessed}) "
        "ON CONFLICT ({col_uid})"
        "   DO UPDATE SET {col_last_accessed} = excluded.{col_last_accessed};"
    ).format(
        table_sessions = sql.Identifier('sessions'),
        col_uid = sql.Identifier('session_id'),
        col_last_accessed = sql.Identifier('last_access'),
        uid = uid,
        last_accessed = 'now'
    )
    c.execute(q)
    conn.commit()


def set_session_dir_size(conn: psycopg.connection.Connection, uid: uuid.UUID, size_kb: int) -> None:
    """ Set the size of the session. Should be done after processing. """
    c = conn.cursor()
    q = sql.SQL(
        "UPDATE {table_sessions} "
        "SET {col_dir_size} = {size_kb} "
        "WHERE {col_session_id} = {uid};"
    ).format(
        table_sessions = sql.Identifier('sessions'),
        col_session_id = sql.Identifier('session_id'),
        col_dir_size = sql.Identifier('dir_size'),
        uid = uid,
        size_kb = size_kb
    )
    c.execute(q)
    conn.commit()

def get_total_size_kb(conn: psycopg.connection.Connection) -> int:
    c = conn.cursor()
    q = sql.SQL(
        "SELECT SUM({col_dir_size}) FROM {table_sessions};"
    ).format(
        table_sessions = sql.Identifier('sessions'),
        col_dir_size = sql.Identifier('dir_size')
    )
    c.execute(q)
    size_total = c.fetchone()
    # print(f'Total size: {size_total[0]}')
    return int(size_total[0])

def get_sessions_to_delete(conn: psycopg.connection.Connection, delete_kb: int) -> list[uuid.UUID]:
    """ Remove old sessions until a total of delete_kb of data has been removed. """
    c = conn.cursor()
    q = sql.SQL(
        "SELECT * FROM {table_sessions} ORDER BY {col_last_access} ASC;"
    ).format(
        table_sessions = sql.Identifier('sessions'),
        col_last_access = sql.Identifier('last_access')
    )
    c.execute(q)
    rows = c.fetchall()
    delete_uids: list[uuid.UUID] = list()
    # print(f'Delete directiories:')
    for row in rows:
        # print(f' - {row[0]} ({row[2]} KB)')
        delete_uids.append(row[0])

    return delete_uids


def deactivate_session(conn: psycopg.connection.Connection, uid: uuid.UUID) -> None:
    """ Remove a session row. Should only be called after the associated files have been removed.
    """
    c = conn.cursor()
    q = sql.SQL(
        "DELETE FROM {table_sessions} "
        "WHERE {col_session_id} = {uid};"
    ).format(
        table_sessions = sql.Identifier('sessions'),
        col_session_id = sql.Identifier('session_id'),
        uid = uid,
    )
    c.execute(q)
    conn.commit()

# Source table getters and setters
# ======================================================================================================================

def get_source(conn: psycopg.connection.Connection, datafile: datafile.database_filetype) -> Optional[int]:
    # Return time last updated, or None if not present
    c = conn.cursor()

    q = sql.SQL(
        "SELECT *"
        "FROM {table_sources}"
        "WHERE {col_source} = {source}"
    ).format(
        table_sources       = sql.Identifier('sources'),
        col_source          = sql.Identifier('source'),
        source              = datafile.name
    )

    print(f'  - Checking precense of {datafile.name} in ID table...')
    c.execute(q)
    result = c.fetchone()

    if result is None:
        print(f'      {datafile.name} not present, inserting.')
        return None
    else:
        print(f'      {datafile.name} present, last updated {result[1]}. Skipping.')
        return result[1]


def update_source(conn: psycopg.connection.Connection, datafile: datafile.database_filetype):
    # TODO
    # Update the corresponding entry in the sources table
    c = conn.cursor()

    q = sql.SQL(
        "INSERT INTO {table_sources} "
        "VALUES ({source}, {last_updated})"
        "ON CONFLICT ({col_source})"
        "   DO UPDATE SET {col_last_updated} = excluded.{col_last_updated}"
    ).format(
        table_sources       = sql.Identifier('sources'),
        col_source          = sql.Identifier('source'),
        col_last_updated    = sql.Identifier('last_updated'),
        source              = datafile.name,
        last_updated        = 'now'
    )

    print(f'    Datafile {datafile.name} processed, inserting into sources table.')
    c.execute(q)
    conn.commit()
    print(f'     - Source table update successful.')


# Database interactions
    # These are done using just a cursor created by the populate script
# ======================================================================================================================

# Database output parser regex
    # Format: '(inchi,InChI=1S/C6H8O7/c7-3(8)1-6(13,5(11)12)2-4(9)10/h13H,1-2H2,(H,7,8)(H,9,10)(H,11,12)/p-3,ChEBI)'
    # '([TYPE],id,[SOURCE])
    # Get list of type strs and source strs

type_strs_re = [t.shortname for t in types.standard_repr_types]
src_strs_re = ['BiGG', 'ChEBI', 'mnx depr', 'PubChem', 'MetaNetX', 'ECMDB']
re_result_tuple = re.compile(
    #  r"^\((" + r'|'.join(type_strs_re) + r"),(\S*),(" + r'|'.join(src_strs_re) + r")\)$"  # OLD
    r"^\((" + r'|'.join(type_strs_re) + r"),([\S ]*),(" + r'|'.join(src_strs_re) + r")\)$"
)
prop_strs_re = properties.properties.keys()
re_properties_tuple = re.compile(
    # r"^\((" + r'|'.join(prop_strs_re) + r"),(\S*),(" + r'|'.join(src_strs_re) + r")\)$"    # OLD
    r"^\((" + r'|'.join(prop_strs_re) + r"),([\S ]*),(" + r'|'.join(src_strs_re) + r")\)$"
)

# Match 1 is ontology type, Match 2 is the ID type, match 3 is the ID, match 4 is the osurce
ont_strs_re = ontology.ont_str_dict.keys()
re_onts_tuple = re.compile(
    r"^\((" + r'|'.join(ont_strs_re) + r"),(" + r'|'.join(type_strs_re) + r"),(\S*),(" + r'|'.join(src_strs_re) + r")\)$"
)

def add_or_update_entry(
        c:          psycopg.Cursor,
        source:     str,
        item:       tuple[types.IdentifierType, str],
        cname:      str | None = None,
        props:      list[(properties.Property, str)] = None,
        refs:       list[tuple[types.IdentifierType, str]] = None,
        structs:    list[tuple[types.StructureType, str]] = None,
        ont_relations:   list[(ontology.Relation, types.IdentifierType, str)] = None
) -> psycopg.Cursor:
    """ Add a 'row' from any database file. Note that all connections will be two-way.
        If the item already exist, add or update the given arguments.
        The source is the string representing the source of these edges.
        If oneway enabled, do not attempt to add back-edges.

        Ontology relations are a list of tuples with the following entries
            Ontology type (Ontology)
            Target type (IdentiferType)
            Target id (str)

    """

    # Ensure standardised identifiers for referencing in database
    try:
        item = (item[0], item[0].stdfunc(item[1]))
    except ValueError:
        # Item id is malformed
        return c

    std_refs: list[tuple[types.IdentifierType, str]]
    std_ont_relations: list[(ontology.Relation, types.IdentifierType, str)]
    if refs:
        std_refs = [
            (ref[0], ref[0].stdfunc(ref[1])) for ref in refs
        ]
    else:
        std_refs = []
    if ont_relations:
        std_ont_relations = [
            (ont[0], ont[1], ont[1].stdfunc(ont[2])) for ont in ont_relations
        ]
    else:
        std_ont_relations = []

    # Construct references array
    if refs:
        sql_refs_list = sql.SQL('ARRAY [') + sql.SQL(', ').join(
            [
                sql.SQL('(') +
                sql.SQL('{ref_list_type}, {ref_list_id}, {source}').format(
                    ref_list_type = ref[0].shortname,
                    ref_list_id = ref[1].strip(),
                    source = source
                ) +
                sql.SQL(')::{REF_ID}').format(
                    REF_ID = sql.Identifier('REF_ID')
                )
                for ref in std_refs
            ]
            ) + sql.SQL(']::{REF_ID}[]').format(
                REF_ID = sql.Identifier('REF_ID')
            )
    else:
        sql_refs_list = sql.SQL('ARRAY[]::{REF_ID}[]').format(
            REF_ID = sql.Identifier('REF_ID')
        )

    # Construct structures array
    if structs:
        sql_structs_list = sql.SQL('ARRAY [') + sql.SQL(', ').join(
            [
                sql.SQL('(') +
                sql.SQL('{struct_list_type}, {struct_list_id}, {source}').format(
                    struct_list_type = struct[0].shortname,
                    struct_list_id = struct[1].strip(),
                    source = source
                ) +
                sql.SQL(')::{REF_ST}').format(
                    REF_ST = sql.Identifier('REF_ST')
                )
                for struct in structs
            ]
        ) + sql.SQL(']::{REF_ST}[]').format(
            REF_ST = sql.Identifier('REF_ST')
        )
    else:
        sql_structs_list = sql.SQL('ARRAY[]::{REF_ST}[]').format(
            REF_ST = sql.Identifier('REF_ST')
        )

    # Construct properties array
    if props:
        sql_props_list = sql.SQL('ARRAY [') + sql.SQL(',').join(
            [
                sql.SQL('({prop_type}, {prop_text}, {source})::{PROP}').format(
                    prop_type=prop[0].prop_name,
                    prop_text=prop[1],
                    source = source,
                    PROP = sql.Identifier('PROP')
                )
                for prop in props
            ]
        ) + sql.SQL(']::{PROP}[]').format(
            PROP = sql.Identifier('PROP')
        )
    else:
        sql_props_list = sql.SQL('ARRAY[]::{PROP}[]').format(
            PROP = sql.Identifier('PROP')
        )

    # Construct ontology relations
    if std_ont_relations:
        sql_rels_list = sql.SQL('ARRAY [') + sql.SQL(',').join(
            [
                sql.SQL('({ONT_TYPE}, {TARGET_TYPE}, {TARGET_ID}, {SOURCE})::{REF_ONT}').format(
                    ONT_TYPE = rel[0].name,
                    TARGET_TYPE = rel[1].shortname,
                    TARGET_ID = rel[2],
                    SOURCE = source,
                    REF_ONT = sql.Identifier('REF_ONT')
                )
                for rel in std_ont_relations
            ]
        ) + sql.SQL(']::{REF_ONT}[]').format(
            REF_ONT = sql.Identifier('REF_ONT')
        )
    else:
        sql_rels_list = sql.SQL('ARRAY[]::{ONT}[]').format(
            ONT = sql.Identifier('REF_ONT')
        )


    # Insert the primary item with references
    # If the primary item already exists, then add references and structures to the corresponding lists
    q = sql.SQL(
        "INSERT INTO {table_ids} "
        "({col_repr_type}, {col_id}, {col_entry_name}, {col_refs}, {col_structs}, {col_props}, {col_ont}) "
        "VALUES ({repr_type}, {id}, {cname}, {sql_refs_list}, {sql_structs_list}, {sql_props_list}, {sql_ont_list}) "
        "ON CONFLICT ({col_repr_type}, {col_id}) DO UPDATE "
        "   SET {col_entry_name} = COALESCE({table_ids}.{col_entry_name}, excluded.{col_entry_name}), "
        "       {col_refs} = {table_ids}.{col_refs} || excluded.{col_refs}, "
        "       {col_structs} = {table_ids}.{col_structs} || excluded.{col_structs}, "
        "       {col_props} = {table_ids}.{col_props} || excluded.{col_props}, "
        "       {col_ont} = {table_ids}.{col_ont} || excluded.{col_ont}"
        ";"
    ).format(
        table_ids = sql.Identifier(main_table_name),
        col_repr_type = sql.Identifier('repr_type'),
        col_id = sql.Identifier('id'),
        col_entry_name = sql.Identifier('entry_name'),
        col_refs = sql.Identifier('refs'),
        col_structs = sql.Identifier('structs'),
        col_props = sql.Identifier('properties'),
        col_ont = sql.Identifier('ontology'),
        repr_type = item[0].shortname,
        id = item[1].strip(),
        cname = cname,
        sql_refs_list = sql_refs_list,
        sql_structs_list = sql_structs_list,
        sql_props_list = sql_props_list,
        sql_ont_list = sql_rels_list
    )
    c.execute(q)

    # Process the referenced items. If they don't exist, insert them with a reference to the primary item.
    # If they do exist, update with a reference to the primary item.
    if refs:
        for ref in std_refs:
            q = sql.SQL(
                "INSERT INTO {table_ids} "
                "({col_repr_type}, {col_id}, {col_refs}, {col_structs}) "
                "VALUES ({ref_repr_type}, {ref_id}, {sql_back_ref}, {empty_struct_list}) "
                "ON CONFLICT ({col_repr_type}, {col_id}) DO UPDATE "
                "   SET {col_refs} = {table_ids}.{col_refs} || {sql_back_ref}; "
            ).format(
                table_ids = sql.Identifier(main_table_name),
                col_repr_type = sql.Identifier('repr_type'),
                col_id = sql.Identifier('id'),
                col_refs = sql.Identifier('refs'),
                col_structs = sql.Identifier('structs'),
                ref_repr_type = ref[0].shortname,
                ref_id = ref[1].strip(),

                # Insert empty list by default
                empty_struct_list = sql.SQL('ARRAY[]::{REF_ST}[]').format(
                    REF_ST = sql.Identifier('REF_ST')
                ),

                # Also construct back-reference (single-element REF_ID array)
                sql_back_ref = sql.SQL(
                    "ARRAY [({primary_item_type}, {primary_item_id}, {source})]::{REF_ID}[]"
                ).format(
                    REF_ID = sql.Identifier('REF_ID'),
                    primary_item_type = item[0].shortname,
                    primary_item_id = item[1].strip(),
                    source = source
                )
            )
            c.execute(q)

    # Also process outgoing ontology edges
        # Remember to add inverse!
    if ont_relations:
        for rel in std_ont_relations:
            rel_type: ontology.Relation = rel[0]
            ref_target_type: types.IdentifierType = rel[1]
            ref_target_id = rel[2]

            # Make the back-reference in ontology, only if the relation has inverse
            if rel_type.inverse:
                # If relation has inverse, insert this relation
                ont_back_ref = sql.SQL(
                    "ARRAY [({inverse_relation_name}, {primary_item_type}, {primary_item_id}, {source})]::{REF_ONT}[]"
                ).format(
                    REF_ONT = sql.Identifier('REF_ONT'),
                    inverse_relation_name = rel_type.inverse.name,
                    primary_item_type = item[0].shortname,
                    primary_item_id = item[1].strip(),
                    source = source
                )
            else:
                # Else, just add an empty ontology array
                ont_back_ref = sql.SQL(
                    "ARRAY []::{REF_ONT}[]"
                ).format(
                    REF_ONT = sql.Identifier('REF_ONT')
                )

            # Make query and insert the new entry
            q = sql.SQL(
                "INSERT INTO {table_ids} "
                "({col_repr_type}, {col_id}, {col_ontology}) "
                "VALUES ({ref_repr_type}, {ref_id}, {ont_back_ref}) "
                "ON CONFLICT ({col_repr_type}, {col_id}) DO UPDATE "
                "   SET {col_ontology} = {table_ids}.{col_ontology} || excluded.{col_ontology}"
            ).format(
                table_ids = sql.Identifier(main_table_name),
                col_repr_type = sql.Identifier('repr_type'),
                col_id = sql.Identifier('id'),
                col_ontology = sql.Identifier('ontology'),
                ref_repr_type = ref_target_type.shortname,
                ref_id = ref_target_id,
                ont_back_ref = ont_back_ref
            )
            c.execute(q)

    return c


# Getters
# ======================================================================================================================

def get_connections_and_properties(
        conn:       psycopg.Connection,
        sources:    list[(types.IdentifierType, str)]
) -> dict[
    (types.IdentifierType, str),
    (
        list[(types.RepresentationType, str, str)],
        str,
        dict[str, str],
        list[(ontology.Relation, types.RepresentationType, str, str)]
    )]:
    """ Given a list of source IDs, for each ID return a tuple containing the following three entries:
         - list of connected IDs and the name of the associated edge.
         - The name, if any, else '',
         - A dictionary of properties, possibly empty
         - Ontology relations (relation, type, id, source)
    """

    # Standardise sources
    sources_str = [
        (src[0], src[0].stdfunc(src[1])) for src in sources
    ]

    # Create cursor with namedtuple row factory
    c = conn.cursor(row_factory = namedtuple_row)

    # First construct the list of sources for the query
    sql_sources_list = sql.SQL('(') + sql.SQL(', ').join(
        [
            sql.SQL("({source_repr_type}, {source_id})").format(
                source_repr_type = source_repr_type.shortname,
                source_id = source_id.strip()
            )
            for source_repr_type, source_id in sources
        ]
    ) + sql.SQL(')')

    # Main query
    q = sql.SQL(
        "SELECT * "
        "FROM {table_ids} "
        "WHERE ({col_repr_type}, {col_id}) IN {sql_sources_list};"
    ).format(
        table_ids       = sql.Identifier('ids'),
        col_repr_type   = sql.Identifier('repr_type'),
        col_id          = sql.Identifier('id'),
        col_refs        = sql.Identifier('refs'),
        col_structs     = sql.Identifier('structs'),
        sql_sources_list = sql_sources_list
    )

    c.execute(q)
    results = c.fetchall()

    # Create map of result
    result_map: dict[
        (types.IdentifierType, str),
        (
            list[(types.RepresentationType, str, str)],
            str,
            dict[str, str],
            list[(ontology.Relation, types.RepresentationType, str, str)]
        )
    ] = dict()

    result: psycopg.cursor.Row
    for result in results:
        # Determine the source vertex first
        from_tuple = (types.string_type_map[result.repr_type], result.id)

        # Process references
        if result.refs:
            refs = result.refs[0:-1]
            refs_list: list[str] = [
                s.strip('"{').replace('\\"', '') for s in refs.split('","')
            ]
        else:
            refs_list = []

        if result.structs:
            structs = result.structs[1:-1]
            structs_list: list[str] = [
                s.strip('"').replace('\\"', '') for s in structs.split('","')
            ]
        else:
            structs_list = []

        # Process the name
        name = result.entry_name
        if not name:
            name = ''

        # Process properties
        props = {}
        if result.properties:
            props_return = result.properties[1:-1]
            props_str_list: list[str] = [
                s.strip('"').replace('\\"', '') for s in props_return.split('","')
            ]
            prop_matches = [
                re.search(re_properties_tuple, prop_str) for prop_str in props_str_list
                ]
            for match in prop_matches:
                if match:
                    props[match.group(1)] = match.group(2)

        # Process ontology (list of Relation, repr_type, id, src)
        ont_rels_list: list[(ontology.Relation, types.RepresentationType, str, str)] = []
        if result.ontology:
            onts = result.ontology[1:-1]
            onts_str_list: list[str] = [
                s.strip('"').replace('\\"', '') for s in onts.split('","')
            ]
            ont_matches = [
                re.search(re_onts_tuple, ont_str) for ont_str in onts_str_list
            ]
            for match in ont_matches:
                if match:
                    ont_rels_list.append((
                        ontology.get_relation_type_from_str(match.group(1)),
                        types.get_type_from_str(match.group(2)),
                        match.group(3),
                        match.group(4)
                    ))

        # Construct result tuple (Refs and structs are added later)
        result_map[from_tuple] = ([], name, props, ont_rels_list)

        # Match with the regex
        re_matches = [
            re.search(re_result_tuple, ref_str) for ref_str in refs_list + structs_list
        ]

        # Encode matches in the fields
        for match in re_matches:
            if match is None:
                continue
            result_map[from_tuple][0].append(
                (
                    types.get_type_from_str(match.group(1)),
                    match.group(2),
                    match.group(3)
                )
            )

    return result_map


def get_names(
        conn: psycopg.Connection,
        sources: list[(types.IdentifierType, str)],
        chunk_size: int = 100
             ) -> dict[(types.IdentifierType, str), str]:
    """ Get name of a database entry, return '' if none found. """

    # Create cursor
    c = conn.cursor(row_factory = namedtuple_row)

    # Split into chunks
    # Divide into 100-item chunks and update result map for each chunk
    i = 0
    sources_chunks: list[list[(types.IdentifierType, str)]] = list()
    while i < len(sources):
        sources_chunks.append(sources[i:min(i+chunk_size, len(sources))])
        i += chunk_size

    # Map
    result_map: dict[(types.IdentifierType, str), str] = dict()

    # First construct the list of sources for the query
    for sources_chunk in sources_chunks:
        sql_sources_list = sql.SQL('(') + sql.SQL(', ').join(
            [
                sql.SQL("({source_repr_type}, {source_id})").format(
                    source_repr_type=source_repr_type.shortname,
                    source_id=source_id.strip()
                )
                for source_repr_type, source_id in sources_chunk
            ]
        ) + sql.SQL(')')

        q = sql.SQL(
            'SELECT {col_repr_type}, {col_id}, {col_entry_name} '
            'FROM {table_ids} '
            'WHERE ({col_repr_type}, {col_id}) IN {sql_sources_list};'
        ).format(
            table_ids = sql.Identifier(main_table_name),
            col_repr_type = sql.Identifier('repr_type'),
            col_id = sql.Identifier('id'),
            col_entry_name = sql.Identifier('entry_name'),
            sql_sources_list = sql_sources_list
        )

        c.execute(q)
        result_rows = c.fetchall()
        result: psycopg.cursor.Row
        for result in result_rows:
            from_tuple = (types.string_type_map[result.repr_type], result.id)
            to_name = result.entry_name
            result_map[from_tuple] = to_name

    return result_map


# Population script
# ======================================================================================================================
def populate(
        data_files_dir: str,
        conn: psycopg.connection.Connection,
        limit: int = 10e10          # Max entries per file to load into the database
                                    # Call with a lower limit to debug
):
    """ Populate the database given files in the directory.
    """
    print(f'Populating database from directory: {data_files_dir}')

    # Check present sources
    print(f'Sources present:')
    c = conn.cursor()
    c.execute(
        sql.SQL(
            "SELECT * FROM {table_sources}"
        ).format(
            table_sources = sql.Identifier('sources')
        )
    )
    results = c.fetchall()
    for row in results:
        print(f'  - {row[0]}\t{row[1]}')

    # Check files
    if os.path.isdir(data_files_dir):
        print(f"Database directory '{data_files_dir}' exists. Found database files:")
        for fname in os.listdir(data_files_dir):
            if fname in datafile.fname_datafile_map.keys():
                print(f'  - {fname}    [recognised: {datafile.fname_datafile_map[fname].name}]')
                datafile.fname_datafile_map[fname].fname_actual = fname # Set the actual filename as found in this step.
            else:
                print(f'  - {fname}    (NOT RECOGNISED)')


    # Populate the identifier table
        # Use populator functions in the 'populators' directory
    conn.commit()
    conn.autocommit = False
    for populator in populators:
        print(f'* Starting populator: {populator.name}')
        populator.populate(
            conn,
            [f'{data_files_dir}/{data_file.fname_actual}' for data_file in populator.datafiles],
            limit
        )
        conn.commit()


# Delete script
# ======================================================================================================================

def cache_delete(
        db_host: str,
        db_port: str,
        db_name: str,
        db_user: str,
        db_pass: str,
        force: bool
):
    """ Delete the database, should primarily be used for debugging.
        Apply the --force command-line parameter to ignore connected sessions.
    """
    try:
        with psycopg.connect(
            f'postgres://{db_user}:{db_pass}@{db_host}:{db_port}'
        ) as conn:
            conn.autocommit = True
            with conn.cursor() as c:

                # List databases
                c.execute("SELECT datname FROM pg_database WHERE datistemplate = false;")
                print(f'Databases found on server:')
                rows = c.fetchall()
                for row in rows:
                    print(f'  - {row[0]}')

                if db_name not in {row[0] for row in rows}:
                    print(f'Database {db_name} does not exist.')
                    return

                # Delete
                print(f'Deleting structrecon_db...')
                if force:
                    c.execute(f"DROP DATABASE {db_name} (FORCE);")
                else:
                    c.execute(f"DROP DATABASE {db_name};")

                # List again
                print(f'Databases found on server:')
                c.execute("SELECT datname FROM pg_database WHERE datistemplate = false;")
                rows = c.fetchall()
                for row in rows:
                    print(f'  - {row[0]}')

    except psycopg.OperationalError as e:
        print(f'Database connection failed:\n\t{e}')
        exit(1)


# Misc
# ======================================================================================================================

# Progress bar
def print_prog_bar(n: int, n_total: int, t_0):
    n = max(n, 1)
    width = 20
    percent = f'{100 * (n/n_total):5.1f}%'
    filled_chars = int(width * n // n_total)
    bar = '[' + '='*filled_chars + ' '*(width-filled_chars) + ']'
    t = time.time() - t_0
    eta = (n_total/n) * t - t

    if n == n_total:
        print(f'\r    {percent}  {bar}  ({n}/{n_total}) Done.')
    else:
        print(f'\r    {percent}  {bar}  ({n}/{n_total}) est. {str(datetime.timedelta(seconds = round(eta)))}', end = "")
