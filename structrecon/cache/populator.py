# Define the base populator
from typing import Callable
import psycopg

from structrecon.model import standard_data_files


class Populator:
    """ Defines a populator"""
    name:           str                                                             # Name to give to edges in graph
    populate:       Callable[[psycopg.connection.Connection, list[str], int], int]  # Function to call
    datafiles:      list[standard_data_files.database_filetype]                     # Datafiles needed

    def __init__(
            self,
            name: str,
            populate: Callable[[psycopg.connection.Connection, list[str], int], int],
            datafiles: list[standard_data_files.database_filetype]
    ):
        self.name = name
        self.populate = populate
        self.datafiles = datafiles
