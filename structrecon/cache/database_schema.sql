-- Meta-information, used to determine if DB is out of data
CREATE TABLE IF NOT EXISTS  meta (
    field TEXT PRIMARY KEY,
    val INT
    );

-- Insert value corresponding to the current version
    -- This can be checked to assess whether to reconstruct the DB
INSERT INTO meta VALUES ('structrecon_version', 1);
INSERT INTO meta VALUES ('schema_version', 1);

-- List of sources and the date on which they were last synchronised
CREATE TABLE IF NOT EXISTS sources (
    source_table TEXT PRIMARY KEY,
    present BOOLEAN,
    last_updated TIME
);

-- Enumerated type
CREATE TYPE repr_types AS ENUM (
    'bigg',
    'chebi'
    -- ETC
    );


-- The main database
CREATE TABLE IF NOT EXISTS ids (
    type
);


COMMIT;