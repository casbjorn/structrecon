from __future__ import annotations
import psycopg
import csv

import structrecon.cache.cache as cache
import structrecon.model.types as types
import structrecon.model.standard_data_files as data_files
import structrecon.cache.populator as populator
import structrecon.model.properties as properties
import time

# Populate function
def chebi_populate(
        conn: psycopg.connection.Connection,
        fnames: list[str],
        limit: int = 10e10
) -> int:
    """ Accession file is first fname, id is the second, names the third"""
    col_chebi_id = 1
    col_source_type = 2
    col_kegg = 4

    c = conn.cursor()

    # ChEBI accession file (references)
    # ------------------------------------------------------------------------------------------------------------------

    if not cache.get_source(conn, data_files.chebi_accession):
        # First, read ChEBI accession file
        print(f'  - Populating from ChEBI accession file.')
        n_lines = 0
        n_lines_total = sum(1 for _ in open(fnames[0]))
        n_lines_interval = n_lines_total // 100
        t_0 = time.time()

        with open(fnames[0], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                chebi = line[col_chebi_id]
                if line[col_source_type] != 'KEGG COMPOUND':
                    kegg = None
                else:
                    kegg = line[col_kegg]

                # Add to database
                cache.add_or_update_entry(
                    c,
                    source='ChEBI',
                    item = (types.CHEBI, chebi),
                    refs = [
                        (types.KEGG, kegg)
                    ] if kegg else []
                )
                if n_lines > limit:
                    break

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.chebi_accession)


    # ChEBI to InChI file (structure)
    # ------------------------------------------------------------------------------------------------------------------
    if not cache.get_source(conn, data_files.chebi_id_inchi):
        # Next, read ChEBI to inchi file:
        print(f'  - Populating from ChEBI id file.')
        n_lines = 0
        n_lines_total = sum(1 for _ in open(fnames[1]))
        n_lines_interval = n_lines_total // 100
        t_0 = time.time()

        with open(fnames[1], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Process line
                chebi = line[0]
                inchi = line[1]

                # Check if the ChEBI id exists in the table, then update, otherwise create new entry
                cache.add_or_update_entry(
                    c,
                    source = 'ChEBI',
                    item = (types.CHEBI, chebi),
                    structs = [
                        (types.INCHI, inchi)
                    ] if inchi else []
                )
                if n_lines > limit:
                    break

        conn.commit()


        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.chebi_id_inchi)

    # ChEBI compounds file
    # ------------------------------------------------------------------------------------------------------------------
    # TODO this should be processed before names, to ensure correct names are set (first has precedence)
    # TODO add star rating property
    if not cache.get_source(conn, data_files.chebi_names):
        # Read names file
        print(f'  - Populating from ChEBI compounds file.')
        n_lines = 0
        n_lines_total = sum(1 for _ in open(fnames[2]))
        n_lines_interval = n_lines_total // 100
        t_0 = time.time()
        with open(fnames[2], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Process line
                chebi = line[0]
                status_tag = line[1]
                cname = line[5]
                stars = line[9]

                if cname == 'null':
                    continue

                # Check if the ChEBI id exists in the table, then update, otherwise create new entry
                cache.add_or_update_entry(
                    c,
                    source='ChEBI',
                    item=(types.CHEBI, chebi),
                    cname = cname if cname else None,
                    props = [
                        (properties.QUALITY, stars)
                    ],
                    refs = [
                        (types.CNAME, cname)
                    ] if cname else None
                )
                if n_lines > limit:
                    break

        conn.commit()

        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.chebi_id_inchi)


    # ChEBI names file
    # ------------------------------------------------------------------------------------------------------------------
    if not cache.get_source(conn, data_files.chebi_names):
        # Read names file
        print(f'  - Populating from ChEBI names file.')
        n_lines = 0
        n_lines_total = sum(1 for _ in open(fnames[3]))
        n_lines_interval = n_lines_total // 100
        t_0 = time.time()
        with open(fnames[3], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Process line
                chebi = line[1]
                name_type = line[2]
                cname = line[4]
                adapted_tag = line[5]
                language = line[6]

                # Filter based on
                if language != 'en':
                    continue
                if name_type != 'SYNONYM':
                    continue

                # Check if the ChEBI id exists in the table, then update, otherwise create new entry
                cache.add_or_update_entry(
                    c,
                    source='ChEBI',
                    item=(types.CHEBI, chebi),
                    cname = cname if cname else None,
                    refs=[
                        (types.CNAME, cname)
                    ] if cname else None
                )
                if n_lines > limit:
                    break

        conn.commit()

        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.chebi_id_inchi)




    # Done
    return 1

chebi_populator = populator.Populator(
    'ChEBI',
    chebi_populate,
    [data_files.chebi_accession, data_files.chebi_id_inchi, data_files.chebi_compounds, data_files.chebi_names]
)
