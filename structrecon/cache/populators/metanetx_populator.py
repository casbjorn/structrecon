from __future__ import annotations
import psycopg
import psycopg.sql as sql
import time
import csv

import structrecon.model.model as model
import structrecon.cache.cache as cache
import structrecon.model.types as types
import structrecon.model.standard_data_files as data_files
import structrecon.cache.populator as populator
import structrecon.model.ontology as ontology

# Table reading data
col_mnx = 0
col_cname = 1
col_ref = 2         # Can be ChEBI
col_inchi = 6
col_inchi_key = 7
col_smiles = 8

# Populate Function
def mnx_populate(
        conn: psycopg.connection.Connection,
        fnames: list[str],
        limit: int = 10e10
) -> int:
    """ Properties file is first argument, deprecation file second. """

    c = conn.cursor()

    # Properties file
    # ------------------------------------------------------------------------------------------------------------------
    if not cache.get_source(conn, data_files.mnx_properties):

        # Line count
        n_lines = 0
        n_lines_total = min(sum(1 for _ in open(fnames[0])), limit)
        n_lines_interval = n_lines_total // 100
        t_0 = time.time()

        print(f'  - Populating from MetaNetX properties file.')
        with open(fnames[0], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Skip comments
                if line[0].startswith('#'):
                    continue

                # Parse the following fields
                mnx = ''
                identifiers = {
                    types.CNAME: '',
                    types.INCHI_KEY: '',
                    types.CHEBI: '',
                    types.SMILES: '',
                    types.INCHI: ''
                }

                try:
                    mnx = line[col_mnx]
                    identifiers[types.CNAME] = line[col_cname]
                    ref = line[col_ref]
                    identifiers[types.INCHI] = line[col_inchi]
                    identifiers[types.INCHI_KEY] = line[col_inchi_key]
                    identifiers[types.SMILES] = line[col_smiles]

                except IndexError:
                    # Top if indexing further rows than exist in the file
                    pass

                if mnx == '':
                    continue

                # Parse the ref, may contain ChEBI
                if ref.startswith('chebi:'):
                    identifiers[types.CHEBI] = ref.removeprefix('chebi:')

                # Parse the cname
                cname: str | None = identifiers[types.CNAME]
                if cname == '':
                    cname = None

                # Add to db
                cache.add_or_update_entry(
                    c,
                    source = 'MetaNetX',
                    item = (types.MNX, mnx),
                    cname = cname,
                    refs = [
                        (repr_type, identifier)
                        for repr_type, identifier in identifiers.items()
                        if isinstance(repr_type, types.IdentifierType)
                        and identifier != ''
                    ],
                    structs = [
                        (repr_type, identifier)
                        for repr_type, identifier in identifiers.items()
                        if isinstance(repr_type, types.StructureType)
                        and identifier != ''
                    ]
                )

                # Check limit
                if n_lines > limit:
                    break

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.mnx_properties)

    # Deprecation file
    # ------------------------------------------------------------------------------------------------------------------
    if not cache.get_source(conn, data_files.mnx_depr):
        n_lines = 0
        n_lines_total = min(sum(1 for _ in open(fnames[1])), limit)
        n_lines_interval = n_lines_total // 100
        t_0 = time.time()

        print(f'  - Populating from MetaNetX deprecation file.')
        with open(fnames[1], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Skip comments
                if line[0].startswith('#'):
                    continue

                # Check both fields
                old_mnx = line[0]
                new_mnx = line[1]

                # Add to database
                cache.add_or_update_entry(
                    c,
                    source = 'MetaNetX',
                    item = (types.MNX, old_mnx),
                    ont_relations = [
                        (ontology.HAS_NEW_ID, types.MNX, new_mnx)
                    ]
                )

                # Check limit
                if n_lines > limit:
                    break

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.mnx_depr)

    # Relations file
    # ------------------------------------------------------------------------------------------------------------------
    if not cache.get_source(conn, data_files.mnx_isom):
        n_lines = 0
        n_lines_total = min(sum(1 for _ in open(fnames[2])), limit)
        n_lines_interval = n_lines_total // 100
        t_0 = time.time()

        print(f'  - Populating from MetaNetX relations file.')
        with open(fnames[2], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):
                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Skip comments
                if line[0].startswith('#'):
                    continue

                parent_id = line[0]
                child_id = line[1]
                relation_text = line[2]

                # Add to database (Parent -> child, the other way is added implicitly)
                cache.add_or_update_entry(
                    c,
                    source = 'MetaNetX',
                    item = (types.MNX, child_id),
                    ont_relations = [
                        (ontology.IS_A, types.MNX, parent_id)
                    ]
                )

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.mnx_depr)



    return 1  # TODO return number of entries
# Define the populator
# ------------------------------------------------------------------------------------------------------------------

mnx_populator = populator.Populator(
    'MetaNetX',
    mnx_populate,
    [data_files.mnx_properties, data_files.mnx_depr, data_files.mnx_isom]
)

