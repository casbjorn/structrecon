from __future__ import annotations
import psycopg
import csv
import time

import structrecon.model.types as types
import structrecon.cache.cache as cache
import structrecon.model.standard_data_files as data_files
import structrecon.cache.populator as populator



# Populate function
def bigg_populate(
        conn: psycopg.connection.Connection,
        fnames: list[str],
        limit: int = 10e10
) -> int:
    """ Return the number of entries generated. """

    # Parameters
    parser_bigg_name_mnx = 'MetaNetX (MNX) Chemical'
    parser_bigg_name_kegg = 'KEGG Compound'
    parser_bigg_name_chebi = 'CHEBI'
    parser_bigg_name_inchi = 'InChI Key'

    parser_bigg_prefix_mnx = 'http://identifiers.org/metanetx.chemical/'
    parser_bigg_prefix_kegg = 'http://identifiers.org/kegg.compound/'
    parser_bigg_prefix_chebi = 'http://identifiers.org/chebi/CHEBI:'
    parser_bigg_prefix_inchi = 'https://identifiers.org/inchikey/'

    add_fields: dict[types.RepresentationType, str]

    if not cache.get_source(conn, data_files.bigg):

        c = conn.cursor()

        # Line count
        n_lines = 0
        n_lines_total = sum(1 for _ in open(fnames[0]))
        n_lines_interval = n_lines_total // 100
        t_0 = time.time()

        # Start parsing
        print(f'  - Populating from BiGG file')
        with open(fnames[0], 'r') as file_bigg:
            next(file_bigg)                             # Skip header
            for line in csv.reader(file_bigg, delimiter = '\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Process
                bigg_id = types.BIGG.stdfunc(line[1])   # File does not come with M_ prefix, so add it here

                # Skip if no desired fields
                if len(line) <= 4:
                    continue

                # Process name field
                cname = line[2]

                # Process list in links field
                links = line[4]
                add_fields = {types.MNX: '', types.KEGG: '', types.INCHI_KEY: '', types.CHEBI: '', types.CNAME: ''}
                if cname:
                    add_fields[types.CNAME] = cname

                for link in links.split(';'):
                    # Each individual link is of the form "name: url"
                    parts = link.split(': ')
                    if len(parts) != 2:
                        continue  # No information in field
                    name = parts[0].strip()
                    url = parts[1].strip()

                    if name == parser_bigg_name_mnx:
                        add_fields[types.MNX] = url.removeprefix(parser_bigg_prefix_mnx)
                    elif name == parser_bigg_name_kegg:
                        add_fields[types.KEGG] = url.removeprefix(parser_bigg_prefix_kegg)
                    elif name == parser_bigg_name_inchi:
                        add_fields[types.INCHI_KEY] = url.removeprefix(parser_bigg_prefix_inchi)
                    elif name == parser_bigg_name_chebi:
                        add_fields[types.CHEBI] = url.removeprefix(parser_bigg_prefix_chebi)

                # For all fields in add_fields, add them to the
                cache.add_or_update_entry(
                    c,
                    source = 'BiGG',
                    item = (types.BIGG, bigg_id),
                    cname = cname,
                    refs = [
                        (repr_type, identifier)
                        for repr_type, identifier in add_fields.items()
                        if isinstance(repr_type, types.IdentifierType)
                        and identifier != ''
                    ]
                )

                if n_lines > limit:
                    break

        # Finally commit
        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.bigg)

    return 1

# Define populator
# ---------------------------------------------------------------------------------------------------------------------
bigg_populator = populator.Populator(
    'BiGG',
    bigg_populate,
    [data_files.bigg]
)
