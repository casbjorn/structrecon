from __future__ import annotations
import psycopg
import time
import json

import structrecon.cache.cache as cache
import structrecon.model.types as types
import structrecon.model.standard_data_files as data_files
import structrecon.cache.populator as populator

# Field names and aux info
field_names = {
        'met_id':           types.ECMDB,
        'chebi_id':         types.CHEBI,
        'kegg_id':          types.KEGG,
        'pubchem_id':       types.PC_CID,
        'name':             types.CNAME,
        'moldb_smiles':     types.SMILES,
        'moldb_inchi':      types.INCHI,
        'moldb_inchikey':   types.INCHI_KEY
    }

# Account for bad data sanitation in datafile
invalid_names = {'null', 'none', 'smiles', 'None', 'Null', ''}

# Populate function
def ecmdb_populate(
        conn: psycopg.connection.Connection,
        fnames: list[str],
        limit: int = 10e10
) -> int:

    c = conn.cursor()

    print(f'ECMDB POP STARTED')

    if not cache.get_source(conn, data_files.ecmdb):
        print(f'  - Populating from ECMDB file')
        with open(fnames[0], 'r') as file:
            data = json.load(file)
            n_lines_total = len(data)
            n_lines_interval = n_lines_total // 100
            n_lines = 0
            t_0 = time.time()
            for entry in data:
                # For each entry

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Reset variables
                identifiers: dict[types.RepresentationType, str] = {repr_type: '' for repr_type in field_names.values()}

                # For each field type, check if it exists in the entry
                for fieldname, repr_type in field_names.items():
                    identifier = str(entry[fieldname])
                    if identifier not in invalid_names:
                        identifiers[repr_type] = identifier

                # Check if name was found
                cname: str | None
                try:
                    cname = identifiers[types.CNAME]
                except KeyError:
                    cname = None

                # Add found identifiers to the database
                cache.add_or_update_entry(
                    c,
                    source = 'ECMDB',
                    item = (types.ECMDB, identifiers[types.ECMDB]),
                    cname = cname,
                    refs = [
                        (repr_type, identifier)
                        for repr_type, identifier in identifiers.items()
                        if isinstance(repr_type, types.IdentifierType)
                        and identifier != ''
                    ],
                    structs= [
                        (repr_type, identifier)
                        for repr_type, identifier in identifiers.items()
                        if isinstance(repr_type, types.StructureType)
                        and identifier != ''
                    ]
                )

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.ecmdb)
    else:
        print('CACHE ERROR')

    return 1


ecmdb_populator = populator.Populator(
    'ECMDB',
    ecmdb_populate,
    [data_files.ecmdb]
)
