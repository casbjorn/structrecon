from __future__ import annotations
import psycopg

import structrecon.cache.cache as cache
import structrecon.model.types as types
import structrecon.model.standard_data_files as data_files
import structrecon.cache.populator as populator
import structrecon.model.properties as properties
import structrecon.model.ontology as ontology
import time
import re

# Populate function
def chebi_ontology_populate(
        conn: psycopg.connection.Connection,
        fnames: list[str],
        limit: int = 10e10
) -> int:
    """ Accession file is first fname, id is the second, names the third"""
    col_chebi_id = 1
    col_source_type = 2
    col_kegg = 4

    c = conn.cursor()

    # Enhance limit to reflect nature of file
    if limit < 10e10:
        limit *= 10

    # ChEBI ontology file
    # ------------------------------------------------------------------------------------------------------------------

    if not cache.get_source(conn, data_files.chebi_ontology):
        # First, read ChEBI accession file
        print(f'  - Populating from ChEBI ontology file.')
        n_lines = 0
        n_lines_total = sum(1 for _ in open(fnames[0]))
        n_lines_interval = n_lines_total // 100
        t_0 = time.time()

        # Keep track of the current ID being worked on and properties found
        chebi_id: str = ''
        name: str = ''
        props: list[(str, str)] = list()
        refs: list[(types.IdentifierType, str)] = list()
        structs: list[(types.IdentifierType, str)] = list()
        ont_relations: list[(ontology.Relation, types.IdentifierType, str)] = list()

        # Define regexes
        re_synonym = re.compile(r'^synonym: "([\s\S]*)" [\s\S]*$')  # Group 1 is the synonym
        re_property = re.compile(r'http://purl\.obolibrary\.org/obo/chebi/(\S*) "(\S*)"')
            # Group 1 is type of property (smiles, inchi), group 2 is the property
        re_relation = re.compile(r'^(\S+) CHEBI:(\d+)$')

        with open(fnames[0], 'r') as file:
            for line in file.readlines():

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Main parsing logic
                # ------------------------------------------------------------------------------------------------------

                # Read the line and parse
                data = line.strip().split(':', 1)
                key = data[0].strip()
                if len(data) >= 2:
                    val = data[1].strip()
                else:
                    continue

                # Add values depending on the key
                match key:
                    case 'alt_id':
                        refs.append((types.CHEBI, val))

                    case 'name':
                        # Name given directly
                        name = val
                        refs.append((types.CNAME, val))

                    case 'synonym':
                        # Synonym, format is:   "NAME" [SOURCE]
                        match = re.match(re_synonym, val)
                        if match:
                            refs.append((types.CNAME, match.group(1)))

                    case 'property_value':
                        # Different values, defined by the obolibrary string
                        match = re.match(re_property, val)
                        if match:
                            prop_type = match.group(1)
                            prop_text = match.group(2)
                            match prop_type:
                                case 'smiles':
                                    structs.append((types.SMILES, prop_text))
                                case 'inchi':
                                    structs.append((types.INCHI, prop_text))
                                case 'inchikey':
                                    refs.append((types.INCHI_KEY, prop_text))
                                case 'charge':
                                    props.append((properties.CHARGE, prop_text))

                    case 'subset':
                        # Describes the star rating, 1_STAR, 2_STAR, 3_STAR
                        props.append((properties.QUALITY, val.removesuffix('_STAR')))

                    case 'xref':
                        xref = val.split(':')
                        if len(xref) >= 2:
                            ref_type = types.get_type_from_str(xref[0])
                            ref_text = xref[1]
                            if ref_type:
                                refs.append((ref_type, ref_text))

                    case 'is_a':
                        # Add is_a ontology
                        target_type, target = val.split(':', 1)
                        if target:
                            ont_relations.append((
                                ontology.IS_A,
                                types.get_type_from_str(target_type),
                                target
                            ))

                    case 'relationship':
                        # Parse other ontology relation
                        match = re_relation.match(val)
                        if match:
                            rel_type = ontology.get_relation_type_from_str(match.group(1))
                            if rel_type:
                                rel_target = match.group(2)
                                if not rel_target:
                                    raise RuntimeError(f'Value target empty at line {n_lines}')
                                ont_relations.append((
                                    rel_type,
                                    types.CHEBI,
                                    rel_target
                                ))

                    # Finalise this entry
                    case 'id':
                        # New entry detected
                        if id:
                            # Finalise processing and ID
                            cache.add_or_update_entry(
                                c,
                                source = 'ChEBI',
                                item = (types.CHEBI, chebi_id),
                                cname = name if name else None,
                                refs = refs,
                                structs = structs,
                                props = props,
                                ont_relations = ont_relations
                            )

                        # Start working on a new id
                        if not val:
                            raise RuntimeError(f'Empty ChEBI value at {n_lines}')

                        chebi_id = val
                        name = ''
                        props = list()
                        refs = list()
                        structs = list()
                        ont_relations = list()

                # -----------------------------------------------------------------------------------------------------.
                if n_lines > limit:
                    break

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.chebi_accession)

    # Done
    return 1

chebi_ontology_populator: populator = populator.Populator(
    'ChEBI Ontology',
    chebi_ontology_populate,
    [data_files.chebi_ontology]
)
