from __future__ import annotations
import psycopg
import time
import csv

import structrecon.cache.cache as cache
import structrecon.model.types as types
import structrecon.model.standard_data_files as data_files
import structrecon.cache.populator as populator
import structrecon.model.ontology as ontology

# Populate function
def pubchem_populate(
        conn: psycopg.connection.Connection,
        fnames: list[str],
        limit: int = 10e10          # 0 for no limit
) -> int:
    """ fnames should contain several entries:
        fnames[0] : CID to InChI
        fnames[1]: SID-Map
        fnames[2]: CID-Title
    """

    # TODO extra argument to limit values to 1000 each, in order to debug in reasonable time.
    c = conn.cursor()

    # READ INCHI FILE
    # ==================================================================================================================
    if not cache.get_source(conn, data_files.pubchem_cid_inchi):
        n_lines = 0
        n_lines_total = min(sum(1 for _ in open(fnames[0])), limit)
        n_lines_interval = n_lines_total // 1000
        t_0 = time.time()

        print(f'  - Populating from PubChem CID to INCHI file')
        cache.print_prog_bar(n_lines, n_lines_total, t_0)
        with open(fnames[0], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                cid = line[0]
                inchi = line[1]
                inchi_key = line[2]

                # Formulate query and add
                cache.add_or_update_entry(
                    c,
                    source = 'PubChem',
                    item = (types.PC_CID, cid),
                    refs = [
                        (types.INCHI_KEY, inchi_key)
                    ],
                    structs = [
                        (types.INCHI, inchi)
                    ]
                )

                # Check limit
                if n_lines > limit:
                    break

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.pubchem_cid_inchi)


    # READ SID MAP
    # ==================================================================================================================
    if not cache.get_source(conn, data_files.pubchem_sid_map):
        n_lines = 0
        n_lines_total = min(sum(1 for _ in open(fnames[1])), limit)
        n_lines_interval = n_lines_total // 1000
        t_0 = time.time()

        # Columns - SID \t SOURCE \t SOURCE ID \t CID
        file_col_source = 1
        file_col_source_id = 2
        file_col_pc_cid = 3

        source_type_map: dict[str, types.IdentifierType] = {
            'KEGG': types.KEGG,
            'ChEBI': types.CHEBI
        }

        print(f'  - Populating from PubChem SID map')
        cache.print_prog_bar(n_lines, n_lines_total, t_0)
        with open(fnames[1], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                if len(line) <= file_col_pc_cid:
                    continue

                cid = line[file_col_pc_cid]
                source = line[file_col_source]
                source_id = line[file_col_source_id]

                # Check for source
                if source in source_type_map.keys():
                    source_type: types.IdentifierType = source_type_map[source]

                    # Add to database
                    cache.add_or_update_entry(
                        c,
                        source='PubChem',
                        item=(types.PC_CID, cid),
                        refs=[
                            (source_type, source_id)
                        ]
                    )

                # Check limit
                if n_lines > limit:
                    break

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.pubchem_sid_map)

    # READ CID Title
    # ==================================================================================================================
    if not cache.get_source(conn, data_files.pubchem_cid_title):
        # Count lines (Specific to this file, strange utf-8 characters).
        n_lines = 0
        with open(fnames[2], 'rb') as file:
            q = file.readlines()
            n_lines_total = min(sum(1 for _ in q), limit)
        n_lines_interval = n_lines_total // 1000
        t_0 = time.time()

        print(f'  - Populating from PubChem CID to title')
        cache.print_prog_bar(n_lines, n_lines_total, t_0)
        with open(fnames[2], 'r') as file:

            while True:     # Main loop, readline needs to be encapsulated in try/catc
                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                try:
                    # Need to read line manually due to weird unicode stuff
                    line = file.readline()
                    if not line:
                        # End of loop
                        break
                    fields = line.split('\t')
                    if len(fields) < 2:
                        # Required fields not present in file
                        continue

                    # Check and add
                    cid = fields[0]
                    name = fields[1].strip()

                    # Fix CID appearing in name field
                    if name.startswith('CID'):
                        continue
                    if name.startswith('Zzz'):
                        continue

                    # Add name field
                    cache.add_or_update_entry(
                        c,
                        source = 'PubChem',
                        item = (types.PC_CID, cid),
                        cname = name,
                        refs = [
                            (types.CNAME, name)
                        ]
                    )

                    # Check limit
                    if n_lines > limit:
                        break

                except UnicodeError as e:
                    print(f'Unicode Error when reading file at line {n_lines}: {e}')
                    pass

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.pubchem_cid_title)

    # READ preferred CID
    # ==================================================================================================================
    if not cache.get_source(conn, data_files.pubchem_cid_preferred):
        # Count lines (Specific to this file, strange utf-8 characters).
        n_lines = 0
        n_lines_total = min(sum(1 for _ in open(fnames[3])), limit)
        n_lines_interval = n_lines_total // 1000
        t_0 = time.time()

        print(f'  - Populating from PubChem preferred CID file')
        cache.print_prog_bar(n_lines, n_lines_total, t_0)
        with open(fnames[3], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Read line
                old_cid = line[0]
                new_cid = line[1]
                cache.add_or_update_entry(
                    c,
                    source = 'PubChem',
                    item = (types.PC_CID, old_cid),
                    ont_relations = [
                        (ontology.HAS_NEW_ID, types.PC_CID, new_cid)
                    ]
                )

                # Check limit
                if n_lines > limit:
                    break

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.pubchem_cid_title)

    # READ preferred CID
    # ==================================================================================================================
    if not cache.get_source(conn, data_files.pubchem_cid_parent):
        # Count lines (Specific to this file, strange utf-8 characters).
        n_lines = 0
        n_lines_total = min(sum(1 for _ in open(fnames[4])), limit)
        n_lines_interval = n_lines_total // 1000
        t_0 = time.time()

        print(f'  - Populating from PubChem CID Parent file')
        cache.print_prog_bar(n_lines, n_lines_total, t_0)
        with open(fnames[4], 'r') as file:
            for line in csv.reader(file, delimiter='\t'):

                # Progress bar
                n_lines += 1
                if n_lines % n_lines_interval == 0:
                    cache.print_prog_bar(n_lines, n_lines_total, t_0)

                # Read line
                child_cid = line[0]
                parent_cid = line[1]
                if child_cid == parent_cid:
                    continue

                cache.add_or_update_entry(
                    c,
                    source='PubChem',
                    item=(types.PC_CID, child_cid),
                    ont_relations=[
                        (ontology.IS_A, types.PC_CID, parent_cid)
                    ]
                )

                # Check limit
                if n_lines > limit:
                    break

        conn.commit()
        cache.print_prog_bar(n_lines_total, n_lines_total, t_0)
        cache.update_source(conn, data_files.pubchem_cid_title)


    return 1


pubchem_populator = populator.Populator(
    'PubChem',
    pubchem_populate,
    [
        data_files.pubchem_cid_inchi,
        data_files.pubchem_sid_map,
        data_files.pubchem_cid_title,
        data_files.pubchem_cid_preferred,
        data_files.pubchem_cid_parent
    ]
)
