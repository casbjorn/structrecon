import logging
import json

import structrecon.fileinterface.fileinterface_base as base
import structrecon.model.app_query as app_query
import structrecon.model.parameters as parameters
import structrecon.model.types as types

class JsonInterface(base.FileInterface):
    """ Implements reading and writing back to a list file. """
    filetype_extension = 'json'

    def add_to_query(
            self,
            query: app_query.AppQuery,
            filename: str,
            primary_id_type: types.RepresentationType = types.AUTO,
            verbose: bool = False
    ):
        """ Given a list of IDs of the specified type, add compounds within to list. """
        log = query.log
        log.info(f'Loading JSON file: {filename}')
        with open(filename, 'r') as f:
            try:
                data = json.load(f)  # TODO limit size of JSON
                assert(isinstance(data, dict))  # JSON data should be a dict

                # First, parse settings
                if 'parameters' in data.keys():
                    param_entries: dict = data['parameters']
                    for param_name, param_value in param_entries.items():
                        # Get name and confirm it is a valid parameter
                        try:
                            param = parameters.parameters_by_name[param_name]
                        except KeyError as e:
                            raise base.CouldNotParseFileError(f'Parameter {param_name} not valid')

                        # Handle different parameter types
                        match param_value:
                            case int() | float() | str() | bool() | list() | dict():
                                log.info(f'Setting parameter {param.name}: {param_value} ({type(param_value)})')
                                query.set_parameter(param, param_value)
                            case _:
                                raise base.CouldNotParseFileError('Unsupported JSON parameter conversion')

                # Next, parse compounds
                try:
                    compounds_data = data['compounds']
                except KeyError:
                    # Done, return
                    return

                if isinstance(compounds_data, list):
                    # Compounds given as a simple list, parse using the set type, or auto-infer
                    log.info(f'JSON parser: Parsing compounds as list')
                    parse_type: types.RepresentationType = types.AUTO
                    if parameters.param_default_type in query.parameters:
                        parse_type_str = query.parameters[parameters.param_default_type]
                        parse_type = types.get_type_from_str(parse_type_str)
                        if parse_type is None:
                            raise base.CouldNotParseFileError(f'Type {parse_type_str} not valid.')
                    for compound_str in compounds_data:
                        assert(isinstance(compound_str, str))
                        query.add_compound_from_line(compound_str, parse_type)

                elif isinstance(compounds_data, dict):
                    # Compounds given as a list of objects
                    log.info(f'JSON parser: Parsing compounds as dictionary')
                    for compound_name, identifier_tuples in compounds_data.items():
                        try:
                            c = app_query.CompoundInput(
                                identifiers = [
                                    (types.get_type_from_str(entry[0]), str(entry[1]))
                                    for entry in identifier_tuples
                                ],
                                source_id = compound_name
                            )
                            query.add_compound(c)
                        except IndexError as e:
                            raise base.CouldNotParseFileError(f'Malformed JSON input: {e}')

                else:
                    raise base.CouldNotParseFileError('Compounds not given in correct format.')

                # Parse reactions
                # TODO

            except ValueError as e:
                raise base.CouldNotParseFileError(e)
            except AssertionError as e:
                raise base.CouldNotParseFileError(e)

            log.info(f'JSON loaded.')
