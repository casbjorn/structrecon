import logging

import structrecon.fileinterface.fileinterface_base as base
import structrecon.model.app_query as app_query
import structrecon.model.types as types

class ListInterface(base.FileInterface):
    """ Implements reading and writing back to a list file. """
    filetype_extension = 'txt'

    def add_to_query(
            self,
            query: app_query.AppQuery,
            filename: str,
            primary_id_type: types.RepresentationType = types.AUTO,
            verbose: bool = False
    ):
        """ Given a list of IDs of the specified type, add compounds within to list. """
        log = query.log
        log.info(f'Loading identifiers from list: {filename}')

        with open(filename, 'r') as f:
            for line in f.readlines():
                identifier = line.strip()
                if not identifier:
                    continue
                query.add_compound_from_line(line, type_default = primary_id_type)

        log.info('Identifiers loaded.')
