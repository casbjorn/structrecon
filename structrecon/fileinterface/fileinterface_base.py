# Defines the interface for reading to and writing from a file representing a metabolic network (e.g. SBML)
from __future__ import annotations

import structrecon.model.app_query as app_query
import structrecon.model.types as types


class CouldNotParseFileError(Exception):
    pass

class FileInterface:
    filetype_extension:             str             # The typical extension for the file

    def add_to_query(
            self,
            query: app_query.AppQuery,
            filename: str,
            primary_id_type: types.RepresentationType = types.BIGG,
            verbose: bool = False
    ):
        """ TODO describe
            Should raise CouldNotParseError if file cannot be correctly read.
        """
        pass

    def save_to_file(
            self,
            input_map: dict[str, list[(str, int)]],
            filename_in: str,
            filename_out: str
    ):
        """ Use a generated input-output mapping to produce a file of the same type.
            If the input file is non-empty, the output file should be formatted identically.
        """
        pass
