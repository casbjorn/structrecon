""" Defines properties
"""

class Property:
    prop_name:          str

    def __init__(self, prop_name: str):
        self.prop_name = prop_name




# Define individual properties
# ----------------------------------------------------------------------------------------------------------------------

QUALITY = Property(
    prop_name = 'quality'
)

CHARGE = Property(
    prop_name = 'charge'
)








# List of properties
properties: dict[str, Property] = {
    'quality': QUALITY
}