""" Defines the ontology types
"""

from __future__ import annotations

# Define the base class and possible properties of the ontology relations
# ----------------------------------------------------------------------------------------------------------------------
class Relation:
    name:           str
    description:    str
    synonyms:       dict[str, str]      # For each source, what this relationship is called

    # Relational properties
    symmetric:      bool
    transitive:     bool
    inverse:        Relation            # If there is a corresponding inverse relation. Not applicable if symmetric
    is_primary:     bool                # If relation has an inverse, is_primary is true if this relation is shown
                                        # If no primaries, then both are shown with a dash /
    # Drawing
    draw:           str                 # 'full' or 'arrow'

    def __init__(
            self,
            name: str,
            description: str,
            symmetric: bool,
            transitive: bool = False,
            inverse: Relation = None,
            is_primary: bool = False,
            synonyms: dict[str, str] = None,
            draw = 'full'
    ):
        self.name = name
        self.description = description
        self.symmetric = symmetric
        self.transitive = transitive
        self.inverse = inverse
        self.is_primary = is_primary
        self.synonyms = synonyms if synonyms else {}
        self.draw = draw


# Define the ontologies
# ----------------------------------------------------------------------------------------------------------------------

IS_A = Relation(
    name = 'Is a',
    description = 'Defines a subclass relation.',
    symmetric = False,
    synonyms = {
        'chebi': 'is_a'
    }
)

HAS_INSTANCE = Relation(
    name = 'Has instance',
    description = 'Reverse of is_a relation',
    symmetric = False,
)
IS_A.inverse = HAS_INSTANCE
HAS_INSTANCE.inverse = IS_A
IS_A.is_primary = True

HAS_PART = Relation(
    name = 'Has part',
    description = 'TODO',
    symmetric = False,
    synonyms = {
        'chebi': 'has_part'
    }
)

TAUTOMER = Relation(
    name = 'Tautomer',
    description = 'TODO',
    symmetric = True,
    transitive = True,
    synonyms = {
        'chebi': 'is_tautomer_of'
    }
)

CONJUCATE_ACID = Relation(
    name = 'Conjugate acid',
    description = 'TODO',
    symmetric = False,
    transitive = False,
    synonyms = {
        'chebi': 'is_conjugate_acid_of'
    }
)

CONJUGATE_BASE = Relation(
    name = 'Conjugate base',
    description = 'TODO',
    symmetric = False,
    transitive = False,
    synonyms = {
        'chebi': 'is_conjugate_base_of'
    }
)
CONJUCATE_ACID.inverse = CONJUGATE_BASE
CONJUGATE_BASE.inverse = CONJUCATE_ACID

STEREOISOMER = Relation(
    name = 'Stereoisomer',
    description = 'TODO',
    symmetric = True,
    transitive = True,
    synonyms = {}
)

ENANTIOMER = Relation(
    name = 'Enantiomer',
    description = 'TODO',
    symmetric = True,
    transitive = True,
    synonyms = {
        'chebi': 'is_enantiomer_of'
    }
)

HAS_NEW_ID = Relation(
    name = 'New ID',
    description = 'Is deprecated ID of',
    symmetric = False,
    transitive = True,
    synonyms = {},
    draw = 'arrow'
)

HAS_OLD_ID = Relation(
    name = 'Old ID',
    description = 'Has deprecated ID',
    symmetric = False,
    transitive = True,
    synonyms = {},
    draw = 'arrow'
)
HAS_NEW_ID.inverse = HAS_OLD_ID
HAS_OLD_ID.inverse = HAS_NEW_ID
HAS_NEW_ID.is_primary = True


# Make list of all ontology relations
# ----------------------------------------------------------------------------------------------------------------------
ontology_relations: list[Relation] = [
    IS_A, HAS_INSTANCE, HAS_PART, TAUTOMER, CONJUCATE_ACID, CONJUGATE_BASE, ENANTIOMER, HAS_NEW_ID, HAS_OLD_ID
]

ont_str_dict: dict[str, Relation] = dict()
for rel in ontology_relations:
    ont_str_dict[rel.name] = rel
    for syn in rel.synonyms.values():
        ont_str_dict[syn] = rel

def get_relation_type_from_str(s: str) -> Relation | None:
    try:
        return ont_str_dict[s]
    except KeyError:
        return None