from __future__ import annotations
import psycopg
import re
import logging
from typing import Callable

import structrecon.model.model as model
import structrecon.model.parameters as parameters
import structrecon.model.types as types
import structrecon.cache.cache as cache
import structrecon.model.ontology as ontology
import structrecon.model.app_query as app_query

# Methods for exploring the vertices
# ==================================================================================================================
def explore(M: model.Model, conn: psycopg.connection.Connection):
    """ Based on the vertices already added to the identifier graph, explore the full graph
    """

    # 1. Determine exploration parameters from query and model
    # -----------------------------------------------------------------------------------------------------------------
    # Possible parameters for each setting
    # 'full' - explore as if normal links
    # 'info' - Informational - Only include ontology relation if both IDs are already in the graph. This will not link
    #           any new IDs, but provide context to the identifier graph
    # 'none' - Disregard
    # int value - explore only this number of steps
    log = M.query.log


    # 2. Database exploration loop
    # ------------------------------------------------------------------------------------------------------------------
    vertex_explore_set: set[model.Vertex] = M.id_graph.nodes()
    vertex_explore_set = {v for v in vertex_explore_set if v.type != types.INPUT}
    steps = 1
    M.generation = 1
    steps_max = M.query.parameters[parameters.param_max_explore_steps]

    while vertex_explore_set and steps <= steps_max:
        # Explore the vertices in the list and potentially run again on the new list
        M.generation = steps
        vertex_explore_set = explore_set(M, conn, vertex_explore_set, M.explore_full, log = log)
        steps += 1


    # 4. Trim the identifier graph
    # ------------------------------------------------------------------------------------------------------------------
    # Nodes which are only connected to the rest of the graph by ontologies that are not set to explore 'full',
    # can be removed, as they provide no extra information



    # 5. Append names to all vertices
    # ------------------------------------------------------------------------------------------------------------------
    get_names: list[(types.IdentifierType, str)] = []
    v: model.Vertex
    for v in M.id_graph.nodes:
        if isinstance(v.type, types.IdentifierType) and not v.name:
            get_names.append((v.type, v.cont))

    name_map = cache.get_names(conn, get_names)
    for v_source, name in name_map.items():
        if name:
            v = M.check_exists(v_source[0], v_source[1])
            if v:
                v.name = name

    # Finish
    log.info(f'Finished exploration.')


def explore_set(
        M: model.Model,
        conn: psycopg.connection.Connection,
        vs: set[model.Vertex],
        explore_full: set[ontology.Relation],
        log: logging.Logger
) -> set[model.Vertex]:
    """ Given a list of vertices, look up all of these in the database, returning the list of new ids
    """

    # When exploring the set, look the whole set up in the database and
    # add new vertices with relevant edge information.
    result_map: dict[
        (types.IdentifierType, str),
        (
            list[(types.RepresentationType, str, str)],
            str,
            dict[str, str],
            list[(ontology.Relation, types.RepresentationType, str, str)]
        )] = dict()
    chunk_size = 100
    vs_list = list(vs)

    # Divide into 100-item chunks and update result map for each chunk
    i = 0
    vs_chunk: list[model.Vertex]
    while i < len(vs_list):
        vs_chunk = vs_list[i:min(i+chunk_size, len(vs_list))]
        result_map.update(cache.get_connections_and_properties(
            conn,
            [(v.type, v.cont) for v in vs_chunk]
        ))
        i += chunk_size

    new_vertices: set[model.Vertex] = set()

    # For each reference, link the vertices and add new vertices to explore list
    for v in vs:
        toomanyrefs: bool = False
        toomanyonts: bool = False
        if (v.type, v.cont) not in result_map.keys():
            continue

        result_map_get = result_map[(v.type, v.cont)]
        refs = result_map_get[0]
        name = result_map_get[1]
        props = result_map_get[2]
        ont_refs = result_map_get[3]

        # Set name and properties
        v.name = name
        v.properties = props

        # Excluded from over-referencing (Hacky and hardcoded, #FIXME)
        maxref_exclude = {
            'M_h', 'M_h2o', 'M_pi', 'M_o2', 'M_co2', 'M_nadp', 'M_coa'
        }

        # Check if cname less than four letters
        if v.type is types.CNAME and len(v.cont) <= 4:
            continue

        # Check if too many references (TODO should be configurable)
        if v.cont not in maxref_exclude:
            if len(refs) > 50:
                log.info(f'{v} excluded due to too many references. refs: {len(refs)}')
                print(f'refs: {len(refs)} -- {v}')      # DEBUG
                toomanyrefs = True
            if len(ont_refs) > 20:
                log.info(f'Ontology: {v} excluded due to too many relations: {len(ont_refs)}')
                toomanyonts = True

        # Add references
        if not toomanyrefs:
            for ref in refs:
                # Check if source (ref_source) is enabled in the settings
                ref_type: types.IdentifierType = ref[0]
                ref_id: str = ref[1]
                ref_source: str = ref[2]

                # Check cname and filter out id it does not pass the filter
                if ref_type == types.CNAME and not cname_filter(ref_id):
                        continue

                if M.sources_enabled[ref_source]:
                    # Create new vertex and link with ref edge
                    u, new_vertex = M.link_identifier(
                        repr_type_A = v.type, id_A = v.cont,
                        repr_type_B = ref_type, id_B = ref_id,
                        dual = False,
                        edge = model.EdgeRef(ref_source)
                    )
                    # Add vertex to explore if it was new and is an identifier
                    if new_vertex and isinstance(u.type, types.IdentifierType):
                        new_vertices.add(u)

        # If it has ontological connections which should be fully explored, also add these
        if not toomanyonts:
            count_parent: int = 0
            for ont_ref in ont_refs:
                # Get the ontology
                relation: ontology.Relation = ont_ref[0]
                target_type: types.RepresentationType = ont_ref[1]
                target_id: str = ont_ref[2]
                source: str = ont_ref[3]

                if M.sources_enabled[source]:
                    # Create new vertex and link with on edge
                    u: model.Vertex
                    u, new_vertex = M.link_identifier(
                        repr_type_A = v.type, id_A = v.cont,
                        repr_type_B = target_type, id_B = target_id,
                        dual = relation.symmetric,
                        edge = model.EdgeOnt(relation, source)
                    )

                    # Add to vertices to explore based on case
                    if new_vertex and relation in explore_full:
                        new_vertices.add(u)

                    if relation == ontology.HAS_INSTANCE:
                        count_parent += 1

    return new_vertices


# Exploration filtering
# ======================================================================================================================
def check_cname_nofilter(cname: str) -> bool:
    return True

def check_cname_normal(cname: str) -> bool:
    # Normal strictness, no names <= 3 alphanumeric characters
    str_filtered = re.sub(r'\W+', '', cname)
    if len(str_filtered) <= 3:
        return False

    return True

def check_cname_strict(cname: str) -> bool:
    # TODO
    return check_cname_normal(cname)


# Define the filter to use, should be set using a parameter
cname_filter: Callable[[str], bool] = check_cname_normal





