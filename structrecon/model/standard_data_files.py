from __future__ import annotations

import structrecon.model.model as model
import os
import re

class database_filetype:
    name:                       str         # User-friendly interface name
    fname_default:              str         # Default filename when downloaded
    fname_actual:               str         # Actual file to look for. If empty, file is not present
    url:                        str         # Suggested URL at which to obtain the file
    download_instruct:          str         # Text to be shown for hos to download database

    @staticmethod
    def validate_file(fname: str) -> bool:
        # TODO should be implemented by each filetype separately
        pass

    @staticmethod
    def file_exists(fname: str):
        return os.path.isfile(fname)


    def __init__(self, name: str, fname_default: str, url: str, validate_funct: callable, download_instruct: str = ''):
        self.name = name
        self.fname_default = fname_default
        self.url = url
        self.download_instruct = download_instruct
        self.validate_file = validate_funct         # Set function
        self.fname_actual = fname_default           # Can be changed in runtime




# Specify database files
# ======================================================================================================================

def validate_trivial(fname: str) -> bool:
    return True

# BiGG
bigg = database_filetype(
    'BiGG',
    'bigg_models_metabolites.txt',
    'http://bigg.ucsd.edu/data_access',
    validate_trivial,
    "Download 'bigg_models_metabolites.txt' from given URL."
)


# ChEBI Accession
chebi_accession = database_filetype(
    'Chebi Accession',
    'chebi_database_accession.tsv',
    'https://ftp.ebi.ac.uk/pub/databases/chebi/Flat_file_tab_delimited/',
    validate_trivial,
    "Download 'database_accession.tsv' from FTP server at given URL."
)

# ChEBI ID to InChI
chebi_id_inchi = database_filetype(
    'Chebi ID to InChI',
    'chebiId_inchi.tsv',
    'https://ftp.ebi.ac.uk/pub/databases/chebi/Flat_file_tab_delimited/',
    validate_trivial,
    "Download 'chebiId_inchi.tsv' from FTP server at given URL. For a more curated list, use the '3star' version."
)

chebi_compounds = database_filetype(
    'Chebi compounds',
    'chebi_compounds.tsv',
    'https://ftp.ebi.ac.uk/pub/databases/chebi/Flat_file_tab_delimited/',
    validate_trivial,
    "Download 'compounds.tsv' from FTP server at given URL. For a more curated list, use the '3star' version."
)

chebi_names = database_filetype(
    'Chebi names',
    'chebi_names.tsv',
    'https://ftp.ebi.ac.uk/pub/databases/chebi/Flat_file_tab_delimited/',
    validate_trivial,
    "Download 'names.tsv' from FTP server at given URL. For a more curated list, use the '3star' version."
)

chebi_ontology = database_filetype(
    'ChEBI ontology',
    'chebi_ontology.obo',
    'https://ftp.ebi.ac.uk/pub/databases/chebi/Flat_file_tab_delimited/',
    validate_trivial,
    "Download 'chebi_ontology.obo' from FTP server at given URL. For a more curated list, use the '3star' version."
)

ecmdb = database_filetype(
    'ECMDB',
    'ecmdb.json',
    'https://ecmdb.ca/downloads',
    validate_trivial,
    "Download full database in JSON format at given URL."
)

mnx_properties = database_filetype(
    'MetaNetX Properties',
    'chem_prop.tsv',
    'https://www.metanetx.org/mnxdoc/mnxref.html',
    validate_trivial,
    "Download 'chem_prop.txt' from given URL."
)

mnx_depr = database_filetype(
    'MetaNetX Deprecation file',
    'chem_depr.tsv',
    'https://www.metanetx.org/mnxdoc/mnxref.html',
    validate_trivial,
    "Download 'chem_depr.tsv' from given URL."
)

mnx_isom = database_filetype(
    'MetaNetX Relations file',
    'chem_isom.tsv',
    'https://www.metanetx.org/mnxdoc/mnxref.html',
    validate_trivial,
    "Download 'chem_isom.tsv' from given URL."
)

pubchem_cid_cname = database_filetype(
    'Pubchem CID-Synonym-filtered file',
    'CID-Synonym-filtered',
    'TODO',  # TODO link
    validate_trivial,
    "Download 'CID-Synonym-filtered' from FTP server."
)

pubchem_cid_inchi = database_filetype(
    'Pubchem CID-InChI file',
    'CID-InChI-Key',
    'TODO',  # TODO link,
    validate_trivial,
    "Download 'CID-InChI-Key' from FTP server."
)

pubchem_sid_map = database_filetype(
    'Pubchem SID Map',
    'SID-Map',
    'https://ftp.ncbi.nlm.nih.gov/pubchem/Substance/Extras/', # TODO link
    validate_trivial,
    "Download 'SID-Map' from FTP server."
)

pubchem_cid_title = database_filetype(
    'Pubchem CID Title file',
    'CID-Title',
    'TODO', # TODO link
    validate_trivial,
    "Download 'CID-Title' from FTP server."
)

pubchem_cid_preferred = database_filetype(
    'Pubchem Preferred CID',
    'CID-Preferred',
    'TODO',  # TODO link
    validate_trivial,
    'Download CID-Preferred from FTP server.'
)

pubchem_cid_parent = database_filetype(
    'Pubchem Parent CIDs',
    'CID-Parent',
    'TODO',  # TODO link
    validate_trivial,
    'Download CID-Parent from FTP server.'
)


# Structures
# ======================================================================================================================

datafiles = [
    bigg,
    chebi_accession,
    chebi_id_inchi,
    chebi_compounds,
    chebi_names,
    ecmdb,
    mnx_properties,
    mnx_depr,
    mnx_isom,
    pubchem_cid_cname,
    pubchem_cid_title,
    pubchem_sid_map,
    pubchem_cid_preferred,
    pubchem_cid_parent
]
fname_datafile_map: dict[str, database_filetype] = dict()
for datafile in datafiles:
    fname_datafile_map[datafile.fname_default] = datafile
    datafile.fname_actual = datafile.fname_default











