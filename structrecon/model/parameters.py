""" Define the parameters of a query.
"""
from __future__ import annotations

from enum import Enum
from typing import Any, Optional, Callable

class Mode(Enum):
    WEB = 1
    CLI = 2
    API = 3


class Parameter:
    name:                   str         # Short name, used in HTML tags
    name_cli:               str         # The name of the parameter when given through the API or CLI
    name_web:               str         # The name of the parameter a shown on the web form
    tag:                    str         # Short tag for CLI (used with '-t' for '--threshold', for example)
    description:            str         # Description shown by CLI help and on the web input form

    param_type:             str         # Either 'int', 'float', 'str', 'bool', 'radio', 'check',
    value_type:             type        # Python type corresponding to the choice of parameter type
    default:                Any         # The default value

    modes:                  list[Mode]  # Modes in which this setting is available

    value_min:              Any | None  # Optional - the minimum accepted value
    value_max:              Any | None  # --
    pattern:                str | None         # If string input, give javascript-compatible regex ("^[fFiIcCtTsS]*$")
    choices:                list[str]   # If type is radio or check, list the choices available

    verify_function:        Optional[Callable[[str], bool]]    # Function to verify a string input

    template_type:          str

    def __init__(
            self,
            name: str,
            name_cli: str,
            name_web: str,
            tag: str,
            description: str,
            param_type: str,
            default: Any,
            modes: list[Mode] = None,
            choices: Optional[list[str]] = None,
            value_min: Any | None = None,
            value_max: Any | None = None,
            pattern: str | None = None,
            verify_function: Optional[Callable[[str], bool]] = None
            ):
        self.name = name
        self.name_cli = name_cli
        self.name_web = name_web
        self.tag = tag
        self.description = description
        self.param_type = param_type
        self.default = default
        self.modes = modes
        self.choices = choices
        self.value_min = value_min
        self.value_max = value_max
        self.pattern = pattern
        self.verify_function = verify_function

        if not self.modes:
            self.modes = [Mode.WEB, Mode.CLI, Mode.API]

        match self.param_type:
            case 'int': self.value_type = int
            case 'float': self.value_type = float
            case 'str': self.value_type = str
            case 'bool': self.value_type = bool
            case 'check': self.value_type = dict
            case 'radio': self.value_type = str

        # Assert that types are compatible
        assert(type(self.default) == self.value_type)


    def parse_value(self, value: str) -> Any:
        """ Given a string as value, try to parse it as the value type of this parameter. """
        if self.verify_function:
            assert(self.verify_function(value))
        # Typecast by default
        cast_value = self.value_type(value)

        # Check bounds
        if self.value_min:
            assert(cast_value >= self.value_min)
        if self.value_max:
            assert(cast_value <= self.value_max)

        return cast_value

    def get_default(self) -> Any:
        """ Get the default value for this parameter. """
        return self.default


# Specify parameters
# ----------------------------------------------------------------------------------------------------------------------
param_conf_threshold = Parameter(
    name                = 'conf_threshold',
    name_cli            = 'Threshold',
    name_web            = 'Confidence Threshold',
    tag                 = 't',
    description         = 'Defines the maximum ratio between the structures with the most and second-most confidence '
                          'which allows automatically selecting the structure with the most confidence. A smaller'
                          ' value is more strict',
    param_type          = 'float',
    default             = 0.8,
    value_min           = 0,
    value_max           = 1,
)

param_stdfunc_order = Parameter(
    name                = 'stdfunc_order',
    name_cli            = 'Standardisations',
    name_web            = 'Standardisation function order',
    tag                 = 'stdo',
    description         = 'Define which standardisation functions to use and the order in which they are applied. '
                          'Entered as a string containing the letter F, I, C, T, S, up to one time each.',
    param_type          = 'str',
    default             = 'FICTS',
    pattern             = "^[fFiIcCtTsS]*$"
)

param_stdfunc_always = Parameter(
    name                = 'stdfunc_always',
    name_cli            = 'stdfunc_always',
    name_web            = 'Always apply standardisations',
    tag                 = 'stda',
    description         = 'Specify standardisation functions to apply always, even if a compound does not need to be '
                          'standardised to have a consistent representation. '
                          'Entered as a string containing the letter F, I, C, T, S, up to one time each.',
    param_type          = 'str',
    default             = 'FI',
    pattern             = "^[fFiIcCtTsS]*$"
)

param_follow_child = Parameter(
    name                = 'follow_child',
    name_cli            = 'follow_child',
    name_web            = 'Explore child entries',
    tag                 = 'ec',
    description         = 'Explore entries linked by the \'is instance of\' relation.'
                          'Not recommended due to linking many unrelated entries.',
    param_type          = 'bool',
    default             = False
)

param_explore_deprecation = Parameter(
    name                = 'explore_depr',
    name_cli            = 'explore_depr',
    name_web            = 'Link deprecated IDs',
    tag                 = 'depr',
    description         = 'Link newer and older alternate IDs',
    param_type          = 'bool',
    default             = True
)

param_sources = Parameter(
    name                = 'sources',
    name_cli            = 'sources',
    name_web            = 'Sources',
    tag                 = 's',
    description         = 'Enable or disable each data source in the analysis.',
    param_type          = 'check',
    default             = {
        'BiGG': True,
        'ChEBI': True,
        'ECMDB': True,
        'MetaNetX': True,
        'PubChem': True
    },
    choices             = [
        'BiGG',
        'ChEBI',
        'ECMDB',
        'MetaNetX',
        'PubChem'
    ]
)

param_default_type = Parameter(
    name                = 'default_type',
    name_cli            = 'default_type',
    name_web            = 'DO NOT INCLUDE',
    tag                 = 'dt',
    description         = 'Default type to parse compounds as.',
    param_type          = 'str',
    default             = 'auto',
    modes               = [Mode.API, Mode.CLI]
)

param_ignore_ontology = Parameter(
    name                = 'ignore_ontology',
    name_cli            = 'ignore_ontoloy',
    name_web            = 'Ignore ontology relations',
    tag                 = 'io',
    description         = 'Ignore ontology relations in ChEBI and PubChem.',
    param_type          = 'bool',
    default             = False
)

param_draw_id_graphs = Parameter(
    name                = 'draw_id_graphs',
    name_cli            = 'draw_id_graphs',
    name_web            = 'Draw identifier graphs',
    tag                 = 'dig',
    description         = 'Draw identifier graphs as .svg files',
    param_type          = 'bool',
    default             = True
)

param_draw_mol_images = Parameter(
    name                = 'draw_mol_images',
    name_cli            = 'draw_mol_images',
    name_web            = 'Draw molecule images',
    tag                 = 'dmi',
    description         = 'Use RDKit to draw images of each molecule for visualisation purposes.',
    param_type          = 'bool',
    default             = True
)

param_draw_confidence_on_all = Parameter(
    name                = 'draw_conf_on_all',
    name_cli            = 'draw_conf_on_all',
    name_web            = 'Draw confidence for all nodes',
    tag                 = 'dca',
    description         = 'Draw the confidence score for all nodes, not just standardised structures. '
                          'By default, the relative confidence scores for the standard structures are shown. '
                          'Enabling this parameter shows the absolute confidence score for all nodes.',
    param_type          = 'bool',
    default             = False
)

param_make_stats_images = Parameter(
    name                = 'make_stats_images',
    name_cli            = 'make_stats_images',
    name_web            = 'Make statistical plots',
    tag                 = 'dsi',
    description         = 'Make plots representing various statistics',
    param_type          = 'bool',
    default             = True
)

param_sbml_infer = Parameter(
    name                = 'infer_sbml_ids',
    name_cli            = 'infer_sbml_ids',
    name_web            = 'Infer SBML ids',
    tag                 = 'sbmli',
    description         = 'Interpret compound names in SBML as identifiers. This may be necessary to disable '
                          'depending on how the SBML file is generated.',
    param_type          = 'bool',
    default             = True
)

param_sbml_enforce_charge = Parameter(
    name                = 'sbml_enforce_charge',
    name_cli            = 'sbml_enforce_charge',
    name_web            = 'Enforce charge from SBML file',
    tag                 = 'sbmlc',
    description         = 'If the input is SBML and a charge is given, enforce that the chosen structure has the '
                          'specified charge.',
    param_type          = 'bool',
    default             = False
)

param_sbml_standardise_charge = Parameter(
    name                = 'sbml_standardise_charge',
    name_cli            = 'sbml_standardise_charge',
    name_web            = 'Standardise charge to value in SBML file',
    tag                 = 'sbmlcs',
    description         = 'If the input is SBML and a charge is given, standardise to that specified charge.',
    param_type          = 'bool',
    default             = True
)

param_sbml_enforce_formula = Parameter(
    name                = 'sbml_enforce_formula',
    name_cli            = 'sbml_enforce_formula',
    name_web            = 'Enforce formula from SBML file',
    tag                 = 'sbmlf',
    description         = 'If the input is SBML and a formula is given, enforce that the chosen structure conforms to '
                          'the specified formula.',
    param_type          = 'radio',
    choices             = [
        'Yes, ignore hydrogen',
        'Yes, enforce hydrogen',
        'No',
    ],
    default             = 'Yes, ignore hydrogen'
)

param_ranking_decay = Parameter(
    name                = 'ranking_decay',
    name_cli            = 'ranking_decay',
    name_web            = 'Confidence ranking decay',
    tag                 = 'rankd',
    description         = 'A value between 0 and 1, indicating the decay of the confidence score, as nodes get '
                          'further from the source. A higher value more strongly favours closer nodes. '
                          'Set to 0 to use the ranking algorithm without this modification.',
    param_type          = 'float',
    default             = 0.05,
    value_min           = 0,
    value_max           = 1
)

param_std_threshold = Parameter(
    name                = 'standardisation_threshold',
    name_cli            = 'standardisation_threshold',
    name_web            = 'Standardisation threshold',
    tag                 = 'stdt',
    description         = 'A value between 0 and 1. When choosing whether to standardise a structure, StructRecon '
                          'will not consider alternate structures with relative confidence less than this '
                          'value.',
    param_type          = 'float',
    default             = 0.2,
    value_min           = 0,
    value_max           = 1
)

param_ranking_depr_weight = Parameter(
    name                = 'depr_weight',
    name_cli            = 'depr_weight',
    name_web            = 'Ranking weight of deprecation edges',
    tag                 = 'deprw',
    description         = 'A value between 0 and 1, indicating the edge weight used when considering ID deprecation '
                          'relations within a database.',
    param_type          = 'float',
    default             = 0.5,
    value_min           = 0,
    value_max           = 1
)

param_ranking_cname = Parameter(
    name                = 'common_name_weight',
    name_cli            = 'common_name_weight',
    name_web            = 'Ranking weight of common names',
    tag                 = 'cnamew',
    description         = 'A value between 0 and 1, indicating the weight of edges from Common Name nodes. A lower '
                          'value may reduce spurious correlations. Set to 0 to automatically exclude nodes only '
                          'connected by common names.',
    param_type          = 'float',
    default             = 0.75,
    value_min           = 0,
    value_max           = 1
)

param_max_explore_steps = Parameter(
    name                = 'max_explore_steps',
    name_cli            = 'max_explore_steps',
    name_web            = 'Maximum exploration steps',
    tag                 = 'esmax',
    description         = 'Maximum number of steps to traverse the identifier graph.',
    param_type          = 'int',
    default             = 20
)

param_sample_random_subset = Parameter(
    name                = 'sample_random_subset',
    name_cli            = 'sample_random_subset',
    name_web            = 'Only process subset',
    tag                 = 'samplerandomsubset',
    description         = 'Set this value less that 1 to only sample a fraction of the input compoundss.'
                          'This should mainly be used to get quicker processing times in order to adjust'
                          'the parameters.',
    param_type          = 'float',
    default             = 1.0,
    value_min           = 0.0,
    value_max           = 1.0
)

param_show_inherited_explicit_properties = Parameter(
    name                = 'show_inherited_explicit_properties',
    name_cli            = 'show_inherited_explicit_properties',
    name_web            = 'Show inherited explicit properties',
    tag                 = 'shep',
    description         = 'Show properties marked as explicit in the ancestors of each structure.',
    param_type          = 'bool',
    default             = False
)

param_show_generation = Parameter(
    name                = 'show_generation',
    name_cli            = 'show_generation',
    name_web            = 'Show generation',
    tag                 = 'shep',
    description         = 'Show the steps necessary to discover each structure.',
    param_type          = 'bool',
    default             = False
)

param_show_preliminary_candidates = Parameter(
    name                = 'show_preliminary_candidates',
    name_cli            = 'show_preliminary_candidates',
    name_web            = 'Show preliminary candidates',
    tag                 = 'shpc',
    description         = 'Show the vertices marked as preliminary candidates. See the paper for more details.',
    param_type          = 'bool',
    default             = False
)

param_show_support = Parameter(
    name                = 'show_support',
    name_cli            = 'show_support',
    name_web            = 'Show support of structure vertices',
    tag                 = 'sppt',
    description         = 'See paper for details.',
    param_type          = 'bool',
    default             = False
)

param_show_noncentral = Parameter(
    name                = 'show_noncentral',
    name_cli            = 'show_noncentral',
    name_web            = 'Show extra vertices',
    tag                 = 'shnc',
    description         = 'Show vertices only related by ontological relations.',
    param_type          = 'bool',
    default             = False
)


parameters_all = [
    param_conf_threshold,
    param_stdfunc_order,
    param_stdfunc_always,
    param_std_threshold,
    param_explore_deprecation,
    param_sources,
    param_default_type,
    param_ignore_ontology,
    param_draw_id_graphs,
    param_draw_mol_images,
    param_draw_confidence_on_all,
    param_make_stats_images,
    param_sbml_infer,
    param_sbml_standardise_charge,
    param_sbml_enforce_charge,
    param_sbml_enforce_formula,
    param_ranking_decay,
    param_ranking_depr_weight,
    param_ranking_cname,
    param_sample_random_subset,
    param_max_explore_steps,
    param_show_noncentral,
    param_follow_child,
    param_show_inherited_explicit_properties,
    param_show_generation,
    param_show_support,
    param_show_preliminary_candidates
]

# Define which parameters to make available in each interface
# ----------------------------------------------------------------------------------------------------------------------
parameters_cli = [param for param in parameters_all if Mode.CLI in param.modes]
parameters_web = [param for param in parameters_all if Mode.WEB in param.modes]
parameters_api = [param for param in parameters_all if Mode.API in param.modes]
parameters_mode = {
    Mode.CLI: parameters_cli,
    Mode.WEB: parameters_web,
    Mode.API: parameters_api
}

parameters_by_name: dict[str, Parameter] = dict()
for param in parameters_all:
    parameters_by_name[param.name] = param
    parameters_by_name[param.name_web] = param
    parameters_by_name[param.name_cli] = param
