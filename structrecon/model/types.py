from __future__ import annotations
from typing import Optional
from typing import Callable
import re

###############################################################################################################
# Defs
###############################################################################################################

# Standard standardiser
def stdfunc_identity(identifier: str) -> str:
    return identifier.strip()

# Either an identifier or structure
class RepresentationType:
    """ Base class for representation types. """
    # TODO split the representation type, s.t. maps are part of the model instead?
    name:               str                 # Common name of this
    shortname: str  # Short name, used in table columns etc, lowercase, underscored
    alt_names:          list[str]                 # Used for input - lowercase, underscored
    prefixes:           list[str]           # List of prefixes to remove from input strings
    suffixes:           list[str]           # List of suffixes to remove from input strings
    stdfunc:            Callable[[str], str]
    recogniser:         re.Pattern | None          # Pattern to recognise an identifier of this type.

    def __repr__(self):
        return self.name

    def trim(self, s: str) -> str:
        """ Remove pre- and suffixes of the string. """
        s = s.strip()
        for prefix in self.prefixes:
            s = s.removeprefix(prefix)
        for suffix in self.suffixes:
            s = s.removesuffix(suffix)
        return s


# model a type of identifier (BIGG, KEGG; MetaNetX, Pubchem CID etc)
class IdentifierType(RepresentationType):
    id_index:           int             # Index of this type, used when getting a list (e.g. id_map[kegg][1234])
    identifiers_org_prefix: str

    def __init__(
            self,
            name: str,
            shortname: str,
            alt_names: list[str],
            prefixes: list[str] = None,
            suffixes: list[str] = None,
            stdfunc: Callable[[str], str] = stdfunc_identity,
            recogniser: re.Pattern | None = None
    ):
        self.name = name
        self.shortname = shortname
        self.alt_names = alt_names
        self.explorers = list()
        self.stdfunc = stdfunc
        self.recogniser = recogniser

        # Set post- and prefixes
        self.prefixes = prefixes if prefixes else []
        self.suffixes = suffixes if suffixes else []

        # Make identifiers org link
        self.identifiers_org_prefix = ''
        for prefix in self.prefixes:
            if prefix.startswith('http://identifiers.org/'):
                self.identifiers_org_prefix = prefix
                if not self.identifiers_org_prefix.endswith('/'):
                    self.identifiers_org_prefix += '/'


    def __int__(self):
        return self.id_index


# model a type of structural identifier (SMILES, SMILES+stereo, InChI, RDKIT molecule(?)...)
class StructureType(RepresentationType):
    st_index:           int             # Index of this type in structure lists

    def __init__(
            self,
            name: str,
            shortname: str,
            alt_names: list[str],
            prefixes: list[str] = None,
            suffixes: list[str] = None,
            stdfunc: Callable[[str], str] = stdfunc_identity,
            recogniser: re.Pattern | None = None
    ):
        self.name = name
        self.shortname = shortname
        self.alt_names = alt_names
        self.explorers = list()
        self.stdfunc = stdfunc
        self.recogniser = recogniser

        # Set post- and prefixes
        self.prefixes = prefixes if prefixes else []
        self.suffixes = suffixes if suffixes else []

    def __int__(self):
        return self.st_index


# model a vertex which identifies one of the compounds in the input file:
class InputType(RepresentationType):
    """ The input type is a special representation type which is always present in models. """
    def __init__(self):
        self.name = 'INPUT'
        self.alt_names = ['input']
        self.shortname = 'input'
        self.prefixes = []
        self.suffixes = []
        self.stdfunc = stdfunc_identity
        self.recogniser = None


###############################################################################################################
# Identifier standardisation functions
###############################################################################################################

# BiGG
compartment_suffixes = [
    '_e', '_ex', '_c', '_p', '_m', '_x', '_b',
    '_E', '_EX', '_C', '_P', '_M', '_X', '_B'
]
def universalise_bigg(bigg: str) -> str:
    """ Create a universal BIGG id without compartment"""
    new_bigg = bigg.strip()
    for suffix in compartment_suffixes:
        new_bigg = new_bigg.removesuffix(suffix)
    new_bigg = new_bigg.replace('_DASH', '')     # Fix weird 'DASH' notation, i.e. M_12ppd_DASH_S -> M_12ppd__S
    new_bigg = new_bigg.replace('__', '_')  # Hacky fix for double udnerscore notation
    new_bigg = new_bigg.removeprefix('m_')
    if not new_bigg.startswith('M_'):
        new_bigg = f'M_{new_bigg}'

    if new_bigg.isupper():
        # Fix upper case
        new_bigg = f'M_{new_bigg.removeprefix("M_").lower()}'

    return new_bigg

def standardise_chebi(chebi: str) -> str:
    """ PREFIX WITH CHEBI: """
    if not chebi:
        return ''
        #raise ValueError(f'Received empty CHEBI')
    chebi_new = chebi.strip()
    if chebi_new.startswith('http://identifiers.org/chebi/CHEBI'):
        return chebi_new.removeprefix('http://identifiers.org/chebi/')
    if chebi_new.startswith('https://identifiers.org/chebi/CHEBI'):
        return chebi_new.removeprefix('https://identifiers.org/chebi/')
    if chebi_new.isnumeric():
        return f'CHEBI:{chebi_new}'
    elif chebi_new.startswith('CHEBI:'):
        return chebi_new
    else:
        raise ValueError(f'Malformed ChEBI: {chebi}')

# CNAME
def standardise_cname(cname: str) -> str:
    """ Lowercase, etc """
    return cname.lower().strip()


########################################################################################################################
# Define standard identifier and representation types
########################################################################################################################

# Input type is generic
INPUT       = InputType()

# Meta-type (auto)
AUTO = IdentifierType(
    'Automatically infer',
    'auto',
    []
)

# Common name
CNAME       = IdentifierType(
    'Common name',
    'cname',
    ['common_name', 'cname'],
    stdfunc = standardise_cname,
    recogniser = re.compile(r'^[^\n\r]+$')
    )

# Metanetx
MNX         = IdentifierType(
    'MetaNetX',
    'mnx',
    ['metanetx', 'mnx'],
    prefixes = [
        'http://identifiers.org/metanetx.chemical/',
        'https://identifiers.org/metanetx.chemical/'
    ],
    recogniser = re.compile(r'^(MNXM\d+|MNX\d+|BIOMASS|WATER)$')
)

# BIGG
BIGG        = IdentifierType(
    'BIGG',
    'bigg',
    ['bigg'],
    prefixes = [
        'http://bigg.ucsd.edu/models/universal/metabolites/',
        'http://identifiers.org/bigg.metabolite/',
        'https://identifiers.org/bigg.metabolite/',
        'bigg.metabolite:'
    ],
    suffixes= [
        '_e', '_ex', '_c', '_p', '_m', '_x', '_b'
    ],
    stdfunc = universalise_bigg,
    recogniser = re.compile(r'^[a-z_A-Z0-9]+$')
)

# Pubchem
PC_CID      = IdentifierType(
    'PubChem',
    'pubchem_cid',
    ['pubchem', 'pc_cid', 'cid'],
    prefixes = [
        'http://identifiers.org/pubchem.compound/',
        'https://identifiers.org/pubchem.compound/'
    ],
    recogniser = re.compile(r'^\d+$')
)

# InChI
INCHI_KEY   = IdentifierType(
    'Inchi Key',
    'inchi_key',
    ['inchi', 'inchi_key'],
    prefixes = [
        'http://identifiers.org/inchikey/'
        'https://identifiers.org/inchikey/'
    ],
    recogniser = re.compile(r'^[A-Z]{14}-[A-Z]{10}(-[A-Z])?$')
)

# KEGG
KEGG        = IdentifierType(
    'KEGG',
    'kegg',
    ['kegg'],
    prefixes = [
        'http://identifiers.org/kegg.compound/',
        'https://identifiers.org/kegg.compound/'
    ],
    recogniser = re.compile(r'^C\d+$')
)

# ChEbI
CHEBI       = IdentifierType(
    'CHEBI',
    'chebi',
    ['chebi'],
    prefixes = [
        'http://identifiers.org/chebi/',
        'https://identifiers.org/chebi/'
    ],
    recogniser = re.compile(r'^CHEBI:\d+$'),
    stdfunc = standardise_chebi
)

# ECMDB
ECMDB       = IdentifierType(
    'ECMDB',
    'ecmdb',
    ['ecmdb'],
    recogniser = re.compile(r'^ECMDB\d+$')
)

FORMULA     = StructureType(
    'Sum Formula',
    'sumformula',
    ['formula']
)

SMILES      = StructureType(
    'SMILES',
    'smiles',
    ['smiles']
)           # TODO

INCHI       = StructureType(
    'InChi',
    'inchi',
    ['inchi'],
    prefixes = [],
    recogniser = re.compile(r'^InChI=\S+$')
)


standard_repr_types: list[RepresentationType] = [
    INPUT, CNAME, MNX, BIGG, PC_CID, INCHI_KEY, KEGG, CHEBI, ECMDB, FORMULA, SMILES, INCHI
]

string_type_map: dict[str, RepresentationType] = {'auto': AUTO}
for repr_type in standard_repr_types:
    string_type_map[repr_type.shortname] = repr_type
    for alt_name in repr_type.alt_names:
        string_type_map[alt_name] = repr_type

def get_type_from_str(repr_type_str: str) -> RepresentationType | None:
    got = string_type_map.get(repr_type_str.lower())
    if got:
        return got
    else:
        return None

def get_canonical_type_name(type_str: str) -> str:
    """ Get the canonical name of a type to use in dicts. """
    return string_type_map.get(type_str).shortname


def get_possible_types_regex(identifier: str) -> list[RepresentationType]:
    """ Gives a list of possible types for the identifier using the regexes of each type. """
    matches: list[RepresentationType] = []
    for t in standard_repr_types:
        if t.recogniser and re.match(t.recogniser, identifier):
            matches.append(t)
    return matches


