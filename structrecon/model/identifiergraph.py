from __future__ import annotations

import networkx as nx
import logging
from typing import Optional
import psycopg

import structrecon.model.model as model
import structrecon.model.parameters as parameters
import structrecon.model.types as types
import structrecon.model.explore as explore
import structrecon.model.standardise as standardise
import structrecon.model.app_query as app_query
import structrecon.model.ontology as ontology
import structrecon.cache.cache as cache

""" Serves as a wrapper for identifier graph types. """

class IdentifierGraph:
    M:              model.Model
    compound:       app_query.CompoundInput
    graph:          nx.MultiDiGraph
    log:            logging.Logger

    v_central:      set[model.Vertex]       # List of central vertices

    explicit_props: dict[model.Vertex, set[str]]                # List of properties (F,I,C,T,S), for which a given vertex is explicit
    explicit_props_inherited: dict[model.Vertex, list[set[str]]] # For structures

    support:        dict[model.Vertex, set[model.Vertex]]   # The predecessors of each structure vertex

    # Candidate structures
    preliminary_candidates:     set[model.Vertex]   # Vertices which are fully standardised according to their explicit
                                                    # properties are considered preliminary candidates

    candidates:                 set[model.Vertex]   # Final candidates are chosen via an algorithm based on the
                                                    # preliminary candidates


    # The vertex map contains a dictionary, where for each type, there is a string->vertex dict
    vertex_map: dict[types.RepresentationType, dict[str, model.Vertex]]

    def __init__(
            self,
            compound: app_query.CompoundInput,
            graph: nx.MultiDiGraph,
            M: model.Model
    ):
        self.compound = compound
        self.graph = graph
        self.M = M
        self.log = M.log
        self.v_central = set()
        self.explicit_props = dict()
        self.explicit_props_inherited = dict()
        self.candidates = set()
        self.preliminary_candidates = set()
        self.support = dict()

        # Make new vertex_map
        self.vertex_map = dict()
        for repr_type in M.vertex_map.keys():
            subdict: dict[str, model.Vertex] = dict()
            for s, v in M.vertex_map[repr_type].items():
                if v in self.graph.nodes():
                    subdict[s] = v
            self.vertex_map[repr_type] = subdict

        # Fix dangling CNAME nodes
        nodes_remove: list = []
        for v in self.graph.nodes:
            if v.type == types.CNAME and self.graph.degree[v] <= 1:
                nodes_remove.append(v)
        self.log.info(f'Removing {len(nodes_remove)} dangling CNAME nodes')
        for v in nodes_remove:
            self.graph.remove_node(v)


        # Finally, do post-explore
        self.post_explore()

        # Set explicit properties
        self.set_explicit_properties()


    def check_exists(self, repr_type: types.RepresentationType, identifier: str) -> Optional[model.Vertex]:
        """ Check if a vertex already exists, and return it. Otherwise None"""
        check_id = repr_type.stdfunc(identifier)

        # Check the corresponding map
        if check_id in self.vertex_map[repr_type].keys():
            return self.vertex_map[repr_type][check_id]
        else:
            return None


    def add_identifier(self, repr_type: types.RepresentationType, id: str) -> model.Vertex:
        """ Add a new identifier and return the created vertex, or return existing vertex."""
        check = self.check_exists(repr_type, id)
        if check:
            # Already exists
            return check
        else:
            v = model.Vertex(self.M, repr_type, id, idgraph = self)
            self.graph.add_node(v)
            return v


    def link_identifier(self,
                        repr_type_A: types.RepresentationType, id_A: str,
                        repr_type_B: types.RepresentationType, id_B: str,
                        dual: bool,                 # If true, add edges in both directions
                        edge: model.Edge,             # Set edge label
                        ) -> (model.Vertex, bool):
        """ Find identifier A in graph and link new identifier B, with optional info to explain the connection.
            The second return argument is False if B already existed, and True if B is newly created.
        """
        # Check if the A vertex already exists
        u = self.check_exists(repr_type_A, id_A)

        # DEBUG
        if not u:
            # Reconstruct vertex map and check again
            self.log.info(f'Reconstructing vertex map for ')
            self.vertex_map[repr_type_A] = {
                v.cont: v
                for v in filter(lambda v_: v_.type is repr_type_A, self.graph.nodes())
            }
            u = self.check_exists(repr_type_A, id_A)

            if not u:
                self.log.warning(f' Attempted linking {repr_type_A.name}, {id_A}, but it does not exist!')
                self.log.warning(f'Dict:')
                self.log.warning(f'  Keys: {sorted(self.vertex_map[repr_type_A].keys())}')
                self.log.warning(f' vertices of type:')
                for v in self.graph.nodes():
                    if v.type == repr_type_A:
                        self.log.warning(f'  - {v}')
                return None, False
        assert u

        # Check if B exists
        B_already_exists = self.check_exists(repr_type_B, id_B)
        if B_already_exists:
            v = B_already_exists
        else:
            v = model.Vertex(self.M, repr_type_B, id_B, idgraph = self)
            self.graph.add_node(v)

        # Add link (edge) with given info as label
        self.graph.add_edge(u, v, label = edge)
        if dual:
            self.graph.add_edge(v, u, label = edge)
        return v, not B_already_exists


    def link_vertices(self, u: model.Vertex, v: model.Vertex, edge: model.Edge, dual: bool = False):
        """ Given two already present vertices of the id graph, link them. """
        self.graph.add_edge(u, v, label=edge)
        if dual:
            self.graph.add_edge(v, u, label=edge)

    def post_explore(self):
        # Explores aux nodes specific to this identifier graph.

        # TODO
            # This function should be considered temporary until a more robust exploration system for each
            # individual identifier graph can be implemented.

        # Explores one step for 'info' ontologies to establish explicit relations for the nodes
        new_entries: set[model.Vertex] = set()

        # Set explicit properties to empty
        for v in self.graph.nodes():
            if v not in self.explicit_props.keys():
                self.explicit_props[v] = set()

        to_explore: set[model.Vertex] = set(filter(
            lambda v: isinstance(v.type, types.IdentifierType),
            self.graph.nodes()
        ))

        # Compute central vertices
        self.v_central = set()
        for v in self.graph.nodes():
            self.v_central.add(v)

        # Make new list of central vertices, defined as those reachable by central vertices from the source
        v_central_new: set[model.Vertex] = set()

        if not to_explore:
            return

        # Get from cache
        result_map: dict[
            (types.IdentifierType, str),
            (
                list[(types.RepresentationType, str, str)],
                str,
                dict[str, str],
                list[(ontology.Relation, types.RepresentationType, str, str)]
            )] = dict()
        chunk_size = 100
        vs_list = list(to_explore)

        # Divide into 100-item chunks and update result map for each chunk
        i = 0
        vs_chunk: list[model.Vertex]
        while i < len(vs_list):
            vs_chunk = vs_list[i:min(i + chunk_size, len(vs_list))]
            result_map.update(cache.get_connections_and_properties(
                self.M.conn,
                [(v.type, v.cont) for v in vs_chunk]
            ))
            i += chunk_size

        for v in to_explore:
            # Add new vertices by ontology.
            # --------------------------------------------------------------------------------------
            if (v.type, v.cont) not in result_map.keys():
                continue
            self.explicit_props[v] = set()

            result_map_get = result_map[(v.type, v.cont)]
            ont_refs = result_map_get[3]

            for ont_ref in ont_refs:
                relation: ontology.Relation = ont_ref[0]
                target_type: types.RepresentationType = ont_ref[1]
                target_id: str = ont_ref[2]
                source: str = ont_ref[3]

                if self.M.sources_enabled[source]:
                    # Create new vertex depending on the ontology
                    if relation in self.M.explore_full or relation in self.M.explore_noncentral:
                        # Add new vertex
                        u, new_vertex = self.link_identifier(
                            repr_type_A = v.type, id_A = v.cont,
                            repr_type_B = target_type, id_B = target_id,
                            dual = relation.symmetric,
                            edge = model.EdgeOnt(relation, source)
                        )

                        # Add to new vertices
                        if new_vertex:
                            self.explicit_props[u] = set()
                            new_entries.add(u)

                            if relation in (ontology.CONJUGATE_BASE, ontology.CONJUCATE_ACID):
                                self.explicit_props[u].add('C')
                            elif relation == ontology.TAUTOMER:
                                self.explicit_props[u].add('T')
                            elif relation in (ontology.ENANTIOMER, ontology.STEREOISOMER):
                                self.explicit_props[u].add('S')

                    elif relation in self.M.explore_info:
                        # Only add ontology between existing edges
                        # TODO
                            # TODO maybe move to the post-explore step
                        pass

                # Set explicit properties based on the ontology
                if relation in (ontology.CONJUGATE_BASE, ontology.CONJUCATE_ACID):
                    self.explicit_props[v].add('C')
                elif relation == ontology.TAUTOMER:
                    self.explicit_props[v].add('T')
                elif relation in (ontology.ENANTIOMER, ontology.STEREOISOMER):
                    self.explicit_props[v].add('S')

        if not new_entries:
            return

        # Next, find structures associated with new entries
        # ------------------------------------------------------------------------------------------
            # TODO also set names!
            # TODO add references to other, existing identifiers
        result_map = cache.get_connections_and_properties(
            self.M.conn,
            [(v.type, v.cont) for v in new_entries]
        )

        for v in new_entries:
            if (v.type, v.cont) not in result_map.keys():
                continue

            result_map_get = result_map[(v.type, v.cont)]
            result_map_get = result_map[(v.type, v.cont)]
            refs = result_map_get[0]
            name = result_map_get[1]
            props = result_map_get[2]
            ont_refs = result_map_get[3]

            # Set name and properties
            v.name = name
            v.properties = props

            # Set references
            for ref in refs:
                # Check if source (ref_source) is enabled in the settings
                ref_type: types.IdentifierType = ref[0]
                ref_id: str = ref[1]
                ref_source: str = ref[2]

                if self.M.sources_enabled[ref_source]:
                    if isinstance(ref_type, types.IdentifierType):
                        # Only link if to another new entry
                        pass
                    elif isinstance(ref_type, types.StructureType):
                        u, new_vertex = self.link_identifier(
                            repr_type_A = v.type, id_A = v.cont,
                            repr_type_B = ref_type, id_B = ref_id,
                            dual = False,
                            edge = model.EdgeRef(ref_source)
                        )

        # Finalise

    def set_explicit_properties(self):
        """ For all vertices in this identifier graph, compute all explicit properties from the ontology. """

        for v in self.graph.nodes():
            # First, set explicit properties if not already set
            if v not in self.explicit_props.keys():
                self.explicit_props[v] = set()

            # TODO check differences with parent if it has an is_a / has_instance relation

            # Also perform name-based analysis
                # TODO
                # TODO - if name ends with (2-), it is charge-explicit
                # TODO - if name contains meso-, L- , etc, it is stereo-explicit
                # TODO - if name contains 'deutarated', it is isotope-explicit