from __future__ import annotations
from typing import Type
import logging
import copy

import networkx as nx
from rdkit.Chem import AllChem as rdk
import rdkit.Chem.MolStandardize.rdMolStandardize as rdk_std
from rdkit import RDLogger

import structrecon.model.model as model
import structrecon.model.identifiergraph as identifiergraph
import structrecon.model.parameters as parameters
import structrecon.model.types as types
import structrecon.model.chem as chem
import structrecon.model.app_query as app_query

# Main standardisation function
# ==================================================================================================================

class StdException(Exception):
    pass

# Target to standardise
charge_target: int = 0

def standardise(
        M: model.Model
):
    query = M.query
    log: logging.Logger = M.log

    # Change logging settings for RDKit (DEBUG, uncomment to enable)
    RDLogger.DisableLog('rdApp.*')

    # Parse sequence
    procs_always: str = query.parameters[parameters.param_stdfunc_always]
    procs: str = query.parameters[parameters.param_stdfunc_order]
    log.info(f'  - Using functions: {procs}')
    log.info(f'  - Always standardise: {procs_always}')

    proc_seq: list[Type[StandardisationProcedure]] = [Procedure_cleanup]
    proc_map: dict[str, Type[StandardisationProcedure]] = {
        'F': Procedure_defragment,
        'I': Procedure_isotope,
        'C': Procedure_uncharge,
        'T': Procedure_tautomer_canon,
        'S': Procedure_stereo
    }

    for char in procs:
        try:
            proc_seq.append(proc_map[char])
        except KeyError as e:
            log.warning(f'Invalid standardisation sequence: {e}')
            raise

    log.info('Standardisation sequence:')
    for proc in proc_seq:
        log.info(f'  [{proc.flag}] {proc.name}')

    # Standardise all compound graphs
    for c, G in M.id_graph_input.items():
        standardise_identifier_graph(M, c, G, log, procs, proc_seq, proc_map)


# Standardisation procedure
# ==================================================================================================
def standardise_identifier_graph(
        M: model.Model,
        c: app_query.CompoundInput,
        G: identifiergraph.IdentifierGraph,
        log: logging.Logger,
        procs: str,
        proc_seq: list[Type[StandardisationProcedure]],
        proc_map: dict[str, Type[StandardisationProcedure]],
):
    # 1. Initialise
    # ----------------------------------------------------------------------------------------------
    query = M.query
    graph = G.graph
    central: set[model.Vertex] = G.v_central
    log.info(f'Starting standardisation procedure for compound {c.source_id}')

    # Charge standardise to specified value
    if query.parameters[parameters.param_sbml_standardise_charge]:
        try:
            charge_target = int(c.properties.get('charge', '0'))
        except KeyError as e:
            charge_target = 0

    # X. Compute support of all initial vertices
    # ------------------------------------------------------------------------------------------
    for v in filter(lambda v_: isinstance(v_.type, types.StructureType), graph.nodes()):
        support: set[model.Vertex] = set()
        for u in graph.reverse().neighbors(v):
            if u in central:
                if isinstance(u.type, types.IdentifierType):
                    support.add(u)
        G.support[v] = support

    # 2. Handle SMILES
    # ----------------------------------------------------------------------------------------------
    v: model.Vertex
    smileslist: list[model.Vertex] = list(filter(lambda v_: v_.type is types.SMILES, graph.nodes()))
    for v in smileslist:
        inchi: str = smiles_to_inchi(v.cont)
        if inchi == '':
            continue

        u, new_vertex = G.link_identifier(
            v.type,
            v.cont,
            types.INCHI,
            inchi,
            dual = False,
            edge = model.EdgeTransformation(),
        )

        if v in central:
            central.add(u)

    # 3. Remove intermediate SMILES
    # ----------------------------------------------------------------------------------------------
        # If an identifier vertex points to both a SMILES and InChI, of which the SMILES maps to
        # the InChI, we do not want to keep the SMILES

    # TODO


    # 4. Calculate inherited explicit properties for all structures
    # ----------------------------------------------------------------------------------------------
    # For each structure, keep a list of parent properties. Each parent property is a set of
    # strings, such as 'F', 'I', etc. Each such set corresponds to one parent of the structure
    for v in filter(lambda v_: v_.type in (types.INCHI, types.SMILES), graph.nodes()):
        G.explicit_props_inherited[v] = list()
        for v_parent in graph.reverse().neighbors(v):
            try:
                props = G.explicit_props[v_parent]
                if props not in G.explicit_props_inherited[v]:
                    G.explicit_props_inherited[v].append(props)
            except KeyError:
                pass

    # Repeat to catch SMILES -> InCHI conversions
    for v in filter(lambda v_: v_.type is types.INCHI, graph.nodes()):
        for v_parent in filter(lambda v_: v_.type is types.SMILES, graph.reverse().neighbors(v)):
            props = G.explicit_props_inherited[v_parent]
            for prop in props:
                if prop not in G.explicit_props_inherited[v]:
                    G.explicit_props_inherited[v].append(prop)


    # 5. Perform standardisation steps until no structures are left
    # ----------------------------------------------------------------------------------------------
    # Keep a queue of structures to process, and pop from this while not empty
    struct_queue: list[model.Vertex] = list()
    for v in G.v_central:
        if v and v.type is types.INCHI:
            struct_queue.append(v)

    while struct_queue:
        v = struct_queue.pop()
        if v.std_flags == '':
            set_std_flags(v, proc_seq, log)
        flags = [f for f in list(v.std_flags) if f in {'F', 'I', 'C', 'T', 'S'}]

        # Standardise according to the explicit properties set
        already_standardised: dict[str, model.Vertex] = dict()

        # TODO implicit standardisation edges?

        for prop_set in G.explicit_props_inherited[v] + [set()]:
            # Prop set like {C, S}, or {}. Standardise according to the first non-standard feature
            # not in each prop-set.
            std_feat: str = ''  # The feature by which to standardise
            for c in flags:
                if (c not in prop_set):
                    std_feat = c
                    break
            else:
                # If the loop is allowed to complete, the structure is considered a candidate,
                # as it is standardised according to all parameters in which it is not explicit.
                G.preliminary_candidates.add(v)
                continue

            # Standardise according to feature std_feat
            u: model.Vertex  # New vertex
            if std_feat not in already_standardised.keys():
                # Standardise and create new vertex
                mol = rdk.MolFromInchi(v.cont)
                set_vertex_properties(v, mol)

                if mol is None:
                    log.warning(f'  Standardisation error at {v.cont}: initial mol is None')
                    continue

                try:
                    mol = proc_map[std_feat].standardise(mol)
                except ValueError as e:
                    log.warning(f'  Standardisation error at {v.cont}: {e}')
                    continue

                # Catch case where molecule generation failed
                if mol is None:
                    log.warning(f'  Standardisation error at {v.cont}: resulting mol is None')
                    continue

                # Generate new inchi
                inchi_new = rdk.MolToInchi(mol, options = '')

                # If not changes happened, break
                if v.cont == inchi_new:
                    log.warning(f'  Standardisation error at {v.cont}: No changes to mol')
                    # TODO Set flags for this compound in this case! THIS HAPPENS
                    continue

                # Create new vertex
                u, new_vertex = G.link_identifier(
                    v.type,
                    v.cont,
                    types.INCHI,
                    inchi_new,
                    dual = False,
                    edge = model.EdgeStd(std_feat)
                )

                # Compute new flags for new vertex
                if new_vertex or u not in G.v_central:
                    struct_queue.append(u)
                    G.explicit_props_inherited[u] = [set()]
                    set_vertex_properties(u, mol)

                    # Set flags for new vertex
                    set_std_flags(u, proc_seq, log)

                G.v_central.add(u)

                # If this feature was not part of the corresponding feat-set, inherit the explicit features
                if std_feat not in prop_set:
                    if prop_set not in G.explicit_props_inherited[u]:
                        G.explicit_props_inherited[u].append(prop_set)

                # Note that v was already standardised according to feature std_feat, giving vertex u
                already_standardised[std_feat] = u

                # Append new structure to queue

            else:
                # Already standardised according to this feature
                u = already_standardised[std_feat]
                if std_feat not in prop_set:
                    if prop_set not in G.explicit_props_inherited[u]:
                        G.explicit_props_inherited[u].append(prop_set)

                # TODO also check whether implicit edge should be changed to explicit


    # X. Update support by propagating
    # ------------------------------------------------------------------------------------------
    structs_sup: dict[model.Vertex, set[model.Vertex]] = dict()

    # Generate list of all structs with support
    for v in filter(lambda v_: isinstance(v_.type, types.StructureType), graph.nodes()):
        try:
            structs_sup[v] = G.support[v]
        except KeyError:
            continue

    # Propagate information
    for v, sup in structs_sup.items():
        for u in nx.descendants(graph, v):
            try:
                G.support[u].update(sup)
            except KeyError:
                G.support[u] = set()
                G.support[u].update(sup)


# Smiles to InChI converter
# ==================================================================================================================

def smiles_to_inchi(smiles: str) -> str:
    mol = rdk.MolFromSmiles(smiles)
    if mol is None:
        return ''
    inchi = rdk.MolToInchi(mol, options = '')
    if not inchi:
        return ''
    return inchi


# Vertex processor
# ======================================================================================================================

def set_vertex_properties(v: model.Vertex, mol: rdk.Mol) -> None:
    """ Set charge and formula of a vertex. Only call on InChI nodes """
    formula: str | None
    charge: int | None

    # Formula
    try:
        formula = rdk.CalcMolFormula(mol, abbreviateHIsotopes = False)
    except Exception:
        formula = None

    # Charge
    try:
        charge = rdk.GetFormalCharge(mol)
    except Exception:
        charge = None

    # Set vertex properties
    if formula:
        v.properties['formula'] = formula
    if charge is not None:
        v.properties['charge'] = str(charge)




# Standardisation flags checker
# ==================================================================================================================

def has_flags(v: model.Vertex, flags: str) -> bool:
    """ Returns true if v is standardised according to the given flags. """
    for f in flags:
        if f not in v.std_flags:
            return False
    return True


def set_std_flags(v: model.Vertex, subprocedure_list: list[Type[StandardisationProcedure]], log: logging.Logger):
    """ For all procedures described here, set the flags of the vertex. """
    if v.type not in {types.INCHI, types.SMILES}:
        return

    v.std_flags = ''

    subprocedure: Type[StandardisationProcedure]
    for subprocedure in subprocedure_list:
        if subprocedure.flag != '':     # Subprocedures with no flags do not need to be checked
            try:
                if subprocedure.check_string(v.type, v.cont):
                    v.std_flags += '-'
                else:
                    v.std_flags += subprocedure.flag
            except ValueError as e:
                # Something went wrong during checking
                log.warning(f'Standard flag checker error for {v.cont}:\n\t{e}')
                v.std_flags += '[CHECKERROR]'


# Standardisation procedures
# ==================================================================================================================

class StandardisationProcedure:
    """ Represents some type of standardisation (e.g. removing acid/base, tautomer standardisation). """
    flag: str  # Single letter used to denote this procedure - leave empty to not display
    name: str

    @staticmethod
    def standardise(m: rdk.Mol) -> rdk.Mol:
        """ Return a new molecule standardised according to the criterion. """
        pass

    @staticmethod
    def check_string(repr_type: types.RepresentationType, s: str) -> bool:
        """ Check whether a given representation is already standardised according to this criterion. """
        pass

    @staticmethod
    def check_and_standardise(v: model.Vertex) -> model.Vertex:
        """ Has a default implementation, but may be overridden if a more efficient one is found.
            Returns a new vertex if it was standardised. """
        # TODO default implementation
        pass


# Cleanup - always to be performed
class Procedure_cleanup(StandardisationProcedure):
    flag = ''
    name = 'Cleanup'

    @staticmethod
    def check_string(repr_type: types.RepresentationType, s: str) -> bool:
        return True

    @staticmethod
    def standardise(m: rdk.Mol) -> rdk.Mol:
        if m is None: raise ValueError
        m = rdk_std.Normalize(m)
        return m

# [F] Removing fragments
class Procedure_defragment(StandardisationProcedure):
    flag = 'F'
    name = 'Remove small organic fragments'

    @staticmethod
    def check_string(repr_type: types.RepresentationType, s: str) -> bool:
        if repr_type == types.SMILES:
            # Check if SMILES contains multiple fragments
            return '.' not in s
        elif repr_type == types.INCHI:
            # Convert to molecule and count fragments by RDKit
            mol = rdk.MolFromInchi(s)

            if mol is None:
                return True # TODO temporary hack fix
                #raise RuntimeError(f'Organic fragment checker: Mol is NONE, generated using MolFromInchi: {s}')

            mol_frags = rdk.GetMolFrags(mol)
            return len(mol_frags) <= 1
        else:
            raise ValueError

    @staticmethod
    def standardise(m: rdk.Mol) -> rdk.Mol:
        if m is None: raise ValueError
        m = rdk_std.FragmentParent(m)
        return m


# [I] Ignoring isotopes
# noinspection PyArgumentList
class Procedure_isotope(StandardisationProcedure):
    flag = 'I'
    name = 'Ignore isotopes'

    @staticmethod
    def check_string(repr_type: types.RepresentationType, s: str) -> bool:
        # Convert to RDKit and loop over atoms
        mol = None
        if repr_type == types.SMILES:
            mol = rdk.MolFromSmiles(s)
        elif repr_type == types.INCHI:
            mol = rdk.MolFromInchi(s)
        else:
            raise ValueError

        if mol is None:
            # Hack?
            raise ValueError

        atom: rdk.Atom
        for atom in mol.GetAtoms():
            if atom.GetIsotope():
                return False
        return True

    @staticmethod
    def standardise(m: rdk.Mol) -> rdk.Mol:
        if m is None: raise ValueError

        atom: rdk.Atom
        for atom in m.GetAtoms():
            if atom.GetIsotope():
                atom.SetIsotope(0)
        return m


# [C] Removing charges from atoms
    # TODO uncharging does not work as expected, sometimes keeps OH- and similar?
# noinspection PyArgumentList
class Procedure_uncharge(StandardisationProcedure):
    flag = 'C'
    name = 'Remove charges'

    uncharger = rdk_std.Uncharger()

    # TODO better uncharging approach?
        # https://rdkit.org/docs/Cookbook.html - section 'neutralizing molecules'
        # https://baoilleach.blogspot.com/2019/12/no-charge-simple-approach-to.html

    @staticmethod
    def check_string(repr_type: types.RepresentationType, s: str) -> bool:
        mol:        rdk.Mol
        mol_init:   rdk.Mol
        if repr_type == types.SMILES:
            mol_init = rdk.MolFromSmiles(s)
            mol = rdk.MolFromSmiles(s)
        elif repr_type == types.INCHI:
            mol_init = rdk.MolFromInchi(s)
            mol = rdk.MolFromInchi(s)
        else:
            raise ValueError

        if not mol:
            raise ValueError

        # Uncharge one
        if charge_target == 0:
            mol = Procedure_uncharge.uncharger.uncharge(mol)



        # Check if the two molecules are different
        return mol.HasSubstructMatch(mol_init) and mol_init.HasSubstructMatch(mol)  # TODO better way to check for identity?


    @staticmethod
    def standardise(m: rdk.Mol) -> rdk.Mol:
        if m is None: raise ValueError
        m = Procedure_uncharge.uncharger.uncharge(m)
        return m


# [T] Change molecule to the canonical tautomer
# noinspection PyArgumentList
class Procedure_tautomer_canon(StandardisationProcedure):
    flag = 'T'
    name = 'Canonicalise tautomer'

    tautomer_enumerator = rdk_std.TautomerEnumerator()

    @staticmethod
    def check_string(repr_type: types.RepresentationType, s: str) -> bool:
        mol:            rdk.Mol
        mol_init:       rdk.Mol
        if repr_type == types.SMILES:
            mol_init = rdk.MolFromSmiles(s)
            mol = rdk.MolFromSmiles(s)
        elif repr_type == types.INCHI:
            mol_init = rdk.MolFromInchi(s)
            mol = rdk.MolFromInchi(s)
        else:
            raise ValueError

        if not mol:
            raise ValueError

        # Uncharge
        Procedure_tautomer_canon.tautomer_enumerator.Canonicalize(mol)

        # Check if the two molecules are different
        return mol.HasSubstructMatch(mol_init) and mol_init.HasSubstructMatch(mol)  # TODO better way to check for identity?

    @staticmethod
    def standardise(m: rdk.Mol) -> rdk.Mol:
        if m is None: raise ValueError
        m = Procedure_tautomer_canon.tautomer_enumerator.Canonicalize(m)
        return m


# [S] Remove all stereochemistry
class Procedure_stereo(StandardisationProcedure):
    flag = 'S'
    name = 'Remove stereochemical information'

    @staticmethod
    def check_string(repr_type: types.RepresentationType, s: str) -> bool:
        if repr_type == types.SMILES:
            # Check for all stereochemistry symbols in the SMILES definition
            return not ('/' in s or '\\' in s or '@' in s)
        elif repr_type == types.INCHI:
            # Check if stereo layers are in the string (/t - tetrahedral, /b double bond,
            return not ('/t' in s or '/b' in s)
        else:
            raise ValueError

    @staticmethod
    def standardise(m: rdk.Mol) -> rdk.Mol:
        rdk.RemoveStereochemistry(m)
        return m


