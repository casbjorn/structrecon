import networkx as nx
import os
import shutil
import graphviz
import logging
import itertools as it


import rdkit.Chem.AllChem as rdk
import rdkit.Chem.Draw as rdk_draw

import structrecon.model.model as model
import structrecon.model.identifiergraph as identifiergraph
import structrecon.model.parameters as parameters
import structrecon.model.types as types
import structrecon.model.ontology as ontology
import structrecon.model.app_query as app_query
import structrecon.model.app_response as app_response

###############################################################################################################
# Definition and settings for the Graph Printer
###############################################################################################################

# Settings
graphviz_newrank = 'true'     # TODO better if false?

class GraphPrinter:
    """ Object to handle printing the model nicely. """

    # Query
    query:                  app_query.AppQuery
    M:                      model.Model
    response:               app_response.AppResponse
    imgdir:                 str
    log:                    logging.Logger

    # Settings
    draw_molecules:         bool
    draw_conf_on_all:       bool

    # Define the colours for nodes used in the graph printing
    c_input:                str
    c_initial:              str
    c_identifier:           str
    c_structure:            str
    c_candidate:            str
    c_standard_conflict:    str

    # Other
    img_format = 'png'          # For some reason, 'svg' option does not work with graphviz
    has_images: bool
    show_generation: bool     # Debug variable to show the step in which each vertex was discovered.

    def __init__(self, query: app_query.AppQuery, response: app_response.AppResponse):
        self.query = query
        self.response = response
        self.M = response.M
        self.log = self.query.log

        # Initialise settings from the query
        self.draw_molecules = query.parameters[parameters.param_draw_mol_images]
        self.draw_conf_on_all = query.parameters[parameters.param_draw_confidence_on_all]
        self.show_generation = self.query.parameters[parameters.param_show_generation]
        self.show_noncentral = query.parameters[parameters.param_show_noncentral]
        self.show_support = query.parameters[parameters.param_show_support]

        # Colours
        self.c_input                = '#71D97E'
        self.c_initial              = '#36f9cc'
        self.c_identifier           = '#a1eaff'

        self.c_structure            = '#8CB8FF'
        self.c_candidate            = '#C18DFF'
        self.c_selected             = '#FFD559'
        self.c_excluded             = '#DADADA'

        self.c_ontology             = '#FFD559'

        self.has_images = False
        self.imgdir = '.'


    # Utility sub-procedures for generating vertex properties
    # ==================================================================================================================

    def set_attr(self, graph: graphviz.Graph):
        """ Applies attributes which apply no matter which subgraph is drawn. """
        graph.attr(
            'node',
            shape='rectangle',
            penwidth='0',
            fontname='Courier',
            style='filled, rounded',
            color='white',
            newrank=graphviz_newrank
        )
        graph.attr(
            'edge',
            fontname='Courier',
            color='#818181'
        )
        graph.attr(concentrate = 'false')
        graph.attr(imagepath = self.imgdir)


    @staticmethod
    def length_limit_string(s: str, length: int) -> str:
        s_new = ''
        for i in range(len(s)):
            if i % length == 0 and i != 0:
                s_new += '<br/>'
            s_new += s[i]
        return s_new


    # Graphviz node content
    def vertex_specification_string(
            self,
            v: model.Vertex,
            compound_response: app_response.CompoundResponse,
            imgdir: str = 'img',
            drawsmiles: bool = False,
            drawname: bool = True,
            drawprops: bool = True,
            conf: float = 0
        ) -> str:
        """ Returns the string to put in the drawing. """
        line_length = 30
        in_str = stringsanitise(v.cont)

        if drawsmiles:
            drawtypes = {types.SMILES, types.INCHI}
        else:
            drawtypes = {types.INCHI}

        # Determine the title
        if isinstance(v.type, types.IdentifierType):
            if v in compound_response.idgraph.explicit_props.keys() and compound_response.idgraph.explicit_props[v]:
                title_str = f"{v.type.name} ({','.join(compound_response.idgraph.explicit_props[v])})"
            else:
                title_str = f'{v.type.name}'

        elif isinstance(v.type, types.StructureType):

            if v.std_flags == '':
                title_str = f'{v.type.name}:'
            else:
                title_str = f'{v.type.name} ({v.std_flags}):'

            # Show explicit inherited properties if enabled
            if self.query.parameters[parameters.param_show_inherited_explicit_properties]:
                try:
                    explicit_props = compound_response.idgraph.explicit_props_inherited[v]
                    title_str += f" {explicit_props}"
                except KeyError:
                    pass
        else:
            title_str = f'{v.type.name}'


        # Determine the string of the ID
        id_str = self.length_limit_string(in_str, line_length)

        # Next, determine whether to display an image
        if self.draw_molecules and v.type in drawtypes:
            drawimg = True
            image = f'mol{v.index}.{self.img_format}'
        else:
            drawimg = False
            image = ''

        # Put all of this into an HTML table
        out =   f'<<TABLE BORDER="0" CELLBORDER="0" CELLSPACING="2" CELLPADDING="0">' \
                f'<TR><TD>{title_str}</TD></TR>' \
                f'<TR><TD><B>{id_str}</B></TD></TR>' \

        # Name
        if drawname and v.name and v.type != types.CNAME:
            out += f'<TR><TD><i>{self.length_limit_string(stringsanitise(v.name), line_length)}</i></TD></TR>'

        # Properties
        if drawprops and v.properties:
            for prop, value in v.properties.items():
                out += f'<TR><TD>{prop}: {value}</TD></TR>'


        if drawimg:
            out += f'<TR><TD><IMG SRC=\"{imgdir}/{image}\"/></TD></TR>'

        # Confidence
        if conf != 0 and (self.draw_conf_on_all):
            if self.draw_conf_on_all:
                # Draw the absolute confidence
                out += f'<TR><TD>Conf: {conf:.3f}</TD></TR>'
        elif v in compound_response.candidates and compound_response.n_candidate_structs > 1:
            # Draw relative confidence
            out += f'<TR><TD>Conf: {compound_response.candidate_conf_rel[v]:.3f}</TD></TR>'

        # Generation
        if self.show_generation and v.generation > 0:
            out += f'<TR><TD>Generation {v.generation}</TD></TR>'

        # If preliminary candidate
        if self.query.parameters[parameters.param_show_preliminary_candidates] and v in compound_response.idgraph.preliminary_candidates:
            out += f'<TR><TD>PRELIMINARY CANDIDATE</TD></TR>'


        # Excluded
        if v in compound_response.excluded_vertices:
            reasons: list[str] = compound_response.excluded_reasons[v]
            if len(reasons) <= 1:
                out += f'<TR><TD>Excluded: {reasons[0]}</TD></TR>'
            else:
                out += f'<TR><TD>Excluded: {", ".join(reasons)}</TD></TR>'

        # Support
        if self.show_support and v.type is types.INCHI:
            out += f'<TR><TD>Support: ({len(compound_response.idgraph.support[v])}):<BR/>'
            for u in compound_response.idgraph.support[v]:
                out += f'{u.cont}<BR/>'
            out += '</TD></TR>'

        out +=  f'</TABLE>>'
        return out


    def vertex_color(self, v: model.Vertex, c_resp: app_response.CompoundResponse) -> (str, str):
        """ Determine color and outline color """

        # Background color
        col: str
        if v not in c_resp.idgraph.v_central:
            col = self.c_excluded

        elif isinstance(v.type, types.InputType):
            col = self.c_input
        elif isinstance(v.type, types.StructureType):
            if v in c_resp.candidates:
                col = self.c_candidate
            else:
                if v not in c_resp.excluded_vertices:
                    col = self.c_structure
                else:
                    col = self.c_excluded
        elif isinstance(v.type, types.IdentifierType):
            if v.initial:
                col = self.c_initial
            else:
                col = self.c_identifier
        else:
            # fallback
            col = 'white'


        # Border
        border = 'white'
        if c_resp.selected_structure == v:
            # border = 'black'
            col = self.c_selected

        # Return the attribute dict
        return col, border


    # Determine if a vertex should be a source or sink
    def vertex_rank(self, v: model.Vertex) -> str:
        if isinstance(v.type, types.InputType):
            rank = 'source'
        else:
            rank = ''

        return rank


    # Code called with cluster/graph/subgraph as context
    def makenode(
            self,
            v: model.Vertex,
            compound_response: app_response.CompoundResponse,
            cluster,
            imgdir: str = 'img',
            conf: float = 0
    ):
        col, border = self.vertex_color(v, c_resp = compound_response)

        if border == 'white':
            edge_width = '0'
        else:
            edge_width = '4'

        if isinstance(v.type, types.IdentifierType) and v.type.identifiers_org_prefix:
            url = f'{v.type.identifiers_org_prefix}{v.cont}'
        else:
            url = ''

        # Add node
        cluster.node(
            f'{v.index}',
            self.vertex_specification_string(v, compound_response, conf = conf, imgdir=imgdir),
            fillcolor = col,
            pencolor = border,
            penwidth = edge_width,
            color = border,
            URL = url,
            target = "_blank"
        )


    # Edge stuff
    # ------------------------------------------------------------------------------------------------------------------
    def print_edge(
            self,
            graph: identifiergraph.IdentifierGraph,
            g: graphviz.Graph,
            u: model.Vertex,
            v: model.Vertex
    ):
        """ Draw all applicable edges between a pair of vertices. """

        # First, find edges in the graph
        edges_fwd: set[model.Edge] = set()
        edges_bck: set[model.Edge] = set()
        if graph.graph.adj[u]:
            try:
                for mdg_edge in graph.graph.adj[u][v].values():
                    edges_fwd.add(mdg_edge['label'])
            except KeyError:
                pass
        if graph.graph.adj[v]:
            try:
                for mdg_edge in graph.graph.adj[v][u].values():
                    edges_bck.add(mdg_edge['label'])
            except KeyError:
                pass

        edges = edges_fwd | edges_bck

        # First, catch single-edge cases such as input edges and standardisation
        if len(edges) == 1:
            edge: model.Edge = list(edges)[0]
            if graph.graph.has_edge(u, v):
                tail = u
                head = v
            else:
                tail = v
                head = u

            # Input edge
            if isinstance(edge, model.EdgeInitial):
                g.edge(
                    str(tail.index),
                    str(head.index),
                    dir='forward'
                )
                return

            # Standardisation edge
            elif isinstance(edge, model.EdgeStd):
                g.edge(
                    str(tail.index),
                    str(head.index),
                    dir='forward',
                    xlabel=edge.stdfunc
                )
            # Smiles -> inchi transformation edges
            elif isinstance(edge, model.EdgeTransformation):
                g.edge(
                    str(tail.index),
                    str(head.index),
                    dir = 'forward',
                    xlabel = ''
                )

        # Next, process reference edges
        ref_labels: set[str] = set()
        for edge in filter(lambda edge: isinstance(edge, model.EdgeRef), edges):
            edge: model.EdgeRef
            ref_labels.add(edge.source)
        if ref_labels:
            g.edge(
                str(u.index),
                str(v.index),
                dir = 'backward' if not edges_fwd else ('none' if edges_bck else 'forward'),
                xlabel = '/'.join(ref_labels)
            )

        # Process ontology edges
        list_fwd = list(zip(filter(lambda edge: isinstance(edge, model.EdgeOnt), edges_fwd), it.repeat('forward')))
        list_bck = list(zip(filter(lambda edge: isinstance(edge, model.EdgeOnt), edges_bck), it.repeat('back')))
        relation_classes_drawn: set[ontology.Relation] = set()

        for edge, direction in list_fwd + list_bck:
            edge: model.EdgeOnt
            rel: ontology.Relation = edge.rel
            if rel not in relation_classes_drawn:

                # If the relation has inverse and inverse is primary, draw that instead
                if edge.rel.inverse and (not edge.rel.is_primary) and (edge.rel.inverse.is_primary):
                    rel = rel.inverse
                    direction = 'back' if direction == 'forward' else 'forward'

                # If it has inverse, but none is prioritized, draw edges both ways
                    # TODO

                # If symmetric, no direction on arrow
                if rel.symmetric:
                    direction = 'none'

                # Draw the relation
                if rel.draw == 'arrow':
                    # Draw as normal relation arc
                    g.edge(
                        str(u.index),
                        str(v.index),
                        dir = direction,
                        xlabel = rel.name,
                    )

                elif edge.rel.draw == 'full':
                    # Draw as a box in the middle of two arcs
                    new_name = f'{u.index}_{rel.name}_{v.index}'

                    # First, make new node
                    g.node(
                        name = new_name,
                        label = rel.name,
                        fillcolor = self.c_ontology,
                        shape = 'diamond'
                    )

                    # Make new edges
                    g.edge(
                        str(u.index), new_name,
                        dir = direction
                    )
                    g.edge(
                        new_name, str(v.index),
                        dir = direction
                    )

                # Mark as drawn
                relation_classes_drawn.add(rel)
                if rel.inverse:
                    relation_classes_drawn.add(rel.inverse)



    # Molecule image drawing
    # ------------------------------------------------------------------------------------------------------------------
    def vertex_image(self, v: model.Vertex, imgdir='out/img') -> str:
        # Save image to out/img/...
        fname = f'{imgdir}/mol{v.index}.{self.img_format}'

        if v.type == types.INCHI:
            mol = rdk.MolFromInchi(v.cont)
        elif v.type == types.SMILES:
            return ''
        else:
            return ''

        if mol is None:
            # Not available, remove
            return ''

        rdk_draw.MolToFile(
            mol,
            fname,
            size = (340, 200),
            kekulize = False,
            wedgeBonds = True,
            imageType = self.img_format
        )

        return fname

    def prepare_images(self, M: model.Model, imgdirectory: str = 'out/img'):
        # First check if directory exists, and clean it out
        if not self.draw_molecules:
            return

        if self.has_images:
            self.log.info('Already has images, break')
            return

        if os.path.isdir(imgdirectory):
            self.log.info('Image directory exists, removing files.')
            # Remove all files within
            for fname in os.listdir(imgdirectory):
                try:
                    if os.path.isfile(fname) or os.path.islink(fname):
                        os.unlink(fname)
                    elif os.path.isdir(fname):
                        shutil.rmtree(fname)
                except Exception as e:
                    self.log.warning('Failed to delete %s. Reason: %s' % (fname, e))
        else:
            self.log.info(f'Creating image directory directory {imgdirectory}')
            os.makedirs(imgdirectory)

        # Draw images for all structure vertices in each idgraph
        hasimg: set[model.Vertex] = set()

        self.log.info(f'Drawing molecule images')
        for idgraph in M.id_graph_input.values():
            for v in idgraph.graph.nodes():
                if v not in hasimg and v.type in {types.INCHI}:
                    self.vertex_image(v, imgdirectory)
                    hasimg.add(v)

        self.log.info(f'Done drawing.')
        self.has_images = True


    # Main graph Drawer
    # ==================================================================================================================
    def print_compounds(
            self,
            M: model.Model,
            outdir: str = 'out',
            subdir: str = 'compounds',
            filetype: str = 'svg',
            nostructs: bool = False,
            simplenames: bool = False   # Whether to only name by input
    ):
        self.log.info('Drawing compound images')

        # Create / reset directory
        if os.path.isdir(f'{outdir}/{subdir}'):
            self.log.info('Directory for compound images exists, removing files.')
            # Remove all files within
            for fname in os.listdir(f'{outdir}/{subdir}'):
                try:
                    if os.path.isfile(fname) or os.path.islink(fname):
                        os.unlink(fname)
                    elif os.path.isdir(fname):
                        shutil.rmtree(fname)
                except Exception as e:
                    self.log.warning('Failed to delete %s. Reason: %s' % (fname, e))
        else:
            self.log.info(f'Creating compound image directory {outdir}/{subdir}')
            os.makedirs(f'{outdir}/{subdir}/img')

        # If necessary, make an image of all the molecules
        if self.draw_molecules:
            self.prepare_images(M, f'{outdir}/{subdir}/img')

        for compound in self.response.compounds.values():
            # Determine name
            if simplenames:
                name = f'{compound.input}'
            else:
                if compound.macro:
                    name = f'[MACRO] {compound.input}'
                else:
                    if compound.n_candidate_structs <= 1:
                        # Count fully standard vertices
                        name = f'[{compound.n_candidate_structs}] {compound.input}'
                    else:
                        # More than 1, also draw conf
                        name = f'[{compound.n_candidate_structs}] {compound.input} (conf {compound.conf_margin:.2f})'

            self.print_graph(
                M,
                compound,
                M.id_graph_input.get(compound.input_compound, None),
                outdir = outdir,
                subdir = subdir,
                name = name,
                conf_values = compound.vertex_conf,
                filetype = filetype,
                nostructs = nostructs
            )


    # Main function for printing a graph component
    # ------------------------------------------------------------------------------------------------------------------
    def print_graph(
            self,
            M: model.Model,
            compound_response: app_response.CompoundResponse,
            idgraph: identifiergraph.IdentifierGraph,
            outdir: str = 'out',
            compound_img_subdir: str = 'img',
            subdir: str = 'temp',
            name = '',
            conf_values: dict[model.Vertex, float] = None,
            filetype: str = 'pdf',
            nostructs: bool = False
            ):
        # Print a particular subset, without any further divisions.
        # Can be either a connected component or a reach-set.

        # Abort if idgraph does not exist
        if idgraph is None:
            self.log.error(f'No idgraph for {compound_response.input_compound.source_id}!')
            return

        # Initialise
        graph = idgraph.graph
        g = graphviz.Graph()
        self.set_attr(g)

        show_noncentral = self.query.parameters[parameters.param_show_noncentral]

        # Make list of vertices to show
        show_vertices: set[model.Vertex] = set()
        if show_noncentral:
            show_vertices = graph.nodes()
        else:
            show_vertices = set(filter(lambda v_: v_ in idgraph.v_central, graph.nodes()))

        # Determine name of the component
        input_vs: set[model.Vertex] = set()
        for v in graph:
            if v.type == types.INPUT:
                input_vs.add(v)

        if name == '':
            if len(input_vs) >= 1:
                file_name = f'{input_vs.pop().cont}'
            else:
                file_name = 'NOINPUT'       # TODO parametrise
        else:
            file_name = name

        # Determine conf vector
        if not conf_values:
            conf_dict = {v: 0 for v in graph}
        else:
            conf_dict = {v: conf_values[v] for v in idgraph.v_central}

        # [1] Draw input vertices as source
        with g.subgraph(
                name=f'c_source',
                graph_attr={}
        ) as source_cluster:
            source_cluster.attr(rank='source')

            for v in show_vertices:
                if v.type == M.input_type:
                    self.makenode(v, compound_response, source_cluster, conf = conf_dict[v])

        # [3] Finally, draw all other nodes
            # If nostructs enabled, do not draw nodes corresponding to structures
        for v in show_vertices:
            # Draw nodes not in sink or source set
            if not v.type == M.input_type \
                    and not (nostructs and v.type in {types.SMILES, types.INCHI}) \
                    and (show_noncentral or v in idgraph.v_central):
                # Create node
                if v in idgraph.v_central:
                    self.makenode(v, compound_response, g, conf = conf_dict[v], imgdir = compound_img_subdir)
                else:
                    self.makenode(v, compound_response, g, conf = 0, imgdir = compound_img_subdir)

            # Add edges for all vertices in all clusters
            if len(nx.edges(graph, v)) < 1:
                # Edge case
                continue


        # [4] Draw all edges
        # --------------------------------------------------------------------------------------------------------------
        # For all pairs in component
        for tail, head in it.combinations(show_vertices, 2):
            if graph.has_edge(head, tail) or graph.has_edge(tail, head):
                self.print_edge(idgraph, g, tail, head)

        # Render
        try:
            g.render(
                filename=f'{file_name}.gv',
                outfile=f'{outdir}/{subdir}/{file_name}.{filetype}',
                directory=f'{outdir}/{subdir}',
                view=False,
                format=filetype
            )
        except graphviz.backend.execute.CalledProcessError as e:
            self.log.error(f'\nGRAPHVIZ RENDOR ERROR: {e}')


# Utility functions
# ======================================================================================================================

def stringsanitise(s: str) -> str:
    """ Remove non-ascii characters from the string and return"""
    return s\
        .replace('>', '')\
        .replace('<', '')\
        .replace('"', '')\
        .replace('&', '')   # Problem occurs with <, > in names


