""" Encapsulates the response expected from a query.
"""
from __future__ import annotations

import logging
from enum import Enum
from collections import defaultdict
from typing import Any
import os.path
import uuid
import networkx as nx

import structrecon.model.model as model
import structrecon.model.identifiergraph as identifiergraph
import structrecon.model.types as types
import structrecon.model.ontology as ontology
import structrecon.model.standardise as std
import structrecon.model.app_query as app_query
import structrecon.model.parameters as parameters
import structrecon.model.chem as chem

class Status(Enum):
    NO_STRUCT = 0           # No structures found
    CONSISTENT = 1          # Only one structure found up to standardisation
    CONSISTENT_SELECT = 2   # Only one structure found due to conf. threshold
    INCOMPATIBLE = 3        # Structures found, but not compatible with the input
    INCONSISTENT = 4        # Multiple possible structures
    MACRO       = 5
    ERR         = 6


# Macro detection constants
macro_substrings_cname = {
        'Biomass', 'biomass',
        '-[acyl-carrierprotein]',
        'tRNA',
        'thioredoxin', 'Thioredoxin',       # Protein
        'SufSE', 'SufBCD',                  # Acceptor complexes
        'protein', 'Protein',
        'IscU', 'ISCU', 'IscS', 'ISCS',
        'Glutaredoxin', 'glutaredoxin'      # Protein
}

macro_substrings_bigg = {
    'ACP',
    'trna'
}

# Compound response object
# ======================================================================================================================

# noinspection PyCallingNonCallable
class CompoundResponse:
    """ Represents the response by StructRecon to each input
    """

    # Basic information
    input:                  str                 # The input string
    v_input:                model.Vertex        # The source vertex
    inputs:                 list[str]           # The list of input strings for this compound
    log:                    logging.Logger      # Log from query
    input_compound:         app_query.CompoundInput     # Input information

    # Connectedness and graph information
    idgraph:                identifiergraph.IdentifierGraph
    connected_compounds:    list[CompoundResponse]      # Other compounds connected to this through the ID graph

    v_central:              set[model.Vertex]           # A vertex is in this cet if considered 'central'

    n_identifiers:          int                         # Size of the component
    n_structures:           int
    n_candidate_structs:    int

    # Confidence
    most_confident_vertex:  model.Vertex
    vertex_conf:            dict[model.Vertex, float]
    candidate_conf_rel:     dict[model.Vertex, float]  # Relative confidence of all maximal std. structs.
                                    # Linearly scaled s.t. the vertex with the highest confidence has conf 1.0
    conf_margin:            float   # The difference between scaled confidence of the most confident (1.0)
                                    # to the second-most confident.'

    vertex_link_distance:   dict[model.Vertex, int]

    # Structure selection
    candidates_preliminary: set[model.Vertex]
    candidates:             set[model.Vertex]
    candidate_structures_sorted: list[model.Vertex]
    selected_structure:     model.Vertex | None
    status:                 Status
    excluded_vertices:      set[model.Vertex]
    excluded_reasons:       dict[model.Vertex, list[str]]
    selected_structure_std_state: str           # Standardisation of selected compound (e.g. FICT-)

    # Derived information
    common_names:           list[str]           # Common names found
    all_structs:            list[model.Vertex]

    macro:                  bool                # Whether the compound has been derived to be a macromolecule
    macro_reasons:          list[str]                 # If macro, the reason this compound has been assigned as such

    # Info for the web-interface table
    result_display:             str
    display_properties:         dict[str, Any]      # Properties to display in table on right pane
    display_properties_lists:    dict[str, list]      # Same as display_properties, but shown in full-width!

    def __init__(self, M: model.Model, input_compound: app_query.CompoundInput):
        """ Analyse the properties of a compound. Sets all values which are possible at this time, others will need
            to be set later.
        """
        self.input_compound = input_compound
        query = M.query
        self.log = query.log

        # 1. Generate compound subgraph set
        # --------------------------------------------------------------------------------------------------------------
        # TODO use the ones which exist in the model
        self.v_input = M.input_vertex_map[input_compound]
        self.input = self.v_input.cont
        self.vertex_conf = dict()
        self.status = Status.ERR
        self.display_properties = dict()
        self.display_properties_lists = dict()

        try:
            self.idgraph = M.id_graph_input[input_compound]
        except KeyError:
            print(f'No graph component for {input_compound.source_id}.')
            return

        self.candidates = self.idgraph.preliminary_candidates.copy()

        # Create view which exclude extra 'noncentral' vertices
        graph_view: nx.MultiDiGraph = nx.MultiDiGraph(self.idgraph.graph.copy(as_view=True))
        v_noncentral: list[model.Vertex] = []
        for v in graph_view.nodes():
            if v not in self.idgraph.v_central:
                v_noncentral.append(v)
        for v in v_noncentral:
            graph_view.remove_node(v)


        # 2. Check standardisation and prune over-standardised structures
        # --------------------------------------------------------------------------------------------------------------
        # A vertex is over-standardised if it has exactly one ancestor which can be considered a candidate.
        # In this case, the vertex is excluded, and the candidate tag passes to the ancestor.
            # Example: M_nad_c
        self.all_structs: list[model.Vertex] = list(filter(lambda v: v.type == types.INCHI, graph_view.nodes()))
        cand_list: list[model.Vertex] = list(self.candidates)
        to_remove: set[model.Vertex] = set()

        for v in cand_list:
            valid_ancestors: set[model.Vertex] = set()
            ancestor: model.Vertex
            for ancestor in graph_view.reverse().neighbors(v):
                if (ancestor.type == types.INCHI) and (
                        self.idgraph.support[ancestor] == self.idgraph.support[v]
                ):
                    valid_ancestors.add(ancestor)

            # If only one direct ancestor with same support
            if len(valid_ancestors) == 1:
                ancestor = valid_ancestors.pop()
                to_remove.add(v)
                cand_list.append(ancestor)

        for v in to_remove:
            cand_list.remove(v)

        # Re-make compound list:
        self.candidates = set(cand_list)
        self.all_structs = list(filter(lambda v: v.type == types.INCHI, graph_view.nodes()))


        # 3. Traverse the set to populate different counters and lists, also set standard property of structures
        # --------------------------------------------------------------------------------------------------------------
        self.n_structures = 0
        self.n_identifiers = 0
        self.n_candidate_structs = 0
        self.common_names = list()
        self.candidate_conf_rel = dict()
        self.candidate_structures_sorted = []

        v: model.Vertex
        for v in graph_view.nodes():
            if v.type == types.CNAME:
                # Add to common name list
                self.common_names.append(v.cont)

            if v.type in {types.SMILES, types.INCHI}:
                # v is type Structure
                if v.type == types.INCHI:
                    self.n_structures += 1

            elif v.type != types.INPUT:
                # v is type Identifier
                self.n_identifiers += 1

        # 4. Calculate edge weights for ranking
        # --------------------------------------------------------------------------------------------------------------

        # Weigh vertices based on types, references based on source, and ontologies based on the type
        type_weight: dict[types.RepresentationType, float] = defaultdict(lambda: 1)
        ref_source_weight: dict[str, float] = defaultdict(lambda: 1)
        ontology_weight: dict[ontology.Relation, float] = defaultdict(lambda: 0)    # By default, do not consider

        # Set weights from parameters
        type_weight[types.CNAME] = M.query.parameters[parameters.param_ranking_cname]
        ontology_weight[ontology.HAS_NEW_ID] = M.query.parameters[parameters.param_ranking_depr_weight]
        ontology_weight[ontology.HAS_OLD_ID] = M.query.parameters[parameters.param_ranking_depr_weight] / 2

        # Set weight of edges
        for u, v, e in graph_view.edges:
            edge_obj: model.Edge = graph_view[u][v][e]['label']
            v_weight = type_weight[v.type]
            if isinstance(edge_obj, model.EdgeOnt):
                graph_view[u][v][e]['weight'] = ontology_weight[edge_obj.rel] * v_weight
            elif isinstance(edge_obj, model.EdgeRef):
                graph_view[u][v][e]['weight'] = ref_source_weight[edge_obj.source] * v_weight
            else:
                graph_view[u][v][e]['weight'] = 1 * v_weight

        # 5. Calculate confidence of each vertex
        # --------------------------------------------------------------------------------------------------------------
        # Make a new subgraph and modify to make compatible with page-ranker
        pagerank_component: nx.MultiDiGraph = nx.MultiDiGraph(graph_view.copy())

        # 'Dangling' nodes prevent the algorithm from converging, and must therefore be handled.
        # An edge will be added, which goes to the input again
        for v in graph_view.nodes():
            if len(list(nx.neighbors(pagerank_component, v))) == 0:
                pagerank_component.add_edge(v, self.v_input)

        # Compute the confidence according to the pagerank alg
        self.vertex_conf = nx.pagerank(
            G = pagerank_component,
            alpha = 0.85,  # Damping factor
            nstart = {v: (1 if v == self.v_input else 0) for v in graph_view},  # Initial state vector
            weight = 'weight'
        )

        # Compute single source shortest path and modify confidence based on the damping factor
        decay = query.parameters[parameters.param_ranking_decay]
        self.vertex_link_distance: dict[model.Vertex, int] = nx.shortest_path_length(
            graph_view,
            self.v_input
        )
        for v in graph_view.nodes():
            self.vertex_conf[v] *= (1-decay) ** self.vertex_link_distance.get(v, 100)

        # X. Normalise confidence scores
        # --------------------------------------------------------------------------------------------------------------
        maxconf: float = max(self.vertex_conf.values())
        for v, conf in self.vertex_conf.items():
            self.vertex_conf[v] = conf/maxconf


        # 6. Compute the list of vertices excluded based on SBML constraints
        # --------------------------------------------------------------------------------------------------------------
        input_charge: int | None
        input_formula: str | None

        try:
            input_charge = int(input_compound.properties['charge'])
        except KeyError:
            input_charge = None

        try:
            input_formula = input_compound.properties['formula']
        except KeyError:
            input_formula = None

        # Filter candidate lists by charge
        self.excluded_vertices = set()
        self.excluded_reasons = {v: [] for v in graph_view.nodes()}

        if query.parameters[parameters.param_sbml_enforce_charge] and input_charge is not None:
            for v in filter(lambda v: v.type == types.INCHI, graph_view.nodes()):
                try:
                    if int(v.properties['charge']) != int(input_charge):
                        self.excluded_vertices.add(v)
                        self.excluded_reasons[v].append('Charge inconsistent')
                except KeyError:
                    pass

        # Filter candidate lists by formula
        if query.parameters[parameters.param_sbml_enforce_formula] in {
            'Yes, ignore hydrogen',
            'Yes, enforce hydrogen'
        } and input_formula is not None:
            i_form = chem.chem_formula_from_string(input_formula)
            if query.parameters[parameters.param_sbml_enforce_formula] == 'Yes, ignore hydrogen':
                ignore_hydrogen = True
                i_form = chem.chem_formula_remove_h(i_form)
            else:
                ignore_hydrogen = False

            for v in filter(lambda v: v.type == types.INCHI, graph_view.nodes()):
                try:
                    v_form = chem.chem_formula_from_string(v.properties['formula'])
                    if ignore_hydrogen:
                        v_form = chem.chem_formula_remove_h(i_form)

                    if i_form != v_form:
                        self.excluded_vertices.add(v)
                        self.excluded_reasons[v].append('Formula inconsistent')
                except KeyError:
                    # In case a formula is not available, exclude
                    pass
                    #self.excluded_vertices.add(v)


        # # 7. Re-evaluate candidate structures after filtering
        # # --------------------------------------------------------------------------------------------------------------
        # self.candidates = set()
        #
        # # Make a view to filter out excluded structures - filter: vertices should be in graph component and not excluded
        # graph_filtered = nx.subgraph_view(
        #     self.idgraph.graph,
        #     lambda v: v not in self.excluded_vertices and v in self.idgraph.graph.nodes()
        # )
        #
        # # List candidate structures from the filtered view
        # filtered_structs: set[model.Vertex] = nx.subgraph_view(
        #     graph_filtered,
        #     lambda v: v.type ==  types.INCHI
        # ).nodes()
        #
        # for v in filtered_structs:
        #
        #     # Iteratively exclude vertices if only one ancestor and ancestor is sufficiently standardised
        #     v_consider: model.Vertex = v
        #     while (graph_filtered.in_degree([v_consider]) == 1):
        #         ancestor: model.Vertex
        #         if self.idgraph.graph.predecessors(v):
        #             ancestor = list(self.idgraph.graph.predecessors(v))[0]
        #         else:
        #             break
        #         if (
        #             ancestor.type == types.INCHI and
        #             std.has_flags(ancestor, query.parameters[parameters.param_stdfunc_always])
        #         ):
        #             # Exclude current vertex and consider ancestor instead
        #             self.excluded_vertices.append(v_consider)
        #             v_consider = ancestor
        #         else:
        #             break
        #
        #     # Finally set the considered vertex to a candidate
        #     if graph_filtered.out_degree(v_consider) == 0 and v_consider in self.idgraph.v_central:
        #         self.candidates.add(v_consider)


        # 7. Set aux information related to confidence calculation
        # --------------------------------------------------------------------------------------------------------------
        if len(self.candidates) > 0:
            self.n_candidate_structs = len(self.candidates)

            # Get most confident vertex and find relative confidence of other maximal structures
            self.most_confident_vertex = max(self.candidates, key = self.vertex_conf.get)
            for v in sorted(self.candidates, key = self.vertex_conf.get, reverse = True):
                self.candidate_conf_rel[v] = self.vertex_conf[v] / self.vertex_conf[self.most_confident_vertex]

            # Calculate the confidence margin for this compound
            if len(self.candidates) <= 1:
                self.conf_margin = 0
            else:
                self.conf_margin = sorted(list(self.candidate_conf_rel.values()))[-2]

            # Sort the list of structures by confidence
            self.candidate_structures_sorted = list(self.candidates)
            self.candidate_structures_sorted.sort(key = self.candidate_conf_rel.get, reverse = True)


        # 8. Compute resulting structure(s) for this compound and set the status
        # --------------------------------------------------------------------------------------------------------------
        self.n_candidate_structs = len(self.candidates)
        if self.n_candidate_structs < 1:
            self.status = Status.NO_STRUCT
            self.selected_structure = None
        elif self.n_candidate_structs == 1:
            self.status = Status.CONSISTENT
            self.selected_structure = self.most_confident_vertex
        else:
            if self.conf_margin <= query.parameters[parameters.param_conf_threshold]:
                self.status = Status.CONSISTENT_SELECT
                self.selected_structure = self.most_confident_vertex
            else:
                self.status = Status.INCONSISTENT
                self.selected_structure = None

        self.log.info(f'Compound {self.input_compound.source_id}: {self.status}')

        # Get standardisation state
        if self.selected_structure:
            self.selected_structure_std_state = self.selected_structure.std_flags
        else:
            self.selected_structure_std_state = ''


        # 9. Determine if the compound is macro
        # --------------------------------------------------------------------------------------------------------------
        self.is_macro = False
        self.macro_reasons = []

        if self.status == Status.NO_STRUCT:
            # Only classify as macro if no structures are found
            compound_names: list[str] = list()
            for v in graph_view.nodes():
                if v.type == types.CNAME:
                    compound_names.append(v.cont)

                for sub in macro_substrings_cname:
                    if sub in v.cont:
                        self.macro_reasons.append(
                            f'Connected name {v.cont} contains substring {sub}'
                        )

                # Check name property of entries
                try:
                    n = v.properties.get('name')
                    for sub in macro_substrings_cname:
                        if sub in n:
                            self.macro_reasons.append(
                                f'Connected vertex {v.cont} has name {n} containing substring {sub}'
                            )

                except KeyError:
                    continue
                except TypeError:
                    continue


            compound_biggs: list[str] = []
            for v in graph_view.nodes():
                if v.type == types.BIGG:
                    compound_biggs.append(v.cont)

                    for sub in macro_substrings_bigg:
                        if sub in v.cont:
                            self.macro_reasons.append(
                                f'Connected BiGG ID {v.cont} contains substring {sub}'
                            )

        if self.macro_reasons:
            self.is_macro = True
            self.status = Status.MACRO

        # 10. Set string to display in web interface for each compound
        # --------------------------------------------------------------------------------------------------------------
        if self.n_candidate_structs < 1:
            self.result_display = 'No structures found'
        elif self.n_candidate_structs == 1:
            self.result_display = self.most_confident_vertex.cont
        else:
            self.result_display = ', '.join(
                [f'{v.cont} [{self.candidate_conf_rel[v]:.2f}]' for v in self.candidates]
            )

        # 11. Select properties to display in the web interface
        # --------------------------------------------------------------------------------------------------------------
        self.display_properties = dict()
        self.display_properties_lists = dict()

        # Structure InChI should be shown in large font at the top, only add extra information

        # Information based on the status
        match self.status:
            case Status.NO_STRUCT:
                # No struct, no information to display
                # TODO what if excluded structs exist?
                pass

            case Status.CONSISTENT:
                # One struct, display extra information
                self.display_properties['Standardisation level'] = self.selected_structure.std_flags

            case Status.CONSISTENT_SELECT:
                # Multiple structures, but one selected
                self.display_properties['Standardisation level'] = self.selected_structure.std_flags
                self.display_properties['Confidence margin'] = self.conf_margin
                self.display_properties['# candidate structures'] = self.n_candidate_structs

                # Also display list of candidate structures separately
                self.display_properties_lists['Alternate structures'] = [
                    v.cont for v in self.candidate_structures_sorted
                ]

            case Status.INCONSISTENT:
                # Multiple structures, none selected
                self.display_properties['Most likely structure'] = self.candidate_structures_sorted[0].cont
                self.display_properties['Confidence margin'] = self.conf_margin
                self.display_properties['# candidate structures'] = self.n_candidate_structs

                # Also display list of candidate structures separately
                self.display_properties_lists['Candidate structures'] = [
                    v.cont for v in self.candidate_structures_sorted
                ]

            case Status.MACRO:
                # Identified as macromolecule
                self.display_properties_lists['Detected reasons'] = self.macro_reasons

        # Also include general information
        self.display_properties['# nodes'] = self.n_identifiers





    def get_struct_vertex(self) -> model.Vertex | None:
        """ If a single structure has been selected, return this. """
        return self.selected_structure



# Response
# ======================================================================================================================
class AppResponse:

    # Basic information
    M:              model.Model
    mode:           parameters.Mode         # The mode in which the response was generated
    session_id:     uuid.UUID               # ID of the web-session if mode == WEB
    query:          app_query.AppQuery      # Query from which to construct the response
    log:            logging.Logger

    explore_view:   nx.MultiDiGraph         # View of the main ID graph only with nodes to fully explore

    # Directory and files
    base_dir:       str                     # Base directory, contains UID directories
    output_dir:     str                     # Session directory

    # Analysis
    compounds:              dict[str, CompoundResponse]     # List of compounds and the response info for each
    compound_default:       CompoundResponse                # HTML - show this compound by default

    # Statistics
    n_macro:                int
    n_single_struct:        int                             # Only a single structure
    n_no_structs:           int
    n_resolved:             int                             # Resolved to a single structure
    n_resolved_by_conf:     dict[float, int]                # For each conf threshold, how many are resolved

    n_consistent:           dict[str, int]                  # How many are consistent for each combination of stdfuncs
    n_inconsistent:         dict[str, int]

    n_total_identifiers:    int
    n_total_structs:        int
    avg_ids_per_compound:   float                           # Number of nodes found per



    def __init__(self, M: model.Model, session_id: uuid.UUID, base_dir: str):
        """ Initialise, doing analysis on the graph in the model
        """
        self.M = M
        self.query = M.query
        self.log = self.query.log
        self.session_id = session_id
        self.base_dir = base_dir
        self.output_dir = os.path.join(self.base_dir, str(self.session_id))

        self.log.info(f'Output directory: {self.output_dir}')

        self.compounds = dict()


        # 1. Analyse all compounds in the model
        # --------------------------------------------------------------------------------------------------------------
        for input_str in M.vertex_map[types.INPUT].keys():
            compound_input: app_query.CompoundInput = M.query.compound_str_map[input_str]
            self.compounds[input_str] = CompoundResponse(M, compound_input)


        # 2. Compute statistics
        # --------------------------------------------------------------------------------------------------------------


        # 3. Finalise initialisation
        # --------------------------------------------------------------------------------------------------------------
        self.compound_default = next(iter(self.compounds.values()))





    # TODO
    pass
