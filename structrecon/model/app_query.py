""" This class defines the input as given by the user, to be used for a model.
"""
from __future__ import annotations

import os.path
from typing import Any
import logging
import uuid

import structrecon.model.types as types
import structrecon.model.ontology as ontology
import structrecon.model.parameters as parameters


# Dictionary allows looking up the relevant parameters given a mode

# Dict to find parameters by name


# Classes needed for the compound and reaction input in the query
# ======================================================================================================================
class CompoundInput:
    """ A compound as given in the input, possibly with multiple associated identifiers.
        The first compound identifier in the list will be considered the 'primary identifier'.
    """
    index:              int
    source_id:          str         # id given in source
    name:               str         # Name found in source
    identifiers:        list[(types.RepresentationType, str)]
    properties:         dict[str, str]

    def __init__(
            self,
            identifiers: list[(types.RepresentationType, str)],
            source_id: str,
            name: str = '',
            properties: dict[str, str] = None
    ):
        self.identifiers = identifiers
        self.source_id = source_id
        self.name = name if name else source_id
        self.index = -1  # Set when added to model
        self.properties = properties if properties else dict()

class ReactionInput:
    """ A reaction, as represented in, for example, an SBML file.
    """
    index:              int
    name:               str


class AppQuery:
    """ An input for the model is a list of compounds, as well as settings.
    """

    # Session details
    mode = parameters.Mode  # Either WEB, CLI, API
    log: logging.Logger     # All activity in this session is logged to this logger.
    uid: uuid.UUID          # Corresponding UUID of session
    outdir: str           # Directory

    # Query
    compounds:          list[CompoundInput]
    reactions:          list[ReactionInput]
    compound_str_map:   dict[str, CompoundInput]

    # Parameters for each parameter in the list of parameters
    query_parameters:         dict[parameters.Parameter, Any]

    # Parameter variables
    conf_threshold:     float
    ontology_explore:   dict[ontology.Relation, str | int]

    def __init__(
            self,
            mode: parameters.Mode,
            uid: uuid.UUID,
            outdir: str
    ):
        """ Initialise given settings from form, API, etc, and the mode (WEB, CLI, API)"""
        self.mode = mode
        self.uid = uid
        self.outdir = outdir

        # Initialise log
        self.log = logging.getLogger(f'structrecon.{self.uid}')
        self.log.propagate = True

        logformat_file = logging.Formatter(
            style='{',
            fmt='{asctime}: {levelname} \t{message}'
        )

        log_file = logging.FileHandler(os.path.join(outdir, 'log'))
        log_file.setLevel(logging.INFO)
        log_file.setFormatter(logformat_file)
        self.log.addHandler(log_file)


        self.log.setLevel(logging.INFO)
        self.log.info(f'Session started. UID: {uid}.')

        self.compounds = []
        self.compound_str_map = dict()
        self.reactions = []
        self.parameters = {}

        # Load parameters
        self.conf_threshold = 0

        # Set default ontology exploration
            # full - explore as a reference
            # noncentral - explore one step, and include ontologies
            # info - only include ontologies between existing nodes
            # none - do not explore
        self.ontology_explore = {
            ontology.IS_A: 'info',
            ontology.HAS_INSTANCE: 'info',
            ontology.HAS_PART: 'none',
            ontology.TAUTOMER: 'noncentral',
            ontology.CONJUCATE_ACID: 'noncentral',
            ontology.CONJUGATE_BASE: 'noncentral',
            ontology.STEREOISOMER: 'noncentral',
            ontology.ENANTIOMER: 'noncentral',
            ontology.HAS_NEW_ID: 'full',
            ontology.HAS_OLD_ID: 'full',
        }

    def add_compound_from_line(
            self,
            line: str,
            type_default: types.RepresentationType = types.AUTO
    ) -> CompoundInput | None:

        # First, check if the first partition of the line is a valid type
        parts = line.split()
        if len(parts) > 1:
            type_str = parts[0]
            repr_type = types.get_type_from_str(type_str)
            if repr_type:
                # Found a type, add the last as identifier
                identifier = ''.join(parts[1:])
                return self.add_identifier(repr_type, identifier)

        # Else treat the entire line as one input
        identifier = line.strip()
        if not identifier:
            return None

        if type_default is types.AUTO:
            # Automatically infer
            inferred_types = types.get_possible_types_regex(identifier)
            return self.add_compound(
                CompoundInput(
                    [(t, identifier) for t in inferred_types],
                    identifier
                )
            )
        else:
            # Use given type
            return self.add_identifier(type_default, identifier)


    def add_compound(self, c: CompoundInput) -> CompoundInput:
        c.index = len(self.compounds)
        self.compounds.append(c)
        self.compound_str_map[c.source_id] = c
        return c


    def add_identifier(self, repr_type: types.RepresentationType, identifier: str) -> CompoundInput:
        """ Add a new compound with just one identifier. """
        c = CompoundInput([(repr_type, identifier)], identifier)
        self.add_compound(c)
        return c


    def add_reaction(self, r: ReactionInput) -> ReactionInput:
        r.index = len(self.reactions)
        self.reactions.append(r)
        return r


    def set_parameter(self, parameter: parameters.Parameter, value: Any):
        # Check that the type matches
        if not isinstance(value, parameter.value_type):
            self.log.error(f'TYPE MISMATCH: value {value}, type: {parameter.value_type}')
            assert(isinstance(value, parameter.value_type))

        self.parameters[parameter] = value


    def is_parameter_set(self, parameter: parameters.Parameter) -> bool:
        """ True if parameter is already set """
        return parameter in self.parameters.keys()


    def set_remaining_parameters_to_default(self):
        """ Set all parameters not yet set to the default setting. """
        for param in parameters.parameters_mode[self.mode]:
            if not self.is_parameter_set(param):
                self.set_parameter(param, param.default)
            else:
                if param.param_type == 'check':
                    # If dict-like, set unset options to default without overriding explicitly set options
                    assert(isinstance(param.default, dict))
                    for key, default_value in param.default.items():
                        if key not in self.parameters[param].keys():
                            self.parameters[param][key] = default_value

        # Set ontologies exploration also
        if self.parameters[parameters.param_explore_deprecation]:
            self.ontology_explore[ontology.HAS_NEW_ID] = 'full'
            self.ontology_explore[ontology.HAS_OLD_ID] = 'full'
        else:
            self.ontology_explore[ontology.HAS_NEW_ID] = 'info'
            self.ontology_explore[ontology.HAS_OLD_ID] = 'info'

        if self.parameters[parameters.param_follow_child]:
            self.ontology_explore[ontology.HAS_INSTANCE] = 'full'
        else:
            self.ontology_explore[ontology.HAS_INSTANCE] = 'info'



    def validate(self) -> bool:
        """ Perform checks and sets default parameters. Should always be run before
            using the query to construct a model.
        """

        # Set default parameters if not already set
        self.set_remaining_parameters_to_default()

        # Check that at least one compound exists
        if len(self.compounds) <= 0:
            raise IOError('No compounds in query')

        # Set parameter variables
        self.conf_threshold = self.parameters[parameters.param_conf_threshold]

        # Finalise
        self.log.info(f'Query validated:\n{self.__str__()}')


        return True  # TODO

    def __str__(self):
        """ Print with parameters. """
        s = 'Query parameters:\n'
        for param in self.parameters.keys():
            s += f'  - {param.name}: {self.parameters[param]}\n'
        return s


    def save_parameters_to_json(self, outdir: str):
        """ Save the parameters as a .json file which can be uploaded later."""
        # TODO
