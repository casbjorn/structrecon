""" Generates the file returned based on an AppQuery, Model, and AppResponse
"""
import json
import os.path

import structrecon.model.app_query as app_query
import structrecon.model.app_response as app_response
import structrecon.model.parameters as parameters


def get_json_response(R: app_response.AppResponse, outdir: str):
    M = R.M
    Q = R.query
    json_out = dict()

    # 1. Encode parameters in JSON
    # ------------------------------------------------------------------------------------------------------------------
    params_dict = {}
    # TODO

    param: parameters.Parameter
    for param, value in Q.parameters.items():
        params_dict[param.name] = value


    json_out['parameters'] = params_dict

    # 2. Encode compounds
    # ------------------------------------------------------------------------------------------------------------------
    compounds_dict = dict()
    for compound_response in R.compounds.values():

        if compound_response is None:
            continue

        # Get compound information
        compound_query: app_query.CompoundInput = compound_response.input_compound

        # Make compound dict based on input and result
        json_compound = dict()
        json_compound['input_id'] = compound_query.source_id

        if compound_query.properties:
            json_compound['properties'] = compound_query.properties

        # Print the status of the compound
        try:
            match compound_response.status:
                case app_response.Status.NO_STRUCT:
                    # No structures detected
                    json_compound['status'] = 'No valid structures'
                    # TODO found un-readable SMILES structures?

                case app_response.Status.MACRO:
                    # No structures, identified as macro molecule
                    json_compound['status'] = 'Macromolecule'
                    json_compound['macro_reasons'] = compound_response.macro_reasons

                case app_response.Status.CONSISTENT:
                    # Only one structure up to certain standardisation
                    json_compound['status'] = 'Consistent'
                    json_compound['inchi'] = compound_response.selected_structure.cont
                    json_compound['standardisation'] = compound_response.selected_structure_std_state
                    json_compound['link_distance'] = compound_response.vertex_link_distance[compound_response.selected_structure]

                case app_response.Status.CONSISTENT_SELECT:
                    # Consistent when selecting based on ranking
                    json_compound['status'] = 'Consistent'
                    json_compound['inchi'] = compound_response.selected_structure.cont
                    json_compound['conf_margin'] = compound_response.conf_margin
                    json_compound['standardisation'] = compound_response.selected_structure_std_state
                    json_compound['link_distance'] = compound_response.vertex_link_distance[compound_response.selected_structure]

                case app_response.Status.INCONSISTENT:
                    # Inconsistent - multiple structures
                    json_compound['status'] = 'Inconsistent'
                    json_compound['conf_margin'] = compound_response.conf_margin
        except KeyError:
            pass


        # Print list of structures, primary structure, confidence, standardisation, etc.
            # This should be printed no matter the status of the compound
        cand_structs_list = list()
        if compound_response.status in {app_response.Status.CONSISTENT_SELECT, app_response.Status.INCONSISTENT}:
            for cand_struct in compound_response.candidates:
                cand_struct_dict = dict()
                cand_struct_dict['inchi'] = cand_struct.cont
                cand_struct_dict['conf'] = compound_response.candidate_conf_rel[cand_struct]
                cand_struct_dict['standardisation'] = cand_struct.std_flags
                cand_struct_dict['link_distance'] = compound_response.vertex_link_distance.get(cand_struct, 'N/A')
                if cand_struct.properties:
                    cand_struct_dict['properties'] = cand_struct.properties
                cand_structs_list.append(cand_struct_dict)

            # Sort list by confidence
            cand_structs_list.sort(
                key = lambda c: c['conf'],
                reverse = True
            )

            json_compound['candidate_structures'] = cand_structs_list

        # Print misc information (size of identifier graph, etc.)
        # TODO

        # Finally append the compound
        compounds_dict[compound_query.source_id] = json_compound

    # Finalise by adding the list to JSON
    json_out['compounds'] = compounds_dict

    # 3. Encode general statistics
    # ------------------------------------------------------------------------------------------------------------------
    stats_dict = {}
    # TODO





    json_out['stats'] = stats_dict

    # 4. Finally, write the JSON data
    # ------------------------------------------------------------------------------------------------------------------
    with open(os.path.join(outdir, 'response.json'), 'w') as f:
        json.dump(
            obj = json_out,
            fp = f,
            indent = 4
        )




