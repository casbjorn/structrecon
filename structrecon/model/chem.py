""" Defines various helper functions related to chemistry
"""

from rdkit.Chem import AllChem as rdk
import structrecon.include.chem_parse as chem_parse

elements: list[str] = [
]

# Chemical formula
# ----------------------------------------------------------------------------------------------------------------------

# A formula is a dictionary from element symbols to a number
ChemFormula = dict[str, int]

def chem_formula_from_string(formula: str) -> ChemFormula:
    return chem_parse.parse_formula(formula)

def chem_formula_to_string(chemformula: ChemFormula) -> str:
    # TODO
    pass

def chem_formula_remove_h(chemformula: ChemFormula) -> ChemFormula:
    """ Returns a copy of the formula with Hydrogens removed. """
    f_2 = chemformula.copy()
    if 'H' in f_2:
        del f_2['H']
    return f_2

def chem_formula_equal_modulo_h(form_a: ChemFormula, form_b: ChemFormula) -> bool:
    """ Return whether two chemical formulae are equal, not counting hydrogens or charge. """
    return chem_formula_remove_h(form_a) == chem_formula_remove_h(form_b)
    # TODO optimise


# noinspection PyArgumentList
def chem_formula_from_molecule(mol: rdk.Mol) -> ChemFormula:
    """. """
    atom: rdk.Atom
    mol_h = rdk.AddHs(mol)
    formula = ChemFormula()
    for atom in mol_h.GetAtoms():
        e = atom.GetSymbol()
        try:
            formula[e] += 1
        except KeyError:
            formula[e] = 1
    return formula
