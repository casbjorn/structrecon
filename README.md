![StructRecon logo](./banner.svg)

---

StructRecon is a cheminformatics tool for automatically determining the structure of 
compounds from given database identifiers.

StructRecon traverses the cross-references between chemical databases in order to obtain a more 
complete view of the information available on any given compound. 
This allows StructRecon to resolve structures for database identifiers which may not directly 
contain the structure. In the case where the program finds multiple conflicting chemical structures, 
a random-walk based scoring algorithm is used to determine the most likely structure.

---

StructRecon is written in Python, and the source code is available at GitLab. It accompanies the paper:

> **Reconciling Inconsistent Molecular Structures from Biochemical Databases** 
> 
> Casper Asbjørn Eriksen, Jakob Lykke Andersen, Rolf Fagerberg, Daniel Merkle (2023)
> 
> *Lecture Notes in Computer Science (LNBI, vol 14248), Proceedings of the International 
> symposium of Bioinformatics Research and Applications (ISBRA 2023)*
> - [DOI:978-981-99-7074-2_5](https://doi.org/10.1007/978-981-99-7074-2_5), 
> - [ArXiV Preprint](https://arxiv.org/abs/2308.12735).

If you choose to use StructRecon in your research, we kindly ask you to cite this paper.

---

## Interactive interface
The University of Southern Denmark hosts an instance of StructRecon. It can be accessed at the homepage of StructRecon: https://cheminf.imada.sdu.dk/structrecon/

## API
StructRecon is also available to use through an API. API usage is detailed in the [API documentation](https://gitlab.com/casbjorn/structrecon/-/wikis/home/API-Documentation)