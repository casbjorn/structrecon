#!/bin/bash
echo "######################################################"
echo "Importing database"
echo "######################################################"
pg_restore -d structrecon_db /backups/structrecon_db_dump
echo "######################################################"
echo "Database import done"
echo "######################################################"
