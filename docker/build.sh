#!/bin/bash
root=$(git rev-parse --show-toplevel)
cd $root
echo "###############################################################################"
echo "Building app"
echo "###############################################################################"
docker build -t structrecon -f docker/App_Dockerfile . || exit 1

echo "###############################################################################"
echo "Building database"
echo "###############################################################################"
docker build -t structrecon-db -f docker/DB_Dockerfile . || exit 1
