import math

import flask
from flask import Flask, render_template, request
from werkzeug.utils import secure_filename

import sys
import os
import uuid
import zipfile
import io
import pathlib
import argparse
import shutil
import psycopg
import logging

# Relevant imports from StructRecon
import structrecon.model.model as model
import structrecon.model.parameters as parameters
import structrecon.model.app_query as app_query
import structrecon.model.types as types
import structrecon.model.app_response as app_response
import structrecon.model.graph_printer as graph_printer
import structrecon.model.json_response as json_response

import structrecon.fileinterface.fileinterface_base as fileinterface_base
import structrecon.fileinterface.fileinterface_list as fileinterface_list
import structrecon.fileinterface.fileinterface_sbml as fileinterface_sbml
import structrecon.fileinterface.fileinterface_json as fileinterface_json

import structrecon.cache.cache as cache

# Initialise the server
# ======================================================================================================================

# Logging - Create global logger and add handler
log_global = logging.getLogger('structrecon')
log_global.setLevel(logging.INFO)
console_log = logging.StreamHandler()
console_log.setLevel(logging.INFO)
log_global.addHandler(console_log)

# Specify command-line arguments for running the server!
# ======================================================================================================================
parser = argparse.ArgumentParser()
parser.add_argument(
    '-d', '--docker',
    help = "Set if running inside a docker container",
    action = 'store_true'
)

parser.add_argument(
    '--deletedb',
    help = "Delete the structrecon_db database (debug)",
    action = 'store_true'
)

parser.add_argument(
    '--force',
    help = "If deleting the database, toggle this to force deletion",
    action = 'store_true'
)

parser.add_argument(
    '-p', '--populate',
    help = "Populate the database from the files located in a directory",
    nargs = 1,
    type = str
)

parser.add_argument(
    '-pl', '--populatelimit',
    help = "Debug: Only partially populate from files up to *n* entries from each file.",
    nargs = 1,
    type = int
)

# If no database info is specified, use a local postgres by default
parser.add_argument(
    '--db_host',
    help = "Hostname for Postgres database server",
    nargs = 1,
    type = str,
    default = 'localhost'
)

parser.add_argument(
    '--db_port',
    help = "Database port",
    nargs = 1,
    type = str,
    default = '5432'
)

parser.add_argument(
    '--db_name',
    help = "Name of the database on the server",
    nargs = 1,
    type = str,
    default = 'structrecon_db'
)

parser.add_argument(
    '--db_user',
    help = "Database username",
    nargs = 1,
    type = str,
    default = 'postgres'
)

parser.add_argument(
    '--db_pass',
    help = "Database password",
    nargs = 1,
    type = str,
    default = 'postgres'
)

parser.add_argument(
    '--session_directory_size',
    help = "The maximum size of the data directory in KB, (Allocate at least 10MB less than the required space).",
    nargs = 1,
    type = int,
    default = 10_000_000
)

parser.add_argument(
    '--only_display',
    help = "Only display the website, without connecting to the database (debug)",
    action = 'store_true'
)


# Parse server command-line arguments
# ======================================================================================================================
args = parser.parse_args()
docker = args.docker

# Docker conf
if docker:
    app_dir = '/app/structrecon-web'
else:
    app_dir = '.'

# Database conf parsing
db_host = args.db_host[0]
db_port = args.db_port[0]
db_name = args.db_name[0]
db_user = args.db_user[0]
db_pass = args.db_pass[0]

# Data directory string
    # TODO set from cmd params
data_dir_str: str = 'database'


# Database
# ======================================================================================================================

# Command-line argument '--deletedb' should remove the database from the given postgres server
if args.deletedb:
    log_global.info(f'Deleting database.')
    log_global.info(f'Connecting to server: postgres://{db_user}:{db_pass}@{db_host}:{db_port}')
    cache.cache_delete(db_host, db_port, db_name, db_user, db_pass, args.force)
    log_global.info(f'Database deleted.')
    exit(0)

# Skip database connection if only testing the website
only_display: bool = False
if args.only_display:
    print(f'Only displaying website.')
    only_display: bool = True

# Initialise connection
if not only_display:
    log_global.info(f'Connecting to database: postgres://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}')
    conn = cache.connect(db_host, db_port, db_name, db_user, db_pass)
    if not conn:
        log_global.info(f'Database connection failed. Aborting.')
        exit(1)
        # TODO also close this connection again at some point

# Populate if chosen
if args.populate:
    # Read number of lines to populate
    if args.populatelimit:
        poplimit = args.populatelimit[0]
    else:
        poplimit = sys.maxsize

    # Populating database from directory
    cache.populate(args.populate[0], conn, limit = poplimit)
    conn.close()
    log_global.info(f'Successfully populated database')       # TODO mention number of entries, etc.
    exit(0)


# Web App Configuration and Initialisation
# ==================================================================================================================

# Init flask app
app = Flask(__name__)

# Define input types
input_types: dict[str, types.RepresentationType] = dict()
input_types['Automatically Infer'] = types.AUTO
repr_type: types.RepresentationType
for repr_type in types.standard_repr_types:
    if repr_type.name == 'INPUT': continue
    input_types[repr_type.alt_names[0]] = repr_type

# Define exceptions
class InvalidFormException(Exception):
    pass

max_dir_size = args.session_directory_size
log_global.info(f'Started server. Max directory size: {max_dir_size} MB.')


# Set loglevel back to error after initialisation
log_global.setLevel(logging.ERROR)
console_log.setLevel(logging.ERROR)


# Primary app view
# ======================================================================================================================
@app.route('/', methods = ['GET', 'POST'])
def main_app():

    # If getting the main page, show the form
    if request.method == 'GET':
        return render_template(
            'app.html',
            input_types = input_types,
            web_params = parameters.parameters_web  # Pass the parameters to make available
        )

    # If a post is submitted, process the request
    elif request.method == 'POST':

        # Generate a session ID and make the corresponding directory
        uid = uuid.uuid4()

        # Directory stuff
        base_dir = os.path.join('structrecon-web/data')
        outdir = os.path.join(base_dir, str(uid))

        try:
            os.mkdir(base_dir)
        except FileExistsError:
            pass
        os.mkdir(outdir)
        os.mkdir(f'{outdir}/compounds')
        os.mkdir(f'{outdir}/compounds/img')

        # Enter UID into database
        cache.activate_session(conn = conn, uid = uid)


        # Examine form content to determine runmode and generate an AppQuery
        form = request.form
        try:

            # Create empty query and populate
            query = app_query.AppQuery(
                mode = parameters.Mode.WEB,
                uid = uid,
                outdir = outdir
            )

            # 1. Load parameters from form and add to query
            # ----------------------------------------------------------------------------------------------------------
            for param in parameters.parameters_web:

                # Do not set if already set by the JSON file (JSON overrides given settings)
                if query.is_parameter_set(param):
                    continue
                query.log.info(f'Param {param.name}, type {param.value_type}')

                if param.param_type in {'int', 'float', 'str'}:
                    # Parse input field
                    query.set_parameter(
                        param,
                        form.get(param.name, type=param.value_type)
                    )
                elif param.param_type == 'bool':
                    # Parse checkbox - see if it exists in form dict
                    if form.get(param.name):
                        query.set_parameter(param, True)
                    else:
                        query.set_parameter(param, False)
                elif param.param_type == 'check':
                    # Checkbox list, return dict[str, bool]
                    dict_choices: dict[str, bool] = {}
                    for choice in param.choices:
                        dict_choices[choice] = False
                    for choice_selected in form.getlist(param.name):
                        dict_choices[choice_selected] = True
                    query.set_parameter(param, dict_choices)
                elif param.param_type == 'radio':
                    query.set_parameter(
                        param,
                        form.get(param.name)
                    )

            # 2. Parse identifier form and add compounds and reactions to query
            # ----------------------------------------------------------------------------------------------------------
            if form.get('input_radio') == 'ids':
                # ID list: get type given as input
                repr_type_str = form.get('select_id_type_1')
                primary_id_type = input_types[repr_type_str]
                query.log.info(f'Repr_type: {primary_id_type.name}')

                # Get ids and add to query
                id_list_str = form.get('input_id_list')
                for line in id_list_str.splitlines():
                    query.add_compound_from_line(line, primary_id_type)

                # Validate
                if len(query.compounds) < 1:
                    raise InvalidFormException('Choose at least one compound as input.')

            elif form.get('input_radio') == 'file_ids':
                # File of IDs - Get type
                repr_type_str = form.get('select_id_type_2')
                repr_type_primary = input_types[repr_type_str]
                query.log.info(f'Repr_type: {repr_type_primary.name}')

                # Handle file upload (to outdir)
                app.config['UPLOAD_FOLDER'] = outdir
                query.log.info(f'FILES: {request.files}')

                if 'id_list_file' not in request.files:
                    raise InvalidFormException('Please select a file to upload.')
                file_upload = request.files['id_list_file']
                if file_upload.filename == '':
                    raise InvalidFormException('Please select a file to upload.')
                fname_upload = secure_filename(file_upload.filename)
                file_upload.save(os.path.join(app.config['UPLOAD_FOLDER'], fname_upload))

                # Put in query
                interface = fileinterface_list.ListInterface()
                interface.add_to_query(
                    query,
                    f'{outdir}/{fname_upload}',
                    repr_type_primary
                )

            elif form.get('input_radio') == 'file_sbml':
                # SBML file - Handle file upload (to outdir)
                app.config['UPLOAD_FOLDER'] = outdir
                if 'sbml_file' not in request.files:
                    raise InvalidFormException('Please select a file to upload.')
                file_upload = request.files['sbml_file']
                if file_upload.filename == '':
                    raise InvalidFormException('Please select a file to upload.')
                fname_upload = secure_filename(file_upload.filename)
                file_upload.save(os.path.join(app.config['UPLOAD_FOLDER'], fname_upload))

                interface = fileinterface_sbml.SBMLInterface()
                interface.add_to_query(
                    query,
                    f'{outdir}/{fname_upload}'
                )

            elif form.get('input_radio') == 'file_json':
                # JSON file - Handle file upload (to outdir)
                app.config['UPLOAD_FOLDER'] = outdir
                if 'json_file' not in request.files:
                    raise InvalidFormException('Please select a file to upload.')
                file_upload = request.files['json_file']
                if file_upload.filename == '':
                    raise InvalidFormException('Please select a file to upload.')
                fname_upload = secure_filename(file_upload.filename)
                file_upload.save(os.path.join(app.config['UPLOAD_FOLDER'], fname_upload))

                interface = fileinterface_json.JsonInterface()
                interface.add_to_query(
                    query,
                    f'{outdir}/{fname_upload}'
                )

            else:
                raise InvalidFormException('No mode selected.')

        except InvalidFormException as e:
            # TODO handle error
            return f'<p>Form handling error: {e}<p>'

        # 3. Create the model and analyse
        # --------------------------------------------------------------------------------------------------------------
        # Print the query
        R = process_query(
            query = query,
            base_dir = base_dir,
            uid = uid
        )

        # Return the actual template
        return render_template(
            'result.html',
            response = R,
            ns_response = app_response,
            ns_parameters = parameters,
            query = query,
            uid = uid,
            outdir = outdir,
            data_dir = outdir.removeprefix('structrecon-web/'),
            conf_margin = query.parameters[parameters.param_conf_threshold]
        )

        # TODO show statistics
        # TODO periodically clear data directory
            # Should maybe be activated when data dir has size greater than set number of gigabytes?
        # TODO links in the SVG file to identifier.org
        # TODO also remove the uploaded file after processing


# API access
# ======================================================================================================================

@app.route('/api/json', methods = ['POST'])
def api_request_json():
    return api_request(only_json = True)



@app.route('/api', methods = ['GET', 'POST'])
def api_request(only_json: bool = False):
    """ Recieves a request and parses it, returns data.
        Takes a json file with settings and optionally compounds and reactions,
        or if no compounds given in the json file, can also take a SBML file or other list of compounds.
    """
    if request.method == 'GET':
        # Confirm api working
        return 'API contact success. Send a POST request to interact with the API.'


    if request.method == 'POST':
        # Process the request based on the input files
        print(f'API POST received.')

        # Generate a session ID and make the corresponding directory
        uid = uuid.uuid4()

        # Directory stuff
        base_dir = os.path.join('structrecon-web/data')
        outdir = os.path.join(base_dir, str(uid))

        try:
            os.mkdir(base_dir)
        except FileExistsError:
            pass
        os.mkdir(outdir)  # TODO handle edge cases
        os.mkdir(f'{outdir}/compounds')
        os.mkdir(f'{outdir}/compounds/img')
        app.config['UPLOAD_FOLDER'] = outdir


        # Get the files
        f_parameters = None
        f_compounds = None
        try:
            f_parameters = request.files['parameters']
        except KeyError:
            print(f'  - Parameters file not found.')
        try:
            f_compounds = request.files['compounds']
        except KeyError:
            print(f'  - Compounds file not found')

        print(f'  - params file: {f_parameters}\n  - compounds file: {f_compounds}')

        # Construct the query object
        query = app_query.AppQuery(
            mode = parameters.Mode.API,
            uid = uid,
            outdir = outdir
        )

        # Load parameters
        if f_parameters:
            fname_parameters = secure_filename(f'{uid}_parameters')
            f_parameters.save(os.path.join(app.config['UPLOAD_FOLDER'], fname_parameters))
            try:
                interface_params = fileinterface_json.JsonInterface()
                interface_params.add_to_query(
                    query,
                    f'{outdir}/{fname_parameters}'
                )
            except IOError:
                return 'Parameters could not be loaded.', 400
            print(f'Parameters loaded from file.')

        # Set remaining parameters
        query.set_remaining_parameters_to_default()


        # Load compounds based on the file suffix
        if f_compounds:
            fname_compounds = secure_filename(f'{uid}_compounds')
            f_compounds.save(os.path.join(app.config['UPLOAD_FOLDER'], fname_compounds))
            try:
                _, f_compounds_extension = os.path.splitext(f_compounds.filename)
                print(f'Compounds filename: {f_compounds.filename}, extension: {f_compounds_extension}')
                interface: fileinterface_base.FileInterface
                if f_compounds_extension == '.json':
                    interface = fileinterface_json.JsonInterface()
                elif f_compounds_extension in {'.xml', '.sbml'}:
                    interface = fileinterface_sbml.SBMLInterface()
                elif f_compounds_extension == '.txt':
                    interface = fileinterface_list.ListInterface()
                else:
                    raise TypeError('Please supply the compounds file in a recognised format.')
                interface.add_to_query(
                    query,
                    f'{outdir}/{fname_compounds}'
                )
                print(f'Compounds file loaded.')
            except TypeError:
                return 'Compounds could not be loaded.', 400
        print(f'API file loading complete.')

        # JSON only ?
        #if query.parameters[parameters.param_only_json_response]:
        #    only_json = True

        # Process the query
        print(query.__str__())
        R = process_query(
            query = query,
            base_dir = base_dir,
            uid = uid,
            only_json = only_json
        )

        # Return JSON / ZIP
        if only_json:
            return get_json(uid)
        else:
            return request_zip(uid)


# Request processing
# ----------------------------------------------------------------------------------------------------------------------

def process_query(
    query: app_query.AppQuery,
    uid: uuid.UUID,
    base_dir: str,
    only_json: bool = False
) -> app_response.AppResponse:
    # Set query parameters

    outdir = os.path.join(base_dir, str(uid))

    # Initialise StructRecon model
    M = model.Model()
    M.load_from_query(query)
    M.explore(conn=conn)
    M.standardise()
    log = query.log

    response = app_response.AppResponse(M, uid, base_dir)

    # Configure printer
    if not only_json:
        printer = graph_printer.GraphPrinter(
            query = query,
            response = response
        )
        # Print ID graphs
        if query.parameters[parameters.param_draw_id_graphs]:
            printer.print_compounds(
                M,
                outdir=outdir,
                simplenames=True  # Make names in the format <INPUT>.svg
            )

    # Print JSON response
    json_response.get_json_response(response, outdir)

    # Print images, and set list in analysis result
    log.info(f'Directory: {outdir}')

    # Set session data
    set_session_size(conn, base_dir, uid)

    # Remove old directories
    check_old_sessions(conn, base_dir, max_dir_size, donotdelete_uid = uid)

    return response


# Resource getters
# ----------------------------------------------------------------------------------------------------------------------

# Compound SVG identifier graph getter
@app.route('/data/<uuid:uid>/compounds/<fname>', methods = ['GET'])
def get_data_file(uid, fname):
    """Simply serve the image at the directory"""
    return flask.send_file(os.path.join(app_dir, 'data', str(uid), 'compounds', fname))

# Molecule image
@app.route('/data/<uuid:uid>/compounds/img/<fname>', methods = ['GET'])
def get_mol_img(uid, fname):
    """Simply serve the image at the directory"""
    return flask.send_from_directory(
        os.path.join(app_dir, 'data', str(uid), 'compounds', 'img'),
        fname
    )

# JSON getter
@app.route('/data/<uuid:uid>/json', methods = ['GET'])
def get_json(uid):
    print(f'JSON download requested')
    return flask.send_from_directory(
        os.path.join(app_dir, 'data', str(uid)),
        'response.json'
    )

# Get directory as zip
@app.route('/data/<uuid:uid>/zip', methods = ['get'])
def request_zip(uid):
    log: logging.Logger = logging.getLogger(f'structrecon.{uid}')
    log.info(f'Zipping directory {uid}')
    base_path = pathlib.Path(os.path.join(app_dir, 'structrecon-web', 'data', str(uid)))
    data = io.BytesIO()

    # Recursively add files to the zip
    def recursive_zip_write(path: pathlib.Path, zip: zipfile.ZipFile):
        for f_name in path.iterdir():
            if f_name.is_file():
                archive_path = os.path.relpath(f_name, base_path)
                zip.write(
                    filename = f_name,
                    arcname = archive_path
                )
            elif f_name.is_dir():
                recursive_zip_write(f_name, zip)

    with zipfile.ZipFile(data, mode = 'w') as z:
        recursive_zip_write(base_path, z)

    # Return the datastream to the start
    data.seek(0)

    log.info(f'Created zip file.')

    return flask.send_file(
        data,
        mimetype='application/zip',
        as_attachment=True,
        download_name='output.zip'
    )






# Session management
# ----------------------------------------------------------------------------------------------------------------------
def set_session_size(conn: psycopg.connection.Connection, datadir: str, uid: uuid.UUID):
    """ Calculate the size of the directory and set in the database. """
    log = logging.getLogger(f'structrecon.{uid}')
    def size_recursive(path: str) -> int:
        size = 0
        for f in os.scandir(path):
            if f.is_file():
                size += f.stat().st_size
            elif f.is_dir():
                size += size_recursive(f.path)
        return size

    # Get size in KB
    size = math.ceil(size_recursive(os.path.join(datadir, str(uid))) / 1e+3)
    log.info(f'UUID: {uid}, size: {size} KB ({datadir}/{str(uid)})')
    cache.set_session_dir_size(conn, uid, size)



def check_old_sessions(conn: psycopg.connection.Connection, datadir: str, maxsize_kb: int, donotdelete_uid: uuid.UUID):
    """ If the size of the data directory exceeds the threshold, remove old sessions until this is
        no longer the case.
        - The datadir is the directory containing the UUID directories
    """
    size_total_kb = cache.get_total_size_kb(conn)
    if size_total_kb > maxsize_kb:
        # Remove older
        uids_delete = cache.get_sessions_to_delete(conn, size_total_kb - maxsize_kb)
        for uid in uids_delete:
            if uid == donotdelete_uid:
                continue
            dir_delete = os.path.join(datadir, str(uid))
            shutil.rmtree(dir_delete)
            cache.deactivate_session(conn, uid)

# App
# ======================================================================================================================

if __name__ == '__main__':
    app.run(host='0.0.0.0')
