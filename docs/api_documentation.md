# StructRecon API documentation

## Communicating with the server
The API can be used with either a self-hosted server, or via the instance hosted
by the Algorithmic Cheminformatics Group at the University of Southern Denmark.

### Sending requests

If using our instance, the request should be directed to 
```https://cheminf-live.imada.sdu.dk/structrecon/api```

Send an **HTTP POST** request to the server with a file named ```compounds``` (format ```.json```, ```.txt```, 
```.sbml``` or ```.xml```). If in JSON format, this file may also contain parameters. Otherwise, parameters can be
specified in a separate JSON-formatted file named ```parameters```.

For example, using CURL:

```shell
curl -X POST https://cheminf-live.imada.sdu.dk/structrecon/api -F "compounds=@mymodel.xml" -F "parameters=@myparams.json" > structrecon-response.zip
```

The server will respond with a zip file containing the output.
Alternatively, if only the JSON file containing the response information is desired, the HTTP POST request can instead
be directed to ```https://cheminf-live.imada.sdu.dk/structrecon/api/json```. For example:

```shell
curl -X POST https://cheminf-live.imada.sdu.dk/structrecon/api/json -F "compounds=@mymodel.xml" > structrecon-response.json
```


## Formatting the input
The ```compounds``` file supplies compounds and reactions, and optionally parameters.
If no parameters are supplied, defaults are used.

```json
{
    "parameters": {
      ...
    },
    "compounds": [
        ...
    ]
}
```

The ```compounds``` field contains a list of compounds. These may be specified in
two ways:

### Compounds with one identifier each
If each compound has only one associated identifier, the ```default_type``` 
field should be specified in the parameters, otherwise StructRecon will attempt
to automatically infer the type of each compound. In this case, ```compounds``` 
is a JSON list, containing the compounds.

```json
{
  "parameters": {
    "default_type": "bigg"
  },
  "compounds": [
      "cit",
      "icit",
      "ooa",
      ...
  ]  
}
```

### Compounds with multiple identifiers
Alternatively, if each compound has multiple associated identifiers, ```compounds```
is a JSON object, with each compound specified as a key-value pair according 
to the following syntax:
```json
"name": [
  ["type", "identifier"],
  ...
]
```

Where ```name``` is a unique identifier for the compound. 
This may be identical to one of the identifiers within the same compound. 
An example input is given below.

```json
{
  "compounds": {
    "citrate": [
      ["bigg", "cit"],
      ["pubchem", 31348],
      ...
    ],
    "oxaloacetate": [
      ["bigg", "ooa"],
      ["pubchem", 970],
      ...
    ],
    ...
  }
}
```

## Specifying parameters
Parameters may be specified within the ```compounds``` file if it is supplied as a JSON file.
Otherwise, they may be uploaded in a JSON-formatted file named ```parameters```.

The parameters are specified as key-value pairs within the ```parameters``` object.
The following parameters are available:

A list of parameters which may be specified is given in the [parameters.md](parameters.md) file.



# Interpreting the response
TODO