**Confidence threshold**: ```conf_threshold``` -  *(number between 0 and 1)*.
Defines the maximum ratio between the structures with the most and second-most 
confidence which allows automatically selecting the structure with the most 
confidence. A smaller value is stricter.

**Standardisation function order**: ```stdfunc_order``` - *(string containing F,I,C,T,S)*.
Define which standardisation functions to use and the order in which they are applied. 
Entered as a string containing the letter F, I, C, T, S, up to one time each.

**Always apply standardisation functions**: ```stdfunc_always``` - *(string containing F,I,C,T,S)*.
Define which standardisation functions will be applied to structures, even if they are not
necessary to ensure a consistent structure is found.

**Link deprecated IDs**: ```explore_depr``` - *(bool)*.
If true, the database traversal algorithm also explores entries which are stated to be either deprecated
entries replaced by the current entry, or if the current entry is deprecated, the new entry.

**Sources**: ```sources``` - *(dictionary str->bool)*.
If this field is specified, a source can be explicitly disabled. If not specified, all
sources are used by default. The sources are ```BiGG```, ```ChEBI```, ```ECMDB```,
```MetaNetX```, ```MetaNetX deprecations``` ```PubChem```.

**Default Type**:```default_type``` - *(string)*.
TODO doc

**Ignore Ontology**: ```ignore_ontology``` - *(bool)*.
If true, ontology relations from ChEBI are ignored.

**Draw Identifier Graphs**: ```draw_id_graphs``` - *(bool)*.
Whether to draw identifier graphs, visusalising the database entry traversal.

**Draw molecule images**: ```draw_mol_images``` - *(bool)*.
Use the RDKit software to draw images of each molecule in the identifier graphs.

**Draw confidence on all nodes**: ```draw_conf_on_all``` - *(bool)*.
By default, only the relative confidence score given to nodes by the ranking algorithm is displayed for the candidate
structures. Enabling this shows the absolute confidence score for all vertices. 

**Draw plots**: ```make_stats_images``` - *(bool)*.
Whether to draw plots of various statistics.

**Infer IDs from SBML file**: ```infer_sbml_ids``` - *(bool)*.
Compounds in SBML files may be given names referring to a particular database, depending on
the source of the SBML file, usually BiGG. 
When this parameter is enabled, StructRecon attempts to discern the type of identifier, and
use the corresponding database entry.

**Enforce Charge from SBML file**: ```sbml_enforce_charge``` - *(bool)*.
If this setting is enabled, and the SBML file contains charge information, candidate structures
found in the database search will be filtered based on whether their formal charge is equivalent
to the one given by the SBML file.

**Enforce formula from SBML file**: ```sbml_enforce_formula``` - *(bool)*.
This setting is similar to the previous, but applies to formulaes.

**Confidence ranking decay**: ```ranking_decay``` - *(number between 0 and 1)*.
Structure nodes which are more closely related to the source in the identifier graph
are preferred. Before selecting a structure, the confidence of a node, ```c```, is modified
based on the distance to the source node ```d``` based on the ranking decay factor ```r``` by
the following relation: ```c' = c * (1-r)^d ```. 
A higher ranking decay factor leads to more sharply decreased confidence scores for 
distant nodes.

**Ranking weight of deprecation edges**: ```depr_weight``` - *(number between 0 and 1)*.
Gives the weight of deprecation edges for the ranking algorithm.

**Ranking weight of common names**: ```common_name_weight``` - *(number between 0 and 1)*.
Gives the weight of common name nodes in the ranking algorithm. 
Reducing this may prevent spurious connection based on common names from being selected by
the ranking algorithm.